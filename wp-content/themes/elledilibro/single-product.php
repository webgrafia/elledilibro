<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package elledilibro
 */

get_header();

get_template_part("template-parts/common/breadcrumb", "product");

global $product;
?>
<?php while ( have_posts() ) : ?>
	<?php the_post(); ?>

    <?php wc_get_template_part( 'content', 'single-product' ); ?>

<?php endwhile; // end of the loop. ?>
<?php if($product->get_type() == 'course' ) {
	get_template_part("template-parts/product/related", $product->get_type());
}else{
	get_template_part("template-parts/common/promo");
}?>

<?php get_template_part("template-parts/common/newsletter"); ?>

<?php

get_footer();
