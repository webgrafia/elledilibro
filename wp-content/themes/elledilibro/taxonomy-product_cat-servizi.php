<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package elledilibro
 */

get_header();
$promo_apertura_servizi = get_field("promo_apertura_servizi", "options");

get_template_part("template-parts/common/breadcrumb", "", array("type" => "white"));
?>

    <section class="section section-archive">
        <div class="container">
            <div class="row justify-content-center sticky-sidebar-container">
                <div class="col-lg-8 order-lg-1 col-xl-9 order-xl-1">
                    <div class="block block-double-featured mb-2 mb-md-2 mb-lg-3">
                        <?php
                        foreach ( $promo_apertura_servizi as $item ) {
                            ?>
                            <div class="block block-featured thumb-right">
                                <div class="block-thumb" style="background-image: url('<?php echo $item["immagine"]["sizes"]["half-square"]; ?>');"></div>
                                <div class="block-body">
                                    <div class="block-content">
                                        <h3><a href="<?php echo $item["link"]; ?>"><?php echo $item["titolo"]; ?></a></h3>
	                                    <?php echo $item["testo"]; ?>
                                    </div><!-- /block-content -->
                                    <div class="block-footer">
                                        <p class="price"><?php echo $item["prezzo"]; ?></p>
                                    </div><!-- /block-footer -->
                                </div><!-- /block-body -->
                            </div><!-- /block -->
                            <?php }  ?>
                    </div>
                        <?php
                        $fasce_servizi = get_field("fasce_servizi", "options");
                        foreach ($fasce_servizi as $servizi){
					?>
                    <div class="block block-list block-list-three pb-2 pb-lg-2 pb-xl-3">
                        <div class="section-title text-center">
                            <h2><?php echo $servizi["titolo"]; ?></h2>
                        </div><!-- /section-title -->
                        <div class="row row-list list-services">
	                        <?php
	                        foreach ( $servizi  ["servizi"] as $servizio ) { ?>
                                <div class="col-lg-4 col-sm-6">
                                    <?php get_template_part("template-parts/card/card", "servizio", $servizio); ?>
                                </div><!-- /col-lg-4 -->
		                        <?php
	                        }
	                        ?>
                        </div><!-- /row -->
                    </div><!-- /block -->
    <?php
}
    ?>

                </div><!-- /col-lg-9 -->
                <div class="col-lg-3 order-lg-0">
					<?php  get_sidebar("servizi"); ?>
                </div><!-- /col-lg-3 -->
            </div><!-- /row -->
        </div><!-- /container -->
    </section><!-- /section -->

<?php // get_template_part("template-parts/common/promo"); ?>

<?php get_template_part("template-parts/common/newsletter"); ?>

<?php
get_footer();
