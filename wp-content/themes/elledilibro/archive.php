<?php

get_header();

get_template_part("template-parts/common/breadcrumb", "", array("type" => "full"));

?>

    <section class="section section-archive mt-2 mb-5">
        <div class="container">
            <div class="row justify-content-center sticky-sidebar-container">
                <div class="col-lg-8 col-xl-9">
                    <div class="section-title text-center">
	                    <?php the_archive_title( '<h2>', '</h2>' ); ?>
                    </div><!-- /section-title -->
                    <div class="block block-list block-list-three">
                        <div class="row row-list list-services">
	                        <?php while ( have_posts() ) : the_post(); ?>
                                <div class="col-lg-4 col-sm-6">
			                        <?php get_template_part("template-parts/card/card", $post->post_type, $post); ?>
                                </div>
	                        <?php endwhile; ?>
                        </div><!-- /row -->
						<?php get_template_part("template-parts/archive/paginate"); ?>
                    </div><!-- /block -->
                </div><!-- /col-lg-8 -->
                <div class="col-lg-4 col-xl-3">
                    <?php get_sidebar() ?>
                </div><!-- /col-lg-4 -->
            </div><!-- /row -->
        </div><!-- /container -->
    </section><!-- /section -->

<?php get_template_part("template-parts/common/newsletter"); ?>

<?php
get_footer();


