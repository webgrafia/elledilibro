<?php
/**
 * elledilibro functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package timsm
 */

require get_template_directory() . '/inc/acf.php';
require get_template_directory() . '/inc/cpt.php';
require get_template_directory() . '/inc/utils.php';
require get_template_directory() . '/inc/actions-filters.php';
require get_template_directory() . '/inc/breadcrumbs.php';
require get_template_directory() . '/inc/blocks.php';

//show_admin_bar( false );


add_theme_support( 'post-thumbnails' );

add_theme_support( 'woocommerce' );

/**
 * Immage sizes
 * @return void
 */
function elledilibro_register_custom_image_sizes() {
	add_image_size( 'big-square', 1500, 1500, true );
	add_image_size( 'half-square', 750, 750, true );
	add_image_size( 'medium-square', 500, 500, true );
	add_image_size( 'card-post', 700, 500, true );
	add_image_size( 'card-corso', 1200, 870, true );
	add_image_size( 'card-service', 325, 232, true );
	add_image_size( 'card-book', 400, 999999, false );
	add_image_size( 'single-book', 900, 999999, false );

	add_image_size( 'featured-post', 1500, 900, true );
}
add_action( 'after_setup_theme', 'elledilibro_register_custom_image_sizes' );


/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function elledilibro_widgets_init() {

	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'elledilibro' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'elledilibro' ),
			'before_widget' => '',
			'after_widget'  => '',
			'before_title'  => '<h3>',
			'after_title'   => '</h3>',
		)
	);

    register_sidebar(
        array(
            'name'          => esc_html__( 'Footer 1', 'elledilibro' ),
            'id'            => 'footer-1',
            'description'   => esc_html__( 'Add widgets here.', 'elledilibro' ),
            'before_widget' => '',
            'after_widget'  => '',
            'before_title'  => '<h3>',
            'after_title'   => '</h3>',
        )
    );
    register_sidebar(
        array(
            'name'          => esc_html__( 'Footer 2', 'elledilibro' ),
            'id'            => 'footer-2',
            'description'   => esc_html__( 'Add widgets here.', 'elledilibro' ),
            'before_widget' => '',
            'after_widget'  => '',
            'before_title'  => '<h3>',
            'after_title'   => '</h3>',
        )
    );
    register_sidebar(
        array(
            'name'          => esc_html__( 'Footer 3', 'elledilibro' ),
            'id'            => 'footer-3',
            'description'   => esc_html__( 'Add widgets here.', 'elledilibro' ),
            'before_widget' => '',
            'after_widget'  => '',
            'before_title'  => '<h3>',
            'after_title'   => '</h3>',
        )
    );
}
add_action( 'widgets_init', 'elledilibro_widgets_init' );


/**
 * setup script
 * @return void
 */
function elledilibro_scripts() {

	wp_enqueue_style( 'elledilibro-main-style', get_template_directory_uri() . '/assets/css/main.css', array(), wp_get_theme()->get( 'Version' ) );
	wp_enqueue_style( 'elledilibro-core-style', get_template_directory_uri() . '/style.css', array(), wp_get_theme()->get( 'Version' ) );


	if (!is_admin()) {
		// Remove default WordPress jQuery
		wp_deregister_script('jquery');
		// Register new jQuery script via Google Library
		wp_register_script('jquery', get_template_directory_uri() . '/assets/js/vendor/jquery-3.4.1.min.js', false, wp_get_theme()->get( 'Version' ), true);
		// Enqueue the script
		wp_enqueue_script('jquery');
	}

	wp_enqueue_script( 'elledilibro-main-js', get_template_directory_uri() . '/assets/js/main.js', array("jquery"), wp_get_theme()->get( 'Version' ), true );

}
add_action( 'wp_enqueue_scripts', 'elledilibro_scripts', 100 );


/**
 * setup menu
 */
if ( ! function_exists( 'elledilibro_register_nav_menu' ) ) {
	function elledilibro_register_nav_menu(){
		register_nav_menus( array(
			'primary_menu' => __( 'Primary Menu', 'elledilibro' ),
		//	'burger_menu'  => __( 'Burger Menu', 'elledilibro' ),
		) );
	}
	add_action( 'after_setup_theme', 'elledilibro_register_nav_menu', 0 );
}



/**
 * @param $can_edit
 * @param $post
 *
 * @return false|mixed
 */
function elledilibro_disable_gutenberg( $can_edit, $post ) {
	// disabilito per tutti
	//    return false;

	if( $post->post_type == 'page' &&
	    (strpos(get_page_template_slug( $post->ID ),  'page-home') !== false)
	) {
		return false;
	}
	return $can_edit;
}
add_filter( 'use_block_editor_for_post', 'elledilibro_disable_gutenberg', 10, 2 );

/**
 * @return void
 */
function elle_admin_style() {
	global $pagenow;
	if (( $pagenow == 'post.php' ) && (get_post_type() == 'post')) {
		//wp_enqueue_style( 'admin-style', get_stylesheet_directory_uri() . '/assets/css/main.css' );
	}
}
add_action( 'admin_enqueue_scripts', 'elle_admin_style');

/** EVENTI  **/
function edl_calendario_eventi(){
    if(is_post_type_archive('bookroom')) {
        $events = get_posts([
            'post_type' => 'bookroom',
            'post_status' => 'publish',
            'numberposts' => -1
        ]);

        $dates = [];
        foreach ($events as $event) {
            $data_inizio = get_post_meta($event->ID, 'data_inizio', true);
            $data_fine = get_post_meta($event->ID, 'data_fine', true);

            if($data_fine && $data_inizio) {
                $data_inizio = strtotime($data_inizio); //sta per 10:00:00 il js però da 12:00:00 c'è da capire questa discrepanza +2 (roma timezone?)
                $data_fine = strtotime($data_fine);
                $checking_day = $data_inizio;
                if(time()<= $data_fine) {
                    while($checking_day <= $data_fine) {
                        if($checking_day >= time()) {
                            $dates[] = date('d-m-Y', $checking_day);
                        }
                        $checking_day = strtotime('+1 days', $checking_day);
                    }
                }
            }
        }

        $dates = array_unique($dates);

        wp_register_script('edl_calendario_eventi', get_template_directory_uri(). '/assets/js/eventi.js', ['jquery'], '0.1', true);
        $args = array(
            'permalink' => get_post_type_archive_link('bookroom'),
            'dates' => "['" .implode("', '", $dates) . "']",
            'selected_day' => $_GET['date'] ?: ''
        );
        wp_localize_script('edl_calendario_eventi', 'edl_calendario_config', $args);
        wp_enqueue_script('edl_calendario_eventi');

        $slicknav_custom_css = "
        .caleran-container .caleran-input .caleran-calendars .caleran-calendar .caleran-days-container .caleran-day.caleran-highlight, .caleran-container-mobile .caleran-input .caleran-calendars .caleran-calendar .caleran-days-container .caleran-day.caleran-highlight {
            background: #c8e1ff!important;
            color: #000;
            border-radius: 0!important;
        }
                
        .caleran-selected1 span, .caleran-selected span {
            border-radius: 6px!important;
            background-color: #388ded!important;
            font-weight: bold;
            color:  #fff!important;
        }
        
        .caleran-today span {
            color:  #fff!important;
            background-color: #b2529e!important;
        }
        ";

        //Add the above custom CSS via wp_add_inline_style
        wp_add_inline_style( 'elledilibro-main-style', $slicknav_custom_css ); //Pass the variable into the main style sheet ID
    }
}
add_action( 'wp_enqueue_scripts',  'edl_calendario_eventi', 100 );

function edl_query_eventi( $query ) {
    if( $query->is_main_query() && !$query->is_feed() && !is_admin() && $query->is_post_type_archive( 'bookroom' ) && $_GET['date'] != '') {
        $meta_query = array(
            'relation' => 'AND',
            array(
                'key' => 'data_inizio',
                'value' => $_GET['date'],
                'compare' => '<='
            ),
            array(
                'key' => 'data_fine',
                'value' => $_GET['date'],
                'compare' => '>='
            ),
        );

        $query->set( 'meta_query', $meta_query );
    }

}
add_action( 'pre_get_posts', 'edl_query_eventi' );

/**  WOOCOMMERCE  **/
function edl_remove_product_tabs( $tabs ) {
    unset( $tabs['description'] );      	// Remove the description tab
    unset( $tabs['reviews'] ); 			// Remove the reviews tab
    unset( $tabs['additional_information'] );  	// Remove the additional information tab

    return $tabs;
}
add_filter( 'woocommerce_product_tabs', 'edl_remove_product_tabs', 98 );

function edl_related_products_args( $args ) {
    return [
        'posts_per_page' => 8,
        'columns'        => 0,
        'orderby'        => 'rand', // @codingStandardsIgnoreLine.
    ];
}
add_filter( 'woocommerce_output_related_products_args', 'edl_related_products_args', 1, 20 );

function edl_remove_on_sale_badge( $sale_badge ){
    return '';
}
add_filter( 'woocommerce_sale_flash', 'edl_remove_on_sale_badge' );

if ( ! function_exists( 'edl_template_single_accordion' ) ) {
    /**
     * Output the product accordion.
     */
    function edl_template_single_accordion() {
        wc_get_template( 'single-product/accordion.php' );
    }
}
add_action( 'woocommerce_single_product_summary', 'edl_template_single_accordion', 21 );

function edl_gift_card_enqueue_js() {
    $js = "
    Number.prototype.format = function(n, x, s, c) {
        var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
            num = this.toFixed(Math.max(0, ~~n));

        return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
    };
    
    $('.select-button').each(function() {
        $(this).on('click', function() {
            $('.select-button').removeClass('active');
            $(this).addClass('active');
            $('.edl-amount').val($(this).data('price').format(2, 3, '.', ','));
            $('.add_to_cart_button').removeClass('disabled');
            $('.price-input').removeClass('disabled');
            $('[name=\"edl_variation_id\"]').val($(this).val());
        });
    });";
    wp_add_inline_script( 'wc-gc-main', $js );
}
add_action( 'wp_enqueue_scripts', 'edl_gift_card_enqueue_js' );


function edl_product_more_info() {
    global $post;

    $richiedi_contatto = get_field("richiedi_contatto", $post->ID);
    if($richiedi_contatto) :
    ?>
    <div class="mt-3">
        <a class="btn btn-outline-dark btn-lg w-100" href="#modalcontatta"><?php the_field("titolo_cta_contatto"); ?></a>
    </div>

    <div class="remodal" data-remodal-id="modalcontatta">
        <button data-remodal-action="close" class="remodal-close" aria-label="Close">
            <svg class="svg-close"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-close"></use></svg>
        </button>
        <div class="remodal-body">
            <h3 class="text-center"><?php the_field("titolo_cta_contatto"); ?></h3>
            <?php the_field("testo_modale_contatto"); ?>
        </div>
    </div>
    <?php endif;
}

add_action( 'edl_product_after_add_to_cart_form', 'edl_product_more_info' );
