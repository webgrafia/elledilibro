<?php
$afterurl ="";
if($args["origin"] == "autore"){
    $afterurl = "?post_type=autore";
}
?>
<aside class="sticky-sidebar">
    <div class="card card-border card-categories mt-3 mt-lg-0">
        <div class="card-body">
            <h3>Generi</h3>
            <nav class="menu-categories">
                <ul class="list-simple">
                    <?php
                    $generi = get_terms( array(
	                    'taxonomy' => 'genere',
	                    'hide_empty' => true,
                    ) );
                    foreach ( $generi as $item ) {
                        ?>
                        <li>
                            <a href="<?php echo get_term_link($item).$afterurl; ?>"><?php echo $item->name; ?></a>
                        </li>
                    <?php
                    }
                    ?>
                </ul>
            </nav>
            <?php
            if($args["origin"] == "autore"){
            ?>
            <h3>Ordina per</h3>
            <nav class="menu-sorting">
                <ul class="list-simple">
                    <li>
                        <a href="<?php echo get_post_type_archive_link("autore"); ?>?order=asc">Autore A-Z</a>
                    </li>
                    <li>
                        <a href="<?php echo get_post_type_archive_link("autore"); ?>?order=desc">Autore Z-A</a>
                    </li>
                </ul>
            </nav>
            <?php }else{ ?>
            <h3>Ordina per</h3>
            <nav class="menu-sorting">
                <ul class="list-simple">
                    <li>
                        <a href="<?php echo get_term_link("libri", "product_cat"); ?>?order=asc">Titolo A-Z</a>
                    </li>
                    <li>
                        <a href="<?php echo get_term_link("libri", "product_cat"); ?>?order=desc">Titolo Z-A</a>
                    </li>
                </ul>
            </nav>
            <?php
            }
            ?>
        </div><!-- /card-body -->
    </div><!-- /card -->
</aside>


