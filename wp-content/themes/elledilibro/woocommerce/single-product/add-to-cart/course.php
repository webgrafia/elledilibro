<?php
/**
 * Course product add to cart
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 0.0.1
 */

defined( 'ABSPATH' ) || exit;

global $product;
?>
<div class="col-lg-4 col-xl-3 offset-xl-1" style="position: relative;">
    <aside class="sticky-sidebar" style="">
        <div class="card card-border">
            <div class="card-body">
                <div class="price-simple">
                    <?php echo elle_get_price_html($product); ?>
                </div><!-- /price-simple -->
                <form class="course_form cart mb-0" action="<?php echo esc_url( apply_filters( 'woocommerce_add_to_cart_form_action', $product->get_permalink() ) ); ?>" method="post" enctype='multipart/form-data' data-product_id="<?php echo absint( $product->get_id() ); ?>">
                    <button type="submit" class="btn btn-lg btn-outline-dark w-100 mb-2">Aggiungi al carrello</button>
                    <input type="hidden" name="add-to-cart" value="<?php echo absint( $product->get_id() ); ?>">
                </form>
                <?php
                $numero_lezioni = get_field("numero_lezioni") ?: 0;
                $altre_caratteristiche = get_field("elementi") ?: [];

                $term = elle_get_product_cat( $product->get_id(), 'corso', true ) ?: [];
                ?>
                <ul class="list-text-icons mb-2">
                    <li>
                        <?php
                        $tipologia_icona_corso = get_field("tipologia_icona_corso");
                        switch ($tipologia_icona_corso){
                            case "in-classe":
                            default:
	                        ?>
                                <svg width="100%" height="100%" viewBox="0 0 32 32" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" xmlns:serif="http://www.serif.com/" style="fill-rule:evenodd;clip-rule:evenodd;stroke-linejoin:round;stroke-miterlimit:2;"><rect id="Rectangle-path" x="0" y="0.035" width="32" height="32" style="fill:none;"></rect><path d="M14.471,15.157c2.171,0 3.539,1.368 3.539,3.54l-7.079,-0c0,-2.089 1.264,-3.433 3.293,-3.534l0.247,-0.006Z"></path><path d="M14.471,9.258c1.303,0 2.36,1.057 2.36,2.36c-0,1.303 -1.057,2.359 -2.36,2.359c-1.303,0 -2.36,-1.056 -2.36,-2.359c0,-1.303 1.057,-2.36 2.36,-2.36Z"></path><path d="M26.606,1.168c0.745,-0 1.349,0.602 1.349,1.348l-0,1.348l4.045,-1.348l0,5.394l-4.045,-1.349l-0,1.349c-0,0.745 -0.604,1.348 -1.349,1.348l-5.393,0c-0.744,0 -1.349,-0.603 -1.349,-1.348l0,-5.394c0,-0.746 0.605,-1.348 1.349,-1.348l5.393,-0Z"></path><path d="M17.168,5.213l-0,2.697l-12.136,-0l0,12.135l18.878,0l-0,-8.09l2.696,-0l0,9.22l2.201,6.602c0.251,0.752 0.15,1.525 -0.279,2.12c-0.429,0.594 -1.129,0.935 -1.922,0.935l-24.271,0c-0.793,0 -1.491,-0.342 -1.92,-0.937c-0.429,-0.594 -0.531,-1.366 -0.28,-2.118l2.2,-6.602l0,-14.614c0,-0.745 0.604,-1.348 1.349,-1.348l13.484,-0Zm7.119,17.529l-19.631,-0l-1.797,5.394l23.226,-0l-1.798,-5.394Z"></path><rect x="11.774" y="24.09" width="5.394" height="2.697"></rect></svg>
	                        <?php
                                break;
	                        case "live":
		                        ?>
                                <svg width="100%" height="100%" viewBox="0 0 32 32" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" xmlns:serif="http://www.serif.com/" style="fill-rule:evenodd;clip-rule:evenodd;stroke-linejoin:round;stroke-miterlimit:2;"><rect id="Rectangle-path" x="0" y="0.035" width="32" height="32" style="fill:none;"/><path id="Shape" d="M30.667,8l-4.03,-0c-2.434,-3.231 -6.288,-5.333 -10.637,-5.333c-4.348,-0 -8.203,2.102 -10.637,5.333l-4.03,-0c-0.736,-0 -1.333,0.597 -1.333,1.333l0,13.334c0,0.737 0.597,1.333 1.333,1.333l4.03,0c2.433,3.232 6.289,5.333 10.637,5.333c4.349,0 8.203,-2.101 10.637,-5.333l4.03,0c0.737,0 1.333,-0.596 1.333,-1.333l0,-13.334c0,-0.736 -0.596,-1.333 -1.333,-1.333Zm-7.647,-0l-1.687,-0l0,-1.223c0.604,0.351 1.166,0.764 1.687,1.223Zm-7.02,-2.667c0.921,0 1.812,0.131 2.667,0.352l-0,2.315l-5.334,-0l0,-2.315c0.855,-0.221 1.746,-0.352 2.667,-0.352Zm-5.333,1.444l-0,1.223l-1.686,-0c0.52,-0.459 1.082,-0.872 1.686,-1.223Zm-1.686,17.223l1.686,0l-0,1.224c-0.604,-0.352 -1.166,-0.764 -1.686,-1.224Zm7.019,2.667c-0.921,-0 -1.812,-0.13 -2.667,-0.351l0,-2.316l5.334,-0l-0,2.316c-0.855,0.221 -1.746,0.351 -2.667,0.351Zm5.333,-1.443l0,-1.224l1.687,0c-0.521,0.46 -1.083,0.872 -1.687,1.224Zm8,-3.891l-26.666,0l-0,-10.666l26.666,-0l0,10.666Z" style="fill-rule:nonzero;"/><path id="Path" d="M15.067,19.735l-0,-1.868l-0.934,-0l0,-3.731l0.934,-0l-0,-1.868l-3.734,-0l0,1.868l0.934,-0l-0,3.731l-3.734,-0l0,-5.599l-1.866,-0l-0,7.467l8.4,-0Z" style="fill-rule:nonzero;"/><path id="Path1" serif:id="Path" d="M16.9,19.735l1.919,-0l-0.019,-0.078l0.933,-3.694l0,0.972c0,1.544 1.256,2.8 2.802,2.8l2.798,-0l0,-1.867l-2.8,-0c-0.516,-0 -0.933,-0.419 -0.933,-0.932l2.8,-0l0,-1.867l-2.8,0c0,-0.516 0.417,-0.932 0.933,-0.932l2.8,0l0,-1.866l-2.8,-0c-0.849,-0 -1.602,0.388 -2.116,0.988l0.25,-0.991l-1.92,-0l-0.894,3.64l-0.92,-3.64l-1.866,-0l1.833,7.467Z" style="fill-rule:nonzero;"/></svg>
		                        <?php
		                        break;
	                        case "online":
		                        ?>
                                <svg width="100%" height="100%" viewBox="0 0 32 32" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" xmlns:serif="http://www.serif.com/" style="fill-rule:evenodd;clip-rule:evenodd;stroke-linejoin:round;stroke-miterlimit:2;"><rect id="Rectangle-path" x="0" y="0.035" width="32" height="32" style="fill:none;"/><path d="M14.471,15.157c2.171,0 3.539,1.368 3.539,3.54l-7.079,-0c0,-2.089 1.264,-3.433 3.293,-3.534l0.247,-0.006Z"/><path d="M14.471,9.258c1.303,0 2.36,1.057 2.36,2.36c-0,1.303 -1.057,2.359 -2.36,2.359c-1.303,0 -2.36,-1.056 -2.36,-2.359c0,-1.303 1.057,-2.36 2.36,-2.36Z"/><path d="M26.606,1.168c0.745,-0 1.349,0.602 1.349,1.348l-0,1.348l4.045,-1.348l0,5.394l-4.045,-1.349l-0,1.349c-0,0.745 -0.604,1.348 -1.349,1.348l-5.393,0c-0.744,0 -1.349,-0.603 -1.349,-1.348l0,-5.394c0,-0.746 0.605,-1.348 1.349,-1.348l5.393,-0Z"/><path d="M17.168,5.213l-0,2.697l-12.136,-0l0,12.135l18.878,0l-0,-8.09l2.696,-0l0,9.22l2.201,6.602c0.251,0.752 0.15,1.525 -0.279,2.12c-0.429,0.594 -1.129,0.935 -1.922,0.935l-24.271,0c-0.793,0 -1.491,-0.342 -1.92,-0.937c-0.429,-0.594 -0.531,-1.366 -0.28,-2.118l2.2,-6.602l0,-14.614c0,-0.745 0.604,-1.348 1.349,-1.348l13.484,-0Zm7.119,17.529l-19.631,-0l-1.797,5.394l23.226,-0l-1.798,-5.394Z"/><rect x="11.774" y="24.09" width="5.394" height="2.697"/></svg>
		                        <?php
		                        break;
                        }
                        ?>
                        <p><?php echo get_field("testo_icona_corso")?get_field("testo_icona_corso"):"Corso"; ?></p>
                    </li>
                    <?php if($numero_lezioni) : ?>
                    <li>
                        <svg width="100%" height="100%" viewBox="0 0 32 32" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" xmlns:serif="http://www.serif.com/" style="fill-rule:evenodd;clip-rule:evenodd;stroke-linejoin:round;stroke-miterlimit:2;"><rect id="Rectangle-path" x="0" y="0.035" width="32" height="32" style="fill:none;"></rect><rect x="8" y="8.8" width="4.8" height="3.2"></rect><rect x="4.8" y="13.6" width="8" height="3.2"></rect><rect x="4.8" y="18.4" width="8" height="3.2"></rect><rect x="19.2" y="18.4" width="8" height="3.2"></rect><rect x="19.2" y="13.6" width="8" height="3.2"></rect><rect x="19.2" y="8.8" width="4.8" height="3.2"></rect><path d="M12.8,2.4c0.426,-0 0.832,0.17 1.131,0.469l2.069,2.069l2.069,-2.069c0.299,-0.301 0.705,-0.469 1.131,-0.469l11.2,-0c0.885,-0 1.6,0.718 1.6,1.6l0,20.8c0,0.883 -0.715,1.6 -1.6,1.6l-10.538,-0l-2.731,2.731c-0.312,0.312 -0.721,0.469 -1.131,0.469c-0.41,-0 -0.819,-0.155 -1.131,-0.469l-2.731,-2.731l-10.538,-0c-0.885,-0 -1.6,-0.717 -1.6,-1.6l0,-20.8c0,-0.882 0.715,-1.6 1.6,-1.6l11.2,0Zm-0.662,3.2l-8.938,0l0,17.6l9.6,0c0.426,0 0.832,0.17 1.131,0.47l0.469,0.469l0,-16.275l-2.262,-2.264Zm16.662,0l-8.938,0l-2.262,2.264l0,16.274l0.469,-0.469c0.299,-0.301 0.705,-0.469 1.131,-0.469l9.6,0l0,-17.6Z"></path></svg>
                        <p><?php echo $numero_lezioni; ?> lezioni</p>
                    </li>
                    <?php endif; ?>
                    <?php foreach ($altre_caratteristiche as $key => $caratteristica) {
                        switch ($caratteristica['value']) {
                            case 'icon-content-video':
                                ?>
                                <li>
                                    <svg width="100%" height="100%" viewBox="0 0 32 32" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" xmlns:serif="http://www.serif.com/" style="fill-rule:evenodd;clip-rule:evenodd;stroke-linejoin:round;stroke-miterlimit:2;"><rect id="Rectangle-path" x="0" y="0.035" width="32" height="32" style="fill:none;"></rect><path d="M3.2,6.4l0,22.4l22.4,0l0,3.2l-24,0c-0.885,0 -1.6,-0.717 -1.6,-1.6l-0,-24l3.2,0Z"></path><path d="M30.4,0c0.885,0 1.6,0.717 1.6,1.6l0,22.4c0,0.883 -0.715,1.6 -1.6,1.6l-22.4,-0c-0.885,-0 -1.6,-0.717 -1.6,-1.6l0,-22.4c0,-0.883 0.715,-1.6 1.6,-1.6l22.4,-0Zm-1.6,3.2l-19.2,-0l0,19.2l19.2,-0l0,-19.2Z"></path><path d="M19.2,5.6c3.976,0 7.2,3.224 7.2,7.2c0,3.978 -3.224,7.2 -7.2,7.2c-3.974,0 -7.2,-3.222 -7.2,-7.2c0,-3.976 3.226,-7.2 7.2,-7.2Zm-1.6,4l0,6.4l4.8,-3.2l-4.8,-3.2Z"></path></svg>
                                    <p>Video lezioni</p>
                                </li>
                                <?php
                                break;
                            case 'icon-download':
                                ?>
                                <li>
                                    <svg width="100%" height="100%" viewBox="0 0 32 32" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" xmlns:serif="http://www.serif.com/" style="fill-rule:evenodd;clip-rule:evenodd;stroke-linejoin:round;stroke-miterlimit:2;"><rect id="Rectangle-path" x="0" y="0.035" width="32" height="32" style="fill:none;"></rect><path d="M22.862,-0c0.737,-0 1.333,0.597 1.333,1.333l0,16l-2.666,0l-0,-14.666l-13.334,-0l0,21.333l8,-0l0,2.667l-14.666,-0c-0.738,-0 -1.334,-0.598 -1.334,-1.334l0,-24c0,-0.736 0.596,-1.333 1.334,-1.333l21.333,-0Zm-17.333,2.667l-2.667,-0l-0,21.333l2.667,-0l-0,-21.333Z"></path><path d="M25.529,32l6.276,-6.276l-1.886,-1.885l-3.057,3.057l0,-7.449l-2.667,-0l0,7.449l-3.057,-3.057l-1.885,1.885l6.276,6.276Z"></path></svg>
                                    <p>Materiali scaricabili</p>
                                </li>
                                <?php
                                break;
                            case 'icon-quiz':
                                ?>
                                <li>
                                    <svg width="100%" height="100%" viewBox="0 0 32 32" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" xmlns:serif="http://www.serif.com/" style="fill-rule:evenodd;clip-rule:evenodd;stroke-linejoin:round;stroke-miterlimit:2;"><rect id="Rectangle-path" x="0" y="0.035" width="32" height="32" style="fill:none;"></rect><path d="M29.692,0l1.994,1.993l-4.64,4.64l4.64,4.641l-1.994,1.993l-4.639,-4.641l-4.641,4.641l-1.994,-1.993l4.641,-4.641l-4.641,-4.64l1.994,-1.993l4.641,4.641l4.639,-4.641Z"></path><path d="M11.223,0.178l2.292,1.639l-7.839,10.973l-5.362,-3.575l1.562,-2.345l3.094,2.062l6.253,-8.754Z"></path><path d="M11.407,10.933l4.228,1.409c0.575,0.192 0.964,0.73 0.964,1.338l-0,5.945l4.57,1.143c0.627,0.157 1.067,0.72 1.067,1.367l-0,8.456c-0.001,0.778 -0.633,1.409 -1.411,1.409l-9.865,0c-0.534,0 -1.021,-0.302 -1.261,-0.779l-4.228,-8.456c-0.218,-0.437 -0.194,-0.955 0.062,-1.371c0.257,-0.414 0.711,-0.668 1.2,-0.668l2.818,-0l0,-8.456c0,-0.452 0.217,-0.878 0.586,-1.144c0.368,-0.265 0.84,-0.337 1.27,-0.193Zm0.963,3.293l-0,7.909c-0,0.778 -0.632,1.409 -1.41,1.409l-1.947,0l2.818,5.637l7.585,0l0,-5.945l-4.57,-1.142c-0.627,-0.158 -1.067,-0.721 -1.067,-1.368l0,-6.03l-1.409,-0.47Z"></path></svg>
                                    <p>Quiz</p>
                                </li>
                                <?php
                                break;
                            case 'icon-certificate':
                                ?>
                                <li>
                                    <svg width="100%" height="100%" viewBox="0 0 32 32" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" xmlns:serif="http://www.serif.com/" style="fill-rule:evenodd;clip-rule:evenodd;stroke-linejoin:round;stroke-miterlimit:2;"><rect id="Rectangle-path" x="0" y="0.035" width="32" height="32" style="fill:none;"></rect><path d="M24,14.667c1.473,-0 2.667,1.194 2.667,2.666c-0,1.473 -1.194,2.667 -2.667,2.667c-1.473,-0 -2.667,-1.194 -2.667,-2.667c0,-1.472 1.194,-2.666 2.667,-2.666Z"></path><path d="M24,9.333c4.412,0 8,3.588 8,8c0,2.79 -1.435,5.246 -3.604,6.678l0.924,6.468c0.075,0.52 -0.164,1.036 -0.609,1.317c-0.218,0.136 -0.463,0.204 -0.711,0.204c-0.259,-0 -0.516,-0.075 -0.74,-0.224l-3.26,-2.173l-3.26,2.173c-0.437,0.292 -1.005,0.3 -1.451,0.02c-0.445,-0.281 -0.684,-0.796 -0.609,-1.317l0.924,-6.468c-2.169,-1.432 -3.604,-3.888 -3.604,-6.678c0,-4.412 3.588,-8 8,-8Zm0,2.667c-2.945,-0 -5.333,2.388 -5.333,5.333c-0,2.946 2.388,5.334 5.333,5.334c2.945,-0 5.333,-2.388 5.333,-5.334c0,-2.945 -2.388,-5.333 -5.333,-5.333Z"></path><path d="M21.333,4c0.738,-0 1.334,0.596 1.334,1.333l-0,1.334l-16,-0l-0,8l6.666,-0l0,2.666l-8,0c-0.737,0 -1.333,-0.597 -1.333,-1.333l-0,-10.667c-0,-0.737 0.596,-1.333 1.333,-1.333l16,-0Z"></path><path d="M24,-0c1.472,-0 2.667,1.196 2.667,2.667l-0,4l-2.667,-0l0,-4l-21.333,-0l-0,16l10.666,-0l0,2.666l-10.666,0c-1.472,0 -2.667,-1.196 -2.667,-2.666l0,-16c0,-1.471 1.195,-2.667 2.667,-2.667l21.333,-0Z"></path></svg>
                                    <p>Attestato di partecipazione</p>
                                </li>
                                <?php
                                break;
                        }
                    } ?>
                </ul>
                <?php
                if($luogo = get_field("luogo")){
                ?>
                <hr class="mb-2"/>
                <div class="address-wrapper mb-2"">
                <small>luogo del corso:</small>
                <address>
                    <?php echo $luogo;
                    if($indirizzo = get_field("indirizzo")) echo '<br>'.$indirizzo;
                    ?>
                </address>
            </div>
            <?php } ?>
                <hr class="mb-2">
            <?php
            if($label_regala = get_field("label_regala")){
                ?>
                <a class="btn btn-sm btn-outline-dark w-100 mb-2" href="<?php the_field("link_regala"); ?>"><?php echo $label_regala; ?></a>
	            <?php
            }
            ?>
                <p class="mb-1">puoi pagarlo con:</p>
                <ul class="list-text-icons">
                    <li>
                        <svg width="100%" height="100%" viewBox="0 0 32 32" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" xmlns:serif="http://www.serif.com/" style="fill-rule:evenodd;clip-rule:evenodd;stroke-linejoin:round;stroke-miterlimit:2;"><rect id="Rectangle-path" x="0" y="0.035" width="32" height="32" style="fill:none;"></rect><path d="M27.436,8.75c0.035,-0.219 0.084,-0.184 0.211,-0.092c2.004,1.496 2.138,4.079 1.68,6.534c-1.165,5.892 -5.074,7.924 -10.084,7.924c-1.058,0 -1.743,-0.162 -2.067,1.391c-0.904,5.677 -0.974,6.111 -0.999,6.305l-0.01,0.075c-0.002,0.017 -0.004,0.037 -0.007,0.062c-0.12,0.607 -0.621,1.051 -1.228,1.051l-4.481,0c-0.501,0 -0.819,-0.409 -0.748,-0.91c-0,0 0.318,-2.067 1.912,-11.975c0.057,-0.43 0.339,-0.663 0.769,-0.663c3.811,0 11.615,0.699 14.431,-7.331c0.261,-0.784 0.48,-1.567 0.621,-2.371Zm-5.032,-7.918c4.241,1.645 4.629,5.61 3.105,9.901c-1.517,4.417 -5.116,6.315 -9.886,6.371c-3.062,0.05 -4.904,-0.494 -5.313,1.708c-0.02,0.108 -0.044,0.249 -0.073,0.416l-0.096,0.576c-0.143,0.864 -0.337,2.08 -0.536,3.338l-0.15,0.946c-0.274,1.732 -0.535,3.394 -0.662,4.18c-0.021,0.127 -0.071,0.176 -0.212,0.176l-5.264,0c-0.536,0 -0.924,-0.466 -0.854,-0.981l4.135,-26.243c0.106,-0.677 0.713,-1.192 1.411,-1.192l1.176,-0.001c2.458,-0.004 4.35,-0.021 5.849,-0.026l1.277,-0.001c3.33,0.011 4.333,0.149 6.093,0.832Z"></path></svg>
                        <p>PayPal</p>
                    </li>
                    <li>
                        <svg width="100%" height="100%" viewBox="0 0 32 32" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" xmlns:serif="http://www.serif.com/" style="fill-rule:evenodd;clip-rule:evenodd;stroke-linejoin:round;stroke-miterlimit:2;"><rect id="Rectangle-path" x="0" y="0.035" width="32" height="32" style="fill:none;"></rect><path d="M29.328,3.556c1.478,-0 2.672,1.194 2.672,2.666l0,19.556c0,1.472 -1.194,2.666 -2.672,2.666l-26.656,0c-1.478,0 -2.672,-1.194 -2.672,-2.666l-0,-19.556c-0,-1.472 1.194,-2.666 2.672,-2.666l26.656,-0Zm-0,12.444l-26.656,-0l0,9.444c0,0.184 0.15,0.334 0.334,0.334l25.988,-0c0.184,-0 0.334,-0.15 0.334,-0.334l-0,-9.444Zm-19.328,3.556c0.367,-0 0.667,0.3 0.667,0.666l-0,2.222c-0,0.367 -0.3,0.667 -0.667,0.667l-4,0c-0.367,0 -0.667,-0.3 -0.667,-0.667l0,-2.222c0,-0.366 0.3,-0.666 0.667,-0.666l4,-0Zm10.667,-0c0.366,-0 0.666,0.3 0.666,0.666l0,2.222c0,0.367 -0.3,0.667 -0.666,0.667l-7.556,0c-0.367,0 -0.667,-0.3 -0.667,-0.667l0,-2.222c0,-0.366 0.3,-0.666 0.667,-0.666l7.556,-0Zm8.327,-13.334l-25.988,0c-0.184,0 -0.334,0.15 -0.334,0.334l0,2.333l26.656,-0l-0,-2.333c-0,-0.184 -0.15,-0.334 -0.334,-0.334Z"></path></svg>
                        <p>Carta di credito</p>
                    </li>
                    <li>
                        <svg width="100%" height="100%" viewBox="0 0 32 32" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" xmlns:serif="http://www.serif.com/" style="fill-rule:evenodd;clip-rule:evenodd;stroke-linejoin:round;stroke-miterlimit:2;"><rect id="Rectangle-path" x="0" y="0.035" width="32" height="32" style="fill:none;"></rect><rect x="1.524" y="27.858" width="28.952" height="3.048"></rect><path d="M9.143,14.144c0.843,-0 1.524,0.683 1.524,1.524l-0,9.143c-0,0.842 -0.681,1.523 -1.524,1.523l-4.571,0c-0.842,0 -1.524,-0.681 -1.524,-1.523l-0,-9.143c-0,-0.841 0.682,-1.524 1.524,-1.524l4.571,-0Zm-1.524,3.048l-1.524,-0l0,6.095l1.524,-0l0,-6.095Z"></path><path d="M18.286,14.144c0.842,-0 1.524,0.683 1.524,1.524l-0,9.143c-0,0.842 -0.682,1.523 -1.524,1.523l-4.572,0c-0.841,0 -1.523,-0.681 -1.523,-1.523l-0,-9.143c-0,-0.841 0.682,-1.524 1.523,-1.524l4.572,-0Zm-1.524,3.048l-1.524,-0l0,6.095l1.524,-0l-0,-6.095Z"></path><path d="M27.428,14.144c0.843,-0 1.524,0.683 1.524,1.524l0,9.143c0,0.842 -0.681,1.523 -1.524,1.523l-4.571,0c-0.841,0 -1.524,-0.681 -1.524,-1.523l0,-9.143c0,-0.841 0.683,-1.524 1.524,-1.524l4.571,-0Zm-1.523,3.048l-1.524,-0l-0,6.095l1.524,-0l-0,-6.095Z"></path><path d="M15.185,0.664c0.497,-0.313 1.13,-0.313 1.627,0l14.476,9.143c0.575,0.363 0.84,1.061 0.652,1.713c-0.19,0.652 -0.786,1.1 -1.464,1.1l-28.952,0c-0.678,0 -1.277,-0.448 -1.464,-1.1c-0.188,-0.652 0.077,-1.35 0.649,-1.713l14.476,-9.143Zm0.815,3.092l-9.211,5.817l18.419,-0l-9.208,-5.817Z"></path></svg>
                        <p>Bonifico bancario</p>
                    </li>
                </ul>
            </div><!-- /card-body -->
        </div><!-- /card -->
    </aside>
</div>
