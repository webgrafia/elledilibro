<?php
/**
 * Single product accordions
 *
 * @package WooCommerce\Templates
 * @version 0.0.1
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}

global $post, $product;
?>
<hr/>
<div class="accordion-wrapper">
    <div class="accordion-header">
        <h3>Descrizione completa</h3>
    </div><!-- /accordion-header -->
    <div class="accordion-content">
        <?php the_content(); ?>
    </div><!-- /accordion-content -->

	<?php
	$altre_info = get_field("altre_info") ?: [];
	if (!empty($altre_info)) : ?>
		<?php foreach ($altre_info as $info) : ?>
            <div class="accordion-header">
                <h3><?php echo $info["titolo"]; ?></h3>
            </div>
            <div class="accordion-content">
				<?php echo $info["testo"]; ?>
            </div>
		<?php endforeach; ?>
	<?php endif; ?>

    <?php if($product->get_type() == 'libro') : ?>
    <div class="accordion-header">
        <h3>Commenti</h3>
    </div><!-- /accordion-header -->
    <div class="accordion-content">
        <div class="reviews-wrapper">
            <?php
            // $args = array ('post_type' => 'product', 'post_id' => $product->get_id());
            // $comments = get_comments( $args );
          //  wp_list_comments( array( 'callback' => 'woocommerce_comments' ), $comments);
            ?>

            <?php
            comments_template( 'single-product-reviews', true);
            ?>

        </div><!-- /reviews-wrapper -->
    </div><!-- /accordion-content -->
    <div class="accordion-header">
        <h3>Dettagli</h3>
    </div><!-- /accordion-header -->
    <div class="accordion-content">
        <div class="book-details">
            <?php
            $generi = wp_get_post_terms( $post->ID, 'genere' );
            if($generi){
                ?>
                <div class="book-details-row">
                    <span class="title">Categoria:</span>
                    <span class="body">
                        <?php
                        $c=0;
                        foreach ($generi as $genere){
                            if($c) echo ", ";
                            $category_name  = $genere->name;
                            $category_link  = get_term_link( $genere );
                            ?>
                            <a href="<?php echo esc_url( $category_link ); ?>" title="<?php echo esc_attr( $category_name ); ?>"><?php echo esc_html( $category_name ); ?></a>
                            <?php
                            $c++;
                        }
                        ?>
                       </span>
                </div>
                <?php
            }

            $attributi_prodotto = get_field("attributi_prodotto") ?: [];
            foreach ($attributi_prodotto as $item){
                ?>
                <div class="book-details-row">
                    <span class="title"><?php echo $item["chiave"]; ?>:</span>
                    <span class="body"><?php echo $item["valore"]; ?></span>
                </div><!-- /book-details-row -->
                <?php
            }
            ?>
        </div><!-- /book-details -->
    </div><!-- /accordion-content -->
    <?php endif; ?>

    <?php  if((($product->get_type() == 'servizio') || ($product->get_type() == 'pacchetto')) &&  ($abilita_modale = get_field("abilita_modale"))): ?>
    <div class="configurator-title mb-1">
        <h3><?php the_field("titolo_modale"); ?></h3>
        <div class="configurator-info">
            <a class="trigger-info tippy-purple" data-tippy-content="<?php the_field("titolo_modale"); ?>" href="#modal01">
                <svg width="100%" height="100%" viewBox="0 0 32 32" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" xmlns:serif="http://www.serif.com/" style="fill-rule:evenodd;clip-rule:evenodd;stroke-linejoin:round;stroke-miterlimit:2;"><rect id="Rectangle-path" x="0" y="0.035" width="32" height="32" style="fill:none;"/><path d="M0,16c0,8.837 7.163,16 16,16c8.837,-0 16,-7.163 16,-16c-0,-8.837 -7.163,-16 -16,-16c-8.837,0 -16,7.163 -16,16Zm3.171,-0c0,-7.085 5.744,-12.829 12.829,-12.829c7.085,0 12.829,5.744 12.829,12.829c-0,7.085 -5.744,12.829 -12.829,12.829c-7.085,-0 -12.829,-5.744 -12.829,-12.829Zm14.415,8.008l-0,-6.406c-0,-0.876 -0.711,-1.586 -1.586,-1.586c-0.875,0 -1.586,0.71 -1.586,1.586l0,6.406c0,0.875 0.711,1.586 1.586,1.586c0.875,-0 1.586,-0.711 1.586,-1.586Zm-4.789,-14.414c-0,1.769 1.434,3.203 3.203,3.203c1.769,-0 3.203,-1.434 3.203,-3.203c0,-1.769 -1.434,-3.204 -3.203,-3.204c-1.769,0 -3.203,1.435 -3.203,3.204Z"/></svg>
            </a>
            <div class="remodal" data-remodal-id="modal01">
                <button data-remodal-action="close" class="remodal-close" aria-label="Close">
                    <svg class="svg-close"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-close"></use></svg>
                </button>
                <div class="remodal-body">
                    <h3 class="text-center"><?php the_field("titolo_modale"); ?></h3>
	                <?php the_field("testo_modale"); ?>
                    <div class="d-flex justify-content-center">
                        <a data-remodal-action="close" class="btn btn-outline-dark btn-lg px-3" href="#">Ok, ho capito</a>
                    </div>
                </div>
            </div>
        </div><!-- /configurator-info -->
    </div><!-- /configurator-title -->
    <?php endif ; ?>
</div><!-- /accordion-wrapper -->
