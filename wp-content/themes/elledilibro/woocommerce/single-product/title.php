<?php
/**
 * Single Product title
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/title.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see        https://docs.woocommerce.com/document/template-structure/
 * @package    WooCommerce\Templates
 * @version    1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
global $product;

if($product->get_type() == 'course') {
    ?>
    <div class="row justify-content-center">
        <div class="col-lg-12 col-xl-10">
            <div class="main-title">
                <?php the_title( '<h1 class="product_title entry-title">', '</h1>' ); ?>
                <?php get_template_part("template-parts/common/share"); ?>
            </div><!-- /main-title -->
        </div><!-- /col-lg-12 -->
    </div>
    <?php
} else {
    the_title( '<h1 class="product_title entry-title">', '</h1>' );
    if($product->get_type() == 'libro') {
        get_template_part("template-parts/single/author", "product");
    }
}
