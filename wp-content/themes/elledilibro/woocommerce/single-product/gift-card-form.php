<?php
/**
 * Gift Card Form
 *
 * Shows the additional form fields on the product page.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/gift-card-form.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce Gift Cards
 * @version 1.6.0
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

do_action( 'woocommerce_gc_before_form', $product );
?>
    <div class="woocommerce_gc_giftcard_form">
        <div class="purchase-configurator pt-1">
            <div class="purchase-configurator-top">
                <?php
                // Get Available variations?
                $get_variations = count( $product->get_children() ) <= apply_filters( 'woocommerce_ajax_variation_threshold', 30, $product );
                $available_variations = $get_variations ? $product->get_available_variations() : false;
                $attributes = $product->get_variation_attributes();
                $attributes = $product->get_variation_attributes();

                foreach ( $attributes as $attribute_name => $options ) :?>
                <div class="form-group configurator-row">
                    <label class="form-label" for="wc_gc_giftcard_to"><?php echo $attribute_name; ?></label>
                    <?php
                    $item = false;
                    foreach ($options as $option) {
                        foreach ($available_variations as $variation) {
                            foreach ($variation['attributes'] as $slug => $value) {
                                if($value == $option) {
                                    $item = $variation;
                                    break;
                                }
                            }
                        }

                        if($item) { ?>
                        <button type="button" class="select-button" value="<?php echo esc_attr($item['variation_id']); ?>" data-price="<?php echo esc_attr($item['display_price']); ?>" data-regular_price="<?php echo esc_attr($item['display_regular_price']); ?>"><?php echo $option; ?></button>
                        <?php }
                   } ?>
                </div>
                <?php endforeach; ?>
            </div>
            <div class="purchase-configurator-top">
                <div class="form-group configurator-row">
                    <?php
                    // Re-fill form.
                    $to = isset( $_REQUEST[ 'wc_gc_giftcard_to' ] ) ? sanitize_text_field( $_REQUEST[ 'wc_gc_giftcard_to' ] ) : '';
                    $to = empty( $to ) && isset( $_REQUEST[ 'wc_gc_giftcard_to_multiple' ] ) ? sanitize_text_field( $_REQUEST[ 'wc_gc_giftcard_to_multiple' ] ) : $to;

                    if ( $product->is_sold_individually() ) { ?>
                    <label class="form-label" for="wc_gc_giftcard_to">Regalo per</label>
                    <input type="text" name="wc_gc_giftcard_to" id="wc_gc_giftcard_to" class="form-control form-control-lg" placeholder="ES: Mario Rossi" value="<?php echo esc_attr( $to ); ?>" />
                    <?php } else {?>
                    <label class="form-label" for="wc_gc_giftcard_to_multiple">Regalo per</label>
                    <input type="text" name="wc_gc_giftcard_to_multiple" id="wc_gc_giftcard_to_multiple" class="form-control form-control-lg" placeholder="<?php echo sprintf( 'Inserisci gli indirizzi email dei destinatari separati da virgola (%s)', wc_gc_get_emails_delimiter() ) ?>" value="<?php echo esc_attr( $to ); ?>"/>
                    <?php } ?>
                </div><!-- /form-group -->
            </div><!-- /purchase-configurator-top -->
            <div class="purchase-configurator-top">
                <div class="form-group configurator-row">
                    <?php
                    $from = isset( $_REQUEST[ 'wc_gc_giftcard_from' ] ) ? sanitize_text_field( wp_unslash( urldecode( $_REQUEST[ 'wc_gc_giftcard_from' ] ) ) ) : ''; // @phpcs:ignore WordPress.Security.ValidatedSanitizedInput.InputNotSanitized

                    if ( empty( $from ) && get_current_user_id() ) {
                        $customer_id = apply_filters( 'woocommerce_checkout_customer_id', get_current_user_id() );
                        $customer    = new WC_Customer( $customer_id );
                        if ( is_a( $customer, 'WC_Customer' ) ) {

                            if ( is_email( $customer->get_display_name() ) || $customer->get_display_name() === $customer->get_username() ) {
                                $customer->set_display_name( $customer->get_first_name() . ' ' . $customer->get_last_name() );
                            }

                            $from = ! empty( trim( $customer->get_display_name() ) ) ? $customer->get_display_name() : '';
                        }
                    }
                    ?>
                    <label class="form-label">Da</label>
                    <input type="text" class="form-control form-control-lg" name="from" placeholder="Inserisci il tuo nome" value="<?php echo esc_attr( $from ); ?>">
                </div><!-- /form-group -->
            </div>
            <div class="purchase-configurator-top">
                <div class="form-group configurator-row">
                    <div class="wc_gc_field wc_gc_giftcard_message">
                        <?php
                        $message = isset( $_REQUEST[ 'wc_gc_giftcard_message' ] ) ? sanitize_textarea_field( str_replace( '<br />', "\n", wp_unslash( urldecode( $_REQUEST[ 'wc_gc_giftcard_message' ] ) ) ) ) : ''; // @phpcs:ignore WordPress.Security.ValidatedSanitizedInput.InputNotSanitized
                        ?>
                        <label for="wc_gc_giftcard_message" class="form-label">Message</label>
                        <textarea class="form-control form-control-lg" rows="3" name="wc_gc_giftcard_message" id="wc_gc_giftcad_message" placeholder="Inserisci un messaggio (opzionale)"><?php echo esc_html( $message ); ?></textarea>
                    </div>
                </div>
            </div><!-- /purchase-configurator-top -->
            <div class="purchase-configurator-top">
                <div class="form-group configurator-row">
                    <div class="wc_gc_field wc_gc_giftcard_delivery">
                        <?php
                        $deliver_date = isset( $_REQUEST[ 'wc_gc_giftcard_delivery' ] ) ? absint( $_REQUEST[ 'wc_gc_giftcard_delivery' ] ) : '';

                        // Check for valid date and reset.
                        if ( $deliver_date < strtotime( 'tomorrow' ) ) {
                            $deliver_date = '';
                        }

                        if ( $deliver_date && isset( $_REQUEST[ '_wc_gc_giftcard_delivery_gmt_offset' ] ) ) {
                            $deliver_date = wc_gc_convert_timestamp_to_gmt_offset( $deliver_date, -1 * (float) $_REQUEST[ '_wc_gc_giftcard_delivery_gmt_offset' ] );
                        }
                        ?>
                        <label for="wc_gc_giftcard_delivery" class="form-label">Data di consegna</label>
                        <input autocomplete="off" readonly type="text" class="datepicker form-control form-control-lg" placeholder="<?php esc_attr_e( 'Subito', 'woocommerce-gift-cards' ); ?>" value="<?php echo $deliver_date ? esc_attr( date_i18n( get_option( 'date_format' ), $deliver_date ) ) : ''; ?>" />
                        <input autocomplete="off" type="hidden" name="wc_gc_giftcard_delivery" />
                        <input autocomplete="off" type="hidden" name="_wc_gc_giftcard_delivery_gmt_offset" />
                        <?php echo wp_kses_post( apply_filters( 'woocommerce_gc_reset_delivery_date_link', '<a class="reset_delivery_date" href="#">' . esc_html__( 'Clear', 'woocommerce-gift-cards' ) . '</a>' ) ); ?>
                    </div>
                </div>
            </div><!-- /purchase-configurator-top -->
            <div class="purchase-configurator-bottom">
                <div class="price-group">
                    <label class="price-label">prezzo finale</label>
                    <div class="price-input<?php echo $product->get_type() != 'simple' ? ' disabled' : ''; ?>">
                        <span>€</span>
                        <?php if( $product->get_type() != 'simple') : ?>
                        <input type="text" class="form-control form-control-lg edl-amount" readonly="" value="0,00">
                        <?php else: ?>
                        <input type="text" class="form-control form-control-lg edl-amount" value="<?php echo $product->get_price(); ?>">
                        <?php endif; ?>
                    </div><!-- /price-input -->
                </div><!-- /price-group -->
                <?php if( $product->get_type() != 'simple') : ?>
                <button class="btn btn-lg btn-outline-dark add_to_cart_button disabled"><?php echo esc_html( $product->single_add_to_cart_text() ); ?></button>
                <?php else: ?>
                <button type="submit" name="add-to-cart" value="<?php echo esc_attr( $product->get_id() ); ?>" class="btn btn-lg btn-outline-dark add_to_cart_button"><?php echo esc_html( $product->single_add_to_cart_text() ); ?></button>
                <?php endif; ?>
            </div><!-- /purchase-configurator-bottom -->
        </div>
    </div>
<?php

do_action( 'woocommerce_gc_after_form', $product );
