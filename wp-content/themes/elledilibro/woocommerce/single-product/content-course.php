<?php
/**
 * Single product course
 *
 * @package WooCommerce\Templates
 * @version 0.0.1
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>
    <div class="block block-title-line">
        <h3>Descrizione</h3>
    </div><!-- /block-title-line -->
    <div class="block block-text text-normal">
		<?php the_content(); ?>
    </div><!-- /block-text -->

<?php
$altre_info = get_field("altre_info") ?: [];
if (!empty($altre_info)) : ?>
	<?php foreach ($altre_info as $info) : ?>
        <div class="block block-title-line">
            <h3><?php echo $info["titolo"]; ?></h3>
        </div>
        <div class="block block-text text-normal">
			<?php echo $info["testo"]; ?>
        </div>
	<?php endforeach; ?>
<?php endif; ?>


<?php
$docenti = get_field("docenti") ?: [];
if (!empty($docenti)) : ?>
    <div class="block block-title-line">
        <h3>Docenti</h3>
    </div><!-- /block-title-line -->
    <div class="block block-carousel-persons">
        <div class="owl-carousel owl-theme carousel-single-nav">
			<?php foreach ($docenti as $docente) : ?>
                <div class="item">
                    <div class="card card-person">
                        <div class="card-thumb">
                            <img src="<?php echo $docente["immagine"]["sizes"]["medium-square"]; ?>" >
                        </div><!-- /card-thumb -->
                        <div class="card-body">
                            <h3><?php echo $docente["nome"]; ?></h3>
							<?php echo $docente["bio"]; ?>
                        </div><!-- /card-body -->
                    </div><!-- /card -->
                </div><!-- /item -->
			<?php endforeach; ?>
        </div>
    </div>
<?php endif; ?>

<?php
$lezioni = get_field("lezione") ?: [];
if (!empty($lezioni)) :
	?>
    <div class="block block-title-line">
        <h3>Programma</h3>
    </div><!-- /block-title-line -->
    <div class="block block-repeater-program">
		<?php foreach ($lezioni as $index => $lezione) : ?>
            <div class="card card-program bg-gray-white">
                <div class="card-body">
                    <h4><?php if($lezione["numero_lezione"]) echo '<strong>'.$lezione["numero_lezione"].':</strong> '; ?><?php echo $lezione['titolo_lezione']; ?></h4>
					<?php if($lezione['descrizione_lezione']) echo '<p>'.$lezione['descrizione_lezione'].'</p>'; ?>
					<?php if($lezione['giorno_lezione'] || $lezione['ora_lezione']){ ?>
                        <div class="program-date-time">
							<?php if($lezione['giorno_lezione']){ ?>
                                <span class="date"><?php echo $lezione['giorno_lezione']; ?></span>
							<?php } ?>
							<?php if($lezione['ora_lezione']){ ?>
                                <span class="time"><?php echo $lezione['ora_lezione']; ?></span>
							<?php } ?>

                        </div><!-- /program-date-time -->
					<?php } ?>
	<?php if($lezione['cta']){ ?>
                    <a class="btn btn-outline-dark btn-sm px-2 mt-1" href="<?php echo $lezione['link']; ?>"><?php echo $lezione['cta']; ?></a>
	<?php } ?>
                </div><!-- /card-body -->
            </div><!-- /card -->
		<?php endforeach; ?>
    </div><!-- /block-repeater-program -->
<?php endif; ?>
<?php
$informazioni = get_field("informazione") ?: [];
if (!empty($informazioni)) :
	?>
    <div class="block block-title-line">
        <h3>Informazioni sul corso</h3>
    </div><!-- /block-title-line -->
    <div class="block block-accordion-simple">
		<?php foreach ($informazioni as $informazione) : ?>
            <div class="accordion-header">
                <h3><?php echo $informazione['titolo']; ?></h3>
            </div><!-- /accordion-header -->
            <div class="accordion-content">
                <p><?php echo $informazione['descrizione']; ?></p>
            </div><!-- /accordion-content -->
		<?php endforeach; ?>
    </div><!-- /block-accordion-simple -->
<?php endif; ?>