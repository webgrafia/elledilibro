<?php
/**
 * Single Product Image
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-image.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.5.1
 */

defined( 'ABSPATH' ) || exit;


global $product;
?>
<?php if($product->get_type() == 'course' ) :
	$tipologia_apertura = get_field("tipologia_apertura");
	if($tipologia_apertura == "podcast"){
		$subclass = $tipologia_apertura;
		$content = get_field("embed");
	}else if($tipologia_apertura == "video"){
		$subclass = $tipologia_apertura;
		$content = wp_oembed_get(get_field("url_video"));
	}else{
		$subclass = "image";
		$content = woocommerce_get_product_thumbnail("single-book");
	}

    ?>
    <div class="block block-main-<?php echo $subclass; ?> block-content-<?php echo $subclass; ?> ">
        <?php echo $content; ?>
    </div>
<?php else: ?>
    <div class="col-lg-5 order-lg-0 col-xl-4 order-xl-0">
        <aside class="sticky-sidebar">
            <div class="book-featured-image mt-3 mt-lg-0">
                <?php echo woocommerce_get_product_thumbnail("single-book"); ?>
            </div><!-- /featured-image -->
            <div class="book-gallery lightgallery book-gallery-first-hidden">
                <?php
                $product_image_id = $product->get_image_id();
                ?>
                <a href="<?php echo wp_get_attachment_image_url( $product_image_id, "full"); ?>">
                    <?php echo wp_get_attachment_image( $product_image_id, "card-book"); ?>
                </a>
                <?php
                $attachment_ids = $product->get_gallery_image_ids();
                if ( $attachment_ids && $product->get_image_id() ) {
                    foreach ( $attachment_ids as $attachment_id ) {
                        ?>
                        <a href="<?php echo wp_get_attachment_image_url( $attachment_id, "full"); ?>">
                            <?php echo wp_get_attachment_image( $attachment_id, "card-book"); ?>
                        </a>
                        <?php
                    }
                }
                ?>
            </div><!-- /book-gallery -->
	        <?php if($product->get_type() == 'libro' ) : ?>
            <div class="d-flex pt-2">
                <?php if($estratto_ipub_pdf = get_field("estratto_ipub_pdf")): ?>
                <a class="link-icon" href="<?php echo $estratto_ipub_pdf; ?>" target="_blank">
                    <svg width="100%" height="100%" viewBox="0 0 32 32" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" xmlns:serif="http://www.serif.com/" style="fill-rule:evenodd;clip-rule:evenodd;stroke-linejoin:round;stroke-miterlimit:2;"><rect id="Rectangle-path" x="0" y="0.035" width="32" height="32" style="fill:none;"></rect><path d="M17.586,31.242l-0,-22.621c-0,-2.311 -1.317,-4.055 -3.37,-5.283c-2.495,-1.493 -6.157,-2.19 -8.92,-2.483c-1.35,-0.143 -2.698,0.293 -3.708,1.201c-1.01,0.907 -1.587,2.201 -1.588,3.559l0,16.171c-0.01,2.477 1.88,4.556 4.353,4.782c0.001,-0 0.001,0 0.001,0c2.341,0.212 5.717,0.702 8.037,1.999c1.13,0.632 2.023,1.44 2.023,2.675l3.172,-0Zm-3.172,-5.16c-0.154,-0.098 -0.313,-0.192 -0.476,-0.283c-2.689,-1.503 -6.584,-2.144 -9.298,-2.389c-0.834,-0.076 -1.472,-0.779 -1.469,-1.618l0,-16.175c0,-0.459 0.195,-0.896 0.537,-1.203c0.341,-0.307 0.796,-0.454 1.252,-0.405c0.001,-0 0.001,-0 0.001,-0c2.357,0.249 5.499,0.777 7.627,2.05c1.03,0.617 1.826,1.402 1.826,2.562l0,17.461Z"></path><path d="M14.414,8.621c0,-2.311 1.317,-4.055 3.37,-5.283c2.495,-1.493 6.157,-2.19 8.92,-2.483c1.35,-0.143 2.698,0.293 3.708,1.201c1.01,0.907 1.587,2.201 1.588,3.559c-0,0.001 -0,0.001 -0,0.001c-0,0 -0,16.17 -0,16.163c0.01,2.484 -1.88,4.563 -4.353,4.789c-0.001,-0 -0.001,0 -0.001,0c-2.341,0.212 -5.717,0.702 -8.037,1.999c-1.13,0.632 -2.023,1.44 -2.023,2.675c-0,-0 -3.172,-0 -3.172,-0l0,-22.621Zm3.172,17.461l-0,-17.461c-0,-1.16 0.796,-1.945 1.826,-2.562c2.128,-1.273 5.27,-1.801 7.627,-2.05c-0,-0 0,-0 0.001,-0c0.456,-0.049 0.911,0.098 1.252,0.405c0.342,0.307 0.537,0.744 0.537,1.203l-0,16.175c0.003,0.839 -0.635,1.542 -1.471,1.618c-2.712,0.245 -6.607,0.886 -9.296,2.389c-0.163,0.091 -0.322,0.185 -0.476,0.283Z"></path></svg>
                    <span>Inizia a leggere</span>
                </a>
                <?php endif; ?>
	            <?php if($estratto_audio = get_field("estratto_audio")): ?>
                <a class="link-icon ms-3" href="<?php echo $estratto_audio ?>">
                    <svg width="100%" height="100%" viewBox="0 0 32 32" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" xmlns:serif="http://www.serif.com/" style="fill-rule:evenodd;clip-rule:evenodd;stroke-linejoin:round;stroke-miterlimit:2;"><rect id="Rectangle-path" x="0" y="0.035" width="32" height="32" style="fill:none;"></rect><path d="M10.779,7.682l-4.388,0c-2.999,0 -5.429,2.431 -5.429,5.429l-0,5.766c-0,2.998 2.43,5.429 5.429,5.429l4.387,0c0,0 12.552,7.098 12.552,7.098c0.023,0.013 0.046,0.025 0.07,0.037c1.638,0.821 3.586,0.733 5.143,-0.233c1.558,-0.966 2.502,-2.671 2.495,-4.504c0,0.006 0,-21.426 0,-21.426c0.002,-1.829 -0.945,-3.528 -2.502,-4.49c-1.556,-0.962 -3.5,-1.049 -5.136,-0.229c-0.024,0.012 -0.047,0.024 -0.069,0.037l-12.552,7.086Zm0.416,3.171c0.274,0 0.542,-0.07 0.78,-0.204l12.876,-7.27c0.646,-0.311 1.407,-0.271 2.018,0.106c0.621,0.384 0.999,1.062 0.998,1.792l0,21.439c0.003,0.731 -0.374,1.412 -0.995,1.797c-0.607,0.376 -1.363,0.419 -2.007,0.115c-0.014,-0.007 -12.889,-7.288 -12.889,-7.288c-0.238,-0.134 -0.507,-0.205 -0.781,-0.205l-4.804,0c-1.247,0 -2.258,-1.011 -2.258,-2.258c-0,-0 -0,-5.766 -0,-5.766c-0,-1.247 1.011,-2.258 2.258,-2.258c-0,0 4.804,0 4.804,0Z"></path><path d="M9.61,9.268l-0,13.453c-0,0.875 0.71,1.585 1.585,1.585c0.876,0 1.586,-0.71 1.586,-1.585l-0,-13.453c-0,-0.875 -0.71,-1.586 -1.586,-1.586c-0.875,0 -1.585,0.711 -1.585,1.586Z"></path></svg>
                    <span>Ascolta l'estratto</span>
                </a>
	            <?php endif; ?>
            </div>
	        <?php endif; ?>


        </aside>
    </div><!-- /col-lg-4 -->
<?php endif; ?>
