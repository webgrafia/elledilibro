<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package elledilibro
 */

get_header();

get_template_part("template-parts/common/breadcrumb", "product");

global $product;
?>
<?php while ( have_posts() ) : ?>
	<?php the_post(); ?>
    <main role="main" class="book-wrapper">
        <div class="container">
            <div class="row justify-content-center sticky-sidebar-container">
                <div class="col-lg-7 order-lg-1 col-xl-5 offset-xl-1 order-xl-1">
                    <h1><?php the_title(); ?></h1>
					<?php get_template_part("template-parts/single/author", "product"); ?>
                    <div class="book-utilities mb-2">
                        <div class="rating-wrapper">
							<?php get_template_part("template-parts/single/rating", "", $product); ?>
                        </div><!-- /rating-wrapper -->
						<?php get_template_part("template-parts/common/share"); ?>
                    </div><!-- /book-utilities -->
                    <div class="book-price-wrapper">
						<?php echo elle_get_price_html($product); ?>
                    </div><!-- /book-price-wrapper -->
                    <div class="book-short-description">
						<?php the_excerpt(); ?>
                    </div><!-- /book-short-description -->
                    <hr/>
                    <div class="accordion-wrapper">
                        <div class="accordion-header">
                            <h3>Descrizione completa</h3>
                        </div><!-- /accordion-header -->
                        <div class="accordion-content">
							<?php the_content(); ?>
                        </div><!-- /accordion-content -->
                        <div class="accordion-header">
                            <h3>Commenti</h3>
                        </div><!-- /accordion-header -->
                        <div class="accordion-content">
                            <div class="reviews-wrapper">
								<?php
								$args = array ('post_type' => 'product', 'post_id' => $product->get_id());
								$comments = get_comments( $args );
								wp_list_comments( array( 'callback' => 'woocommerce_comments' ), $comments);
								?>
                            </div><!-- /reviews-wrapper -->
                        </div><!-- /accordion-content -->
                        <div class="accordion-header">
                            <h3>Dettagli</h3>
                        </div><!-- /accordion-header -->
                        <div class="accordion-content">
                            <div class="book-details">
								<?php
								$generi = wp_get_post_terms( $post->ID, 'genere' );
								if($generi){
									?>
                                    <div class="book-details-row">
                                        <span class="title">Categoria:</span>
                                        <span class="body"><?php

											$c=0;
											foreach ($generi as $genere){
												if($c) echo ", ";
												$category_name  = $genere->name;
												$category_link  = get_term_link( $genere );
												?>
                                                <a href="<?php echo esc_url( $category_link ); ?>" title="<?php echo esc_attr( $category_name ); ?>"><?php echo esc_html( $category_name ); ?></a>
												<?php
												$c++;
											}
											?>
                                        </span>
                                    </div>
									<?php
								}

                                $attributi_prodotto = get_field("attributi_prodotto") ?: [];
                                foreach ($attributi_prodotto as $item){
                                    ?>
                                    <div class="book-details-row">
                                        <span class="title"><?php echo $item["chiave"]; ?>:</span>
                                        <span class="body"><?php echo $item["valore"]; ?></span>
                                    </div><!-- /book-details-row -->
                                        <?php
                                }
								?>
                            </div><!-- /book-details -->
                        </div><!-- /accordion-content -->
                    </div><!-- /accordion-wrapper -->
                    <div class="purchase-configurator">
                        <?php do_action( 'woocommerce_template_single_add_to_cart' ); ?>
                    </div>
                </div><!-- /col-lg-7 -->
                <div class="col-lg-5 order-lg-0 col-xl-4 order-xl-0">
                    <aside class="sticky-sidebar">
                        <div class="book-featured-image mt-3 mt-lg-0">
							<?php echo woocommerce_get_product_thumbnail("single-book"); ?>
                        </div><!-- /featured-image -->
                        <div class="book-gallery lightgallery book-gallery-first-hidden">
							<?php
							$attachment_ids = $product->get_gallery_image_ids();
							if ( $attachment_ids && $product->get_image_id() ) {
								foreach ( $attachment_ids as $attachment_id ) {
									?>
                                    <a href="<?php echo wp_get_attachment_image_url( $attachment_id, "full"); ?>">
										<?php echo wp_get_attachment_image( $attachment_id, "card-book"); ?>
                                    </a>
									<?php
								}
							}
							?>
                        </div><!-- /book-gallery -->
                    </aside>
                </div><!-- /col-lg-4 -->
            </div><!-- /row -->
        </div><!-- /container -->
    </main><!-- /main -->

<?php endwhile; // end of the loop. ?>

<?php get_template_part("template-parts/common/promo"); ?>

<?php get_template_part("template-parts/common/newsletter"); ?>

<?php

get_footer();
