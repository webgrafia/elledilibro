<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package elledilibro
 */

get_header();

get_template_part("template-parts/common/breadcrumb", "", array("type" => "white"));
while ( have_posts() ) : the_post();

	?>
    <main role="main" class="author-page-wrapper">
        <div class="container">
            <div class="row justify-content-center sticky-sidebar-container">
                <div class="col-lg-9 order-lg-1">
                    <div class="author-main">
                        <article class="author-container">
                            <h1><?php the_title(); ?></h1>
							<?php the_content(); ?>
							<?php
							$libri = elle_get_libri($args);
							if($libri){
								?>
                                <div class="block block-books">
                                    <div class="section-title text-center mb-2">
                                        <h2>Libri dell’autore</h2>
                                    </div><!-- /section-title -->
                                    <div class="owl-carousel owl-theme carousel-three-items">
										<?php
										foreach ($libri as $libro){
											get_template_part("template-parts/card/card", "libro", $libro);
										}
										?>
                                    </div><!-- /carousel-three-items -->
                                </div><!-- /block -->
								<?php
							}
							?>
                        </article>
						<?php get_template_part("template-parts/common/share", "floating"); ?>
                    </div><!-- /author-main -->
                </div><!-- /col-lg-9 -->
                <div class="col-lg-3 order-lg-0">
                    <aside class="sticky-sidebar">
                        <div class="card card-author">
                            <div class="card-thumb">
								<?php the_post_thumbnail( $post->ID, 'medium-square' ); ?>
                            </div><!-- /card-thumb -->
                            <div class="card-body p-1">
                                <div class="social-links">
									<?php
									$facebook = get_field("facebook", $post);
									if($facebook){
										?>
                                        <a href="<?php echo $facebook; ?>">
                                            <svg class="svg-social-facebook"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-social-facebook"></use></svg>
                                        </a>
										<?php
									}
									$instagram = get_field("instagram", $post);
									if($instagram){
										?>
                                        <a href="<?php echo $instagram; ?>">
                                            <svg class="svg-social-instagram"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-social-instagram"></use></svg>
                                        </a>
										<?php
									}
									$linkedin = get_field("linkedin", $post);
									if($linkedin){
										?>
                                        <a href="<?php echo $linkedin; ?>">
                                            <svg class="svg-social-linkedin"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-social-linkedin"></use></svg>
                                        </a>
										<?php
									}
									?>
	                                <?php the_favorites_button($args->ID); ?>

                                </div><!-- /social-links -->
                            </div><!-- /card-body -->
                        </div><!-- /card -->
                    </aside>
                </div><!-- /col-lg-3 -->
            </div><!-- /row -->
        </div><!-- /container -->
    </main><!-- /main -->

<?php get_template_part("template-parts/single/related", get_post_type(), $post); ?>

<?php get_template_part("template-parts/common/promo"); ?>

<?php get_template_part("template-parts/common/newsletter"); ?>

<?php
endwhile;

get_footer();
