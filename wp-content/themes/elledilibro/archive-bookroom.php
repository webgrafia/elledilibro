<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package elledilibro
 */

get_header();

get_template_part("template-parts/common/breadcrumb", "", array("type" => "white"));

?>

    <section class="section section-archive mb-5">
        <div class="container">
            <div class="row justify-content-center sticky-sidebar-container">
                <div class="col-lg-4 col-xl-3">
	                <?php  get_sidebar("bookroom", array("origin" => $_REQUEST["post_type"])); ?>
                </div><!-- /col-lg-4 -->
                <div class="col-lg-8 col-xl-9">
                    <?php
                    $eventi_in_evidenza = get_field("eventi_in_evidenza", "options");
                    if($eventi_in_evidenza){
                    ?>
                    <div class="block block-double-featured">
                        <?php
                        foreach ( $eventi_in_evidenza as $item ) {
                            ?>
                            <div class="block block-featured thumb-right">
                                <div class="block-thumb" style="background-image: url('<?php echo $item["immagine"]["sizes"]["half-square"]; ?>');"></div>
                                <div class="block-body">
                                    <div class="block-content">
                                        <h3><a href="<?php echo $item["link"]; ?>"><?php echo $item["titolo"]; ?></a></h3>
	                                    <?php echo $item["testo"]; ?>
                                    </div><!-- /block-content -->
                                    <?php
                                    if($item["data"]){
                                    ?>
                                    <div class="block-footer">
                                        <span class="date"><?php echo $item["data"]; ?></span>
                                    </div><!-- /block-footer -->
                                        <?php
                                    }
                                        ?>
                                </div><!-- /block-body -->
                            </div><!-- /block -->
	                        <?php
                        }
                        ?>
                    </div><!-- /block-double-featured -->
                    <?php
                    }
                    ?>
                    <div class="block block-list block-list-three">
                        <div class="row row-list list-events">
	                        <?php while ( have_posts() ) : the_post(); ?>
                                <div class="col-lg-4 col-sm-6">
			                        <?php get_template_part("template-parts/card/card", $post->post_type, $post); ?>
                                </div>
	                        <?php endwhile; ?>
                        </div><!-- /row -->
	                    <?php get_template_part("template-parts/archive/paginate"); ?>
                    </div><!-- /block -->
                </div><!-- /col-lg-8 -->
            </div><!-- /row -->
        </div><!-- /container -->
    </section><!-- /section -->

<?php get_template_part("template-parts/common/newsletter"); ?>

<?php
get_footer();
