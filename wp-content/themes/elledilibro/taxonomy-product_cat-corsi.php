<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package elledilibro
 */

get_header();
$slider_featured_corsi = get_field("slider_featured_corsi", "options");

get_template_part("template-parts/common/breadcrumb", "", array("type" => "white"));
?>

    <section class="section section-archive">
        <div class="container">
            <div class="row justify-content-center sticky-sidebar-container">
                <div class="col-lg-9 order-lg-1">
					<?php
					if($_REQUEST["order"]){
                        //get_template_part("template-parts/archive/autori");
					}else{
						if($slider_featured_corsi){
                            get_template_part("template-parts/common/slider", "", array("slider_featured" => $slider_featured_corsi));
						}
					}

					get_template_part("template-parts/archive/products", "corsi");
					?>

                </div><!-- /col-lg-9 -->
                <div class="col-lg-3 order-lg-0">
					<?php  get_sidebar("corsi"); ?>
                </div><!-- /col-lg-3 -->
            </div><!-- /row -->
        </div><!-- /container -->
    </section><!-- /section -->

<?php // get_template_part("template-parts/common/promo"); ?>

<?php get_template_part("template-parts/common/newsletter"); ?>

<?php
get_footer();
