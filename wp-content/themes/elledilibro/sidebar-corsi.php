<?php
$idparent = get_term_by("slug", "corsi", "product_cat");

if($idparent){
?>
<aside class="sticky-sidebar">
    <div class="card card-border card-categories mt-3 mt-lg-0">
        <div class="card-body">
            <h3>Corsi</h3>
            <nav class="menu-categories">
                <ul class="list-simple">
                    <?php
                    $items = get_terms( array(
	                    'taxonomy' => 'product_cat',
	                    'hide_empty' => false,
                        'parent' => $idparent->term_id
                    ) );
                    foreach ( $items as $item ) {
                        ?>
                        <li>
                            <a href="<?php echo get_term_link($item); ?>"><?php echo $item->name; ?></a>
                        </li>
                    <?php
                    }
                    ?>
                </ul>
            </nav>

        </div><!-- /card-body -->
    </div><!-- /card -->
</aside>
<?php
}


