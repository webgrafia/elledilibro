(function ($) {
    $(function() {
        $(".datepicker-single1").caleran({
            singleDate: true,
            inline: true,
            calendarCount: 1,
            showHeader: false,
            showFooter: false,
            locale: "it",
            startOnMonday: true,
            startDate: (edl_calendario_config.selected_day ? moment(edl_calendario_config.selected_day, 'YYYYMMDD'): moment().startOf('day')),
            endDate: (edl_calendario_config.selected_day ? moment(edl_calendario_config.selected_day, 'YYYYMMDD'): moment().startOf('day')),
            validateClick: function(cellmoment){
                setTimeout(function() {
                    $('.caleran-day').each(function() {
                        if($(this).hasClass('caleran-selected1') && $(this).data('value') != cellmoment.unix()) {
                            $(this).removeClass('caleran-selected1');
                        }
                    });
                }, 100);

                window.location.href = edl_calendario_config.permalink + '?date=' + cellmoment.format('YYYYMMDD');

                return true;
            },
            ondraw: function(caleran){
                let dates = edl_calendario_config.dates;
                for (var c = 0; c < caleran.config.calendarCount; c++) {
                    var calendar = caleran.calendars.find('.caleran-calendar').eq(c);
                    var cells_in_month = calendar.find('.caleran-days-container > div').not('.caleran-dayofweek, .caleran-weeknumber');
                    for (var i = 0; i < cells_in_month.length; i++) {
                        var cell = $(cells_in_month[i]);
                        var cellDate = ts2human(parseInt(cell.attr("data-value")));
                        if(dates.indexOf(cellDate) != -1)
                        {
                            cell.addClass('caleran-highlight');
                        }
                        // console.log(moment(cellDate).format('YYYYMMDD'));
                        if(edl_calendario_config.selected_day != '' && cellDate.split(/\D/).reverse().join('') == edl_calendario_config.selected_day ) {
                            cell.addClass('caleran-selected1');
                        }
                    }
                }
            }
        });
    });
})(jQuery);

function ts2human(ts) {
    var dt = new Date(ts * 1000);
    date = dt.getDate();
    month = dt.getMonth() + 1;
    year = dt.getFullYear();

    date = date.toString().padStart(2, '0');

    month = month.toString().padStart(2, '0');

    return `${date}-${month}-${year}`;
}