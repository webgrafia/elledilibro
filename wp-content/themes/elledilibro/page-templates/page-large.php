<?php
/* Template Name: Pagina larga */

get_header();

get_template_part("template-parts/common/breadcrumb");

?>


    <section class="section section-main-title bg-gray-light">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-9 col-xl-7">
                    <h1><?php the_title(); ?></h1>
                </div><!-- /col-lg-9 -->
            </div><!-- /row -->
        </div><!-- /container -->
    </section><!-- /section -->

    <main role="main" class="page-wrapper">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-11">
                    <article class="page-container">
						<?php the_content(); ?>
                    </article>
                </div><!-- /col-lg-7 -->
            </div><!-- /row -->
        </div><!-- /container -->
    </main><!-- /main -->


<?php get_template_part("template-parts/common/newsletter"); ?>

<?php

get_footer();
