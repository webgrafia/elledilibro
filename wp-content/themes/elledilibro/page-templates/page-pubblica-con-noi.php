<?php
/* Template Name: Pubblica con noi */

get_header();

get_template_part("template-parts/common/breadcrumb");

?>


    <section class="section section-main-title bg-gray-light">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-9 col-xl-7">
                    <h1><?php the_title(); ?></h1>
                </div><!-- /col-lg-9 -->
            </div><!-- /row -->
        </div><!-- /container -->
    </section><!-- /section -->

    <main role="main" class="page-wrapper">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-9 col-xl-7">
                    <article class="page-container">
						<?php the_content(); ?>
                    </article>
                </div><!-- /col-lg-7 -->
            </div><!-- /row -->
        </div><!-- /container -->
    </main><!-- /main -->


<?php

$corsi_partecipa = get_field("corsi_partecipa");
if($corsi_partecipa){
?>
    <section class="section section-carousel pt-3 pt-md-4 pt-lg-5">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="section-title text-center">
                        <h2><?php the_field("titolo_partecipa"); ?></h2>
                        <p><?php the_field("sottotitolo_partecipa"); ?></p>
                    </div><!-- /section-title -->
                    <div class="owl-carousel owl-theme carousel-three-items">
                        <?php
                        foreach ( $corsi_partecipa as $item ) {
                            get_template_part("template-parts/card/card", "course", $item );
                        }
                        ?>
                    </div><!-- /carousel-three-items -->
                </div><!-- /col -->
            </div><!-- /row -->
        </div><!-- /container -->
    </section><!-- /section -->
    <?php
}
$articoli_blog = get_field("articoli_blog");
if($articoli_blog){
    ?>
    <section class="section section-list py-3 py-md-4 py-lg-5">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="section-title text-center">
                        <h2><?php the_field("titolo_blog"); ?></h2>
                    </div><!-- /section-title -->
                </div><!-- /col -->
            </div><!-- /row -->
            <div class="row row-list">
	                <?php
	                foreach ( $articoli_blog as $item ) {
                        echo '<div class="col-lg-3">';
		                get_template_part("template-parts/card/card", "post", $item );
                        echo '</div>';
	                }
	                ?>
            </div><!-- /row -->
        </div><!-- /container -->
    </section><!-- /section -->
    <?php
}
    ?>


<?php get_template_part("template-parts/common/newsletter"); ?>

<?php

get_footer();
