<?php
/* Template Name: Home */

get_header();
?>

<?php

$blocchi = get_field("blocchi");

foreach ( $blocchi as $blocco ) {
    get_template_part("template-parts/home/block", $blocco["acf_fc_layout"], $blocco);
}
?>

<?php get_template_part("template-parts/common/newsletter"); ?>

<?php
get_footer();


