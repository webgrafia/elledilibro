<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package elledilibro
 */

get_header();
?>

    <main role="main" class="page-wrapper">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-9 col-xl-7">
                    <article class="page-container">
                        <div class="page-404">
                            <h3>404</h3>
                            <p>Oops! Questa pagina non esiste.</p>
                            <div class="d-flex justify-content-center pt-2">
                                <a class="btn btn-lg btn-purple" href="<?php echo home_url(); ?>">Torna alla home</a>
                            </div>
                        </div>
                    </article>
                </div><!-- /col-lg-9 -->
            </div><!-- /row -->
        </div><!-- /container -->
    </main><!-- /main -->

<?php
get_footer();
