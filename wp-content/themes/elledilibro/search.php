<?php

get_header();

get_template_part("template-parts/common/breadcrumb", "", array("type" => "white"));

?>

<form action="<?php echo home_url(); ?>">
    <section class="section pb-1 pb-md-2 pb-lg-3">
        <div class="container">
            <div class="row justify-content-center sticky-sidebar-container">
                <div class="col-lg-3">
                    <aside class="sticky-sidebar">
                        <div class="card card-border card-categories mt-3 mt-lg-0">
                            <div class="card-body p-3">
                                <h3>Cerca:</h3>
                                <div class="form-check mb-1">
                                    <input class="form-check-input" type="radio" name="post_type" id="search-check1" value="any" <?php if(("any" === $_GET["post_type"]) || (!$_GET["post_type"])) echo "checked"; ?>>
                                    <label class="form-check-label" for="search-check1">Tutto il sito</label>
                                </div><!-- /form-check -->
                                <div class="form-check mb-1">
                                    <input class="form-check-input" type="radio" name="post_type" id="search-check2" value="autore" <?php if("autore" === $_GET["post_type"]) echo "checked"; ?>>
                                    <label class="form-check-label" for="search-check2">Autori</label>
                                </div><!-- /form-check -->
                                <div class="form-check mb-1">
                                    <input class="form-check-input" type="radio" name="post_type" id="search-check3" value="post" <?php if("post" === $_GET["post_type"]) echo "checked"; ?>>
                                    <label class="form-check-label" for="search-check3">Blog</label>
                                </div><!-- /form-check -->
                                <div class="form-check mb-1">
                                    <input class="form-check-input" type="radio" name="post_type" id="search-check4" value="bookroom" <?php if("bookroom" === $_GET["post_type"]) echo "checked"; ?>>
                                    <label class="form-check-label" for="search-check4">Bookroom</label>
                                </div><!-- /form-check -->
                                <div class="form-check mb-1">
                                    <input class="form-check-input" type="radio" name="post_type" id="search-check5" value="corso" <?php if("corso" === $_GET["post_type"]) echo "checked"; ?>>
                                    <label class="form-check-label" for="search-check5">Corsi</label>
                                </div><!-- /form-check -->
                                <div class="form-check mb-1">
                                    <input class="form-check-input" type="radio" name="post_type" id="search-check6" value="libro" <?php if("libro" === $_GET["post_type"]) echo "checked"; ?>>
                                    <label class="form-check-label" for="search-check6">Libri</label>
                                </div><!-- /form-check -->
                                <div class="form-check mb-1">
                                    <input class="form-check-input" type="radio" name="post_type" id="search-check7" value="servizio" <?php if("servizio" === $_GET["post_type"]) echo "checked"; ?>>
                                    <label class="form-check-label" for="search-check7">Servizi</label>
                                </div><!-- /form-check -->
                            </div><!-- /card-body -->
                        </div><!-- /card -->
                    </aside>
                </div><!-- /col-lg-3 -->
                <div class="col-lg-9">
                    <div class="form-group mb-2">
                        <label class="form-label">Fai la tua ricerca</label>
                        <div class="input-group">
                            <input name="s" type="text" class="form-control form-control-lg" placeholder="Inserici il testo da cercare" value="<?php echo get_search_query(); ?>">
                            <button class="btn btn-lg btn-outline-dark" type="submit">Cerca</button>
                        </div><!-- /input-group -->
                    </div><!-- /form-group -->
                    <?php if(have_posts()): ?>
                    <div class="block block-list block-list-three">
                        <div class="row row-list list-services">
			                <?php while ( have_posts() ) : the_post(); ?>
                                <div class="col-lg-4 col-sm-6">
					                <?php
                                    if($post->post_type == "product"){
	                                    $product = wc_get_product( $post->ID );
	                                    get_template_part("template-parts/card/card", $product->get_type(), $post);
                                    }else{
	                                    get_template_part("template-parts/card/card", $post->post_type, $post);
                                    }
                                    ?>
                                </div>
			                <?php endwhile; ?>
                        </div><!-- /row -->
		                <?php get_template_part("template-parts/archive/paginate"); ?>
                    </div><!-- /block -->
                    <?php else: ?>
                    <div class="search-results-empty">
                        <p>Nessun risultato</p>
                    </div><!-- /search-results-empty -->
                    <?php endif; ?>
                </div><!-- /col-lg-9 -->
            </div><!-- /row -->
        </div><!-- /container -->
    </section><!-- /section -->
</form>

<?php // get_template_part("template-parts/common/newsletter"); ?>

<?php
get_footer();


