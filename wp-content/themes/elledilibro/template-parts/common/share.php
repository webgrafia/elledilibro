<?php

?>
<div class="favourite">
	<?php the_favorites_button($args->ID); ?>
</div>
<div class="share-wrapper">

    <a class="toggle-share" href="#">
        <span>Condividi</span>
        <svg class="svg-share"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-share"></use></svg>
    </a>
    <div class="share-buttons-container">
        <div class="share-buttons">
            <div id="share-buttons"></div>
        </div>
    </div><!-- /share-buttons -->
</div><!-- /share-wrapper -->