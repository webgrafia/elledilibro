<?php
/**
 * box newsletter
 */
$colore_background_newsletter = get_field("colore_background_newsletter", "options");
$colore_testo_newsletter = get_field("colore_testo_newsletter", "options");
$colore_bottone_newsletter = get_field("colore_bottone_newsletter", "options");
$colore_testo_bottone_newsletter = get_field("colore_testo_bottone_newsletter", "options");

?>
<style>
    .card-featured.bg-dark {
        background-color:<?php echo $colore_background_newsletter; ?> !important;
    }
    .card-featured.bg-dark .card-content h2 a,
    .card-featured.bg-dark .card-content p{
        color:<?php echo $colore_testo_newsletter; ?>;
    }

    .card-featured.bg-dark .card-footer .btn-white{
        background-color:<?php echo $colore_bottone_newsletter; ?> !important;
        color:<?php echo $colore_testo_bottone_newsletter; ?> !important;
    }
</style>
<section class="section section-featured">
	<div class="container-fluid">
		<div class="card card-featured bg-dark" >
			<div class="card-thumb"  style="background-image: url('<?php $img = get_field("newsletter_immagine", "options"); echo $img["sizes"]["big-square"]; ?>');"></div>
			<div class="card-body">
				<div class="card-content text-large">
					<h2><a href="<?php the_field("newsletter_link", "options"); ?>"><?php the_field("newsletter_titolo", "options"); ?></a></h2>
                    <?php the_field("newsletter_testo", "options"); ?>
				</div><!-- /card-content -->
				<div class="card-footer">
					<a class="btn btn-lg btn-white" target="_blank" href="<?php the_field("newsletter_link", "options"); ?>"><?php the_field("newsletter_cta", "options"); ?></a>
				</div><!-- /card-footer -->
			</div><!-- /card-body -->
		</div><!-- /card -->
	</div><!-- /container-fluid -->
</section><!-- /section -->
