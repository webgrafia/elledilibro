<?php
if($args["type"] == "white"){
    $class = "";
    $col = "col";
}else if($args["type"] == "full"){
	$class = "bg-gray-light";
	$col = "col";
}else{
	$class = "bg-gray-light";
	$col = "col-lg-10";
}
?>
<section class="section section-breadcrumbs <?php echo $class; ?>">
	<div class="container">
		<div class="row justify-content-center">
			<div class="<?php echo $col; ?>">
					<?php
					$array = array(
						'container' => 'div',
						'before'          => '',
						'after'           => '',
						'browse_tag'      => '',
						'list_tag'        => '',
						'item_tag'        => '',
						'show_on_front'   => true,
						'network'         => false,
						'show_title'      => true,
						'show_browse'     => false,
						'post_taxonomy' => array(
							'post'  => 'category',
							'product'  => 'product_cat',
						),
						'echo'            => true);
					breadcrumb_trail($array); ?>
			</div><!-- /col-lg-10 -->
		</div><!-- /row -->
	</div><!-- /container -->
</section><!-- /section -->
