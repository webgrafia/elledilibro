<?php
$promozioni = get_field("promozioni", "options") ?: [];
if(count($promozioni)){
?>
<section class="section section-carousel py-3 py-md-4 py-lg-5">
	<div class="container">
		<div class="row">
			<div class="col">
				<div class="section-title text-center">
					<h2><?php the_field("titolo_sezione_promo", "options"); ?></h2>
				</div><!-- /section-title -->
				<div class="owl-carousel owl-theme carousel-four-items">
					<?php foreach ( $promozioni as $item ) { ?>
						<div class="item">
							<article class="card card-service">
								<div class="card-thumb">
									<img src="<?php echo $item["immagine"]["sizes"]["card-post"]; ?>" title="<?php echo esc_attr($item["titolo"]); ?>" alt="<?php echo esc_attr($item["titolo"]); ?>">
								</div><!-- /card-thumb -->
								<div class="card-body">
									<h3><a href="<?php echo $item["link"]; ?>"><?php echo $item["titolo"]; ?></a></h3>
									<?php echo $item["testo"]; ?>
								</div><!-- /card-body -->
							</article><!-- /card -->
						</div><!-- /item -->
						<?php } ?>
				</div><!-- /carousel-four-items -->
			</div><!-- /col -->
		</div><!-- /row -->
	</div><!-- /container -->
</section><!-- /section -->
<?php
}