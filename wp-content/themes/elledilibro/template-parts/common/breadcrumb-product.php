<section class="section section-breadcrumbs">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-10">
                <?php
                $array = array(
                    'container' => 'div',
                    'before'          => '',
                    'after'           => '',
                    'browse_tag'      => '',
                    'list_tag'        => '',
                    'item_tag'        => '',
                    'show_on_front'   => true,
                    'network'         => false,
                    'show_title'      => true,
                    'show_browse'     => false,
                    'post_taxonomy' => array(
                        'post'  => 'category',
                        'product'  => 'product_cat',
                    ),
                    'echo'            => true);
                breadcrumb_trail($array);

                do_action( 'woocommerce_before_single_product' );
                ?>
            </div><!-- /col-lg-10 -->
        </div><!-- /row -->
    </div><!-- /container -->
</section><!-- /section -->


