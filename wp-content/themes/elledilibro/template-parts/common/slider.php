<?php
$slider_featured = $args["slider_featured"];
?>
<div class="block block-featured-wrapper featured-wrapper bg-dark mb-2 mb-lg-3 mb-xl-4">
	<div class="owl-carousel owl-theme carousel-single-nav-dots">
		<?php foreach ( $slider_featured as $item ) { ?>
			<div class="item">
				<div class="card card-featured card-featured-small bg-dark thumb-right">
					<div class="card-thumb" style="background-image: url('<?php echo $item["immagine"]["sizes"]["big-square"]; ?>');"></div>
					<div class="card-body">
						<div class="card-content">
							<h2><a href="<?php echo $item["link"]; ?>"><?php echo $item["titolo"]; ?></a></h2>
							<?php echo $item["testo"]; ?>
						</div><!-- /card-content -->
						<div class="card-footer">
							<a class="btn btn-lg btn-outline-white" href="<?php echo $item["link"]; ?>"><?php echo $item["cta"]; ?></a>
						</div><!-- /card-footer -->
					</div><!-- /card-body -->
				</div><!-- /card -->
			</div><!-- /item -->
			<?php } ?>
	</div><!-- /carousel-single-nav-dots -->
</div><!-- /featured-wrapper -->

