<?php
global $product;

$related_query = new WP_Query(array(
	'post_type' => 'product',
	'post__not_in' => array($product->get_id()),
	'posts_per_page' => 6,
	'orderby' => 'date',
	'tax_query' => array(
		array(
			'taxonomy' => 'product_type',
			'field'    => 'slug',
			'terms'    => $product->get_type(),
		),
	),
));
if($related_query){
?>

<section class="section section-carousel py-3 py-md-4 py-lg-5">
	<div class="container">
		<div class="row">
			<div class="col">
				<div class="section-title text-center">
					<h2>Altri corsi</h2>
				</div><!-- /section-title -->
				<div class="owl-carousel owl-theme carousel-three-items">
					<?php while ($related_query->have_posts()) { ?>
						<?php $related_query->the_post(); ?>
						<?php get_template_part("template-parts/card/card", "course", $related_query->post); ?>
					<?php } ?>
				</div><!-- /carousel-three-items -->
			</div><!-- /col -->
		</div><!-- /row -->
	</div><!-- /container -->
</section><!-- /section -->
<?php
}