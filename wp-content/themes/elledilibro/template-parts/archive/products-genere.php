<div class="category-title">
    <h1>Genere: <span class="text-uppercase">
            <?php
            $queried_object = get_queried_object();
            if ( $queried_object ) {
	            $tax   = get_taxonomy( $queried_object->taxonomy );
	            $title = single_term_title( '', false );
	            echo $title;
            }
            ?>
        </span></h1>
</div>

<div class="block block-list block-list-four">
    <div class="row row-list">
	    <?php while ( have_posts() ) : the_post(); ?>
        <div class="col-lg-3 col-sm-6">
	        <?php get_template_part("template-parts/card/card", "libro", $post); ?>
        </div><!-- /col-lg-3 -->
	    <?php endwhile; ?>
    </div><!-- /row -->
	<?php get_template_part("template-parts/archive/paginate"); ?>
</div>
