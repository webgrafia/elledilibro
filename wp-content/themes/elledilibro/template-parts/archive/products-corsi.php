<div class="block block-list block-list-three">
    <div class="row row-list list-products">
		<?php while ( have_posts() ) : the_post(); ?>
            <div class="col-lg-4 col-sm-6">
				<?php
				$product = wc_get_product( $post->ID );
				get_template_part("template-parts/card/card", "course", $post); ?>
            </div><!-- /col-lg-3 -->
		<?php endwhile; ?>
    </div>
	<?php get_template_part("template-parts/archive/paginate"); ?>
</div>