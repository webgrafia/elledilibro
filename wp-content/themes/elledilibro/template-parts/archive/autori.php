<div class="section-title text-center">
    <h2>Autori
        <?php
        if(is_tax("genere")){

        $queried_object = get_queried_object();
        if ( $queried_object ) {
	        $tax   = get_taxonomy( $queried_object->taxonomy );
	        $title = single_term_title( '', false );
            echo $title;
        }
        }else if($_REQUEST["order"]){
            if($_REQUEST["order"] == "asc")
                echo "A-Z";
            else  if($_REQUEST["order"] == "desc")
	            echo "Z-A";
        }
        ?>
    </h2>
</div>
<div class="row row-list list-autori">
<?php while ( have_posts() ) : the_post(); ?>
	<div class="col-lg-4 col-sm-6">
	<?php get_template_part("template-parts/card/card", "autore", $post); ?>
	</div>
<?php endwhile; ?>
    <?php get_template_part("template-parts/archive/paginate"); ?>
</div>