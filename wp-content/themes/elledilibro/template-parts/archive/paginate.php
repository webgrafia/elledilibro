<?php
    $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

    $big = 999999999;
    $pages = paginate_links( array(
        'base' => str_replace( $big, '%#%', get_pagenum_link( $big ) ),
        'format' => '?paged=%#%',
        'current' => max( 1, get_query_var('paged') ),
        'prev_text' => '<svg class="svg-arrow-long-left"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-arrow-long-left"></use></svg>',
        'next_text' => '<svg class="svg-arrow-long-right"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-arrow-long-right"></use></svg>',
        'type'  => 'array'
    ) );
if( is_array( $pages ) ) {
?>
<div class="pager">
    <ol class="pager-list">
        <?php
        foreach ( $pages as $page ) {
            if(strip_tags($page) == $paged)
	            echo "<li class='current'>$page</li>";
            else
                echo "<li>$page</li>";
        }
        ?>
    </ol>
</div><!-- /pager -->
<?php
}
