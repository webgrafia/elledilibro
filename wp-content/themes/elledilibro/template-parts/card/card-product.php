<?php
/**
 * card libro
 */
global $product;
if(!$product)
    $product = wc_get_product( $args->ID );
$bgcolor = get_field("background_card", $args->ID);
if(!$bgcolor) $bgcolor = "#ff4a53";
?>
<div class="item">
	<div class="card card-book">
		<div class="card-thumb" style="background-color: <?php echo $bgcolor; ?>;">
			<div class="card-book-utilities">
				<a class="icon-cart" href="<?php echo $product->add_to_cart_url(); ?>" data-tippy-content="Aggiungi al carrello">
					<svg class="svg-cart"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-cart"></use></svg>
				</a>
				<a class="icon-wishlist" href="#" data-tippy-content="Aggiungi alla wishlist">
					<svg class="svg-wishlist"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-wishlist"></use></svg>
				</a>
			</div><!-- /card-book-utilities -->
			<a href="<?php echo get_permalink($args); ?>">
                <?php
                $image = get_the_post_thumbnail( $args->ID, 'card-book' );
                echo $image ?: '<img src="'  . get_bloginfo('template_directory') . '/assets/placeholders/book-cover.png" title="" alt="">';
                ?>
			</a>
		</div><!-- /card-thumb -->
		<div class="card-body">
			<div class="card-content">
				<span class="author">
                    <?php
                    $c=0;
                    $autori = elle_get_autori($args);
                    foreach ($autori  as $autore ) {
	                    if($c) echo ", ";
	                    echo $autore->post_title;
                    }
                    ?>
                </span>
				<h3><a href="<?php echo get_permalink($args); ?>"><?php echo $args->post_title; ?></a></h3>
			</div><!-- /card-content -->
			<div class="card-footer">
				<div class="card-price">
					<?php echo elle_get_price_html($product); ?>
				</div><!-- /card-price -->
			</div><!-- /card-footer -->
		</div><!-- /card-body -->
	</div><!-- /card -->
</div><!-- /item -->
