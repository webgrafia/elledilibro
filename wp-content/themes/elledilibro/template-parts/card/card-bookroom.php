<?php
/**
 * card bookroom
 */
?>

<article class="card card-article">
    <div class="card-thumb">
        <?php echo get_the_post_thumbnail( $args->ID, 'card-post' ); ?>
    </div><!-- /card-thumb -->
    <div class="card-body">
        <span class="date"><?php the_field("data_inizio", $args->ID); ?></span>
        <h3><a href="<?php echo get_permalink($args); ?>"><?php echo $args->post_title; ?></a></h3>
        <p><?php echo get_the_excerpt($args); ?></p>
    </div><!-- /card-body -->
</article><!-- /card -->
