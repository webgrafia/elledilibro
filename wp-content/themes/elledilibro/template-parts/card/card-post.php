<?php
/**
 * card post
 */
//print_r($args);
?>

<article class="card card-article">
    <div class="card-thumb">
        <?php echo get_the_post_thumbnail( $args->ID, 'card-post' ); ?>
    </div><!-- /card-thumb -->
    <div class="card-body">
                  <span class="category">
                      <?php
                      $categories = wp_get_post_terms( $args->ID, 'category' );
                      if($categories){
	                      $first_cat      = $categories[0];
	                      $category_name  = $first_cat->name;
	                      $category_link  = get_term_link( $first_cat );
                          ?>
                          <a href="<?php echo esc_url( $category_link ); ?>" title="<?php echo esc_attr( $category_name ); ?>"><?php echo esc_html( $category_name ); ?></a>
                      <?php
                      }
                      ?>
                  </span>
        <h3><a href="<?php echo get_permalink($args); ?>"><?php echo $args->post_title; ?></a></h3>
        <p><?php echo get_the_excerpt($args); ?></p>
    </div><!-- /card-body -->
</article><!-- /card -->
