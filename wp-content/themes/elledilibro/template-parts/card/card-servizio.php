<?php
/**
 * card servizio
 */
//print_r($args);
$product = wc_get_product( $args->ID );
?>

<div class="item">
    <article class="card card-service">
        <div class="card-thumb">
            <?php
            $image = get_the_post_thumbnail( $args->ID, 'card-service' );
            echo $image ?: '<img src="'  . get_bloginfo('template_directory') . '/assets/placeholders/placeholder-700x500.jpg" title="" alt="">';
            ?>
        </div><!-- /card-thumb -->
        <div class="card-body">
            <h3><a href="<?php echo get_permalink($args); ?>"><?php echo $args->post_title; ?></a></h3>
            <p><?php echo  wp_trim_words($args->post_excerpt, 20); ?></p>
        </div><!-- /card-body -->
        <div class="card-footer">
            <div class="card-price">
                <?php echo elle_get_price_html($product); ?>
            </div><!-- /card-price -->
        </div><!-- /card-footer -->
    </article><!-- /card -->
</div>
