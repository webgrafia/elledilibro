<?php
/**
 * card autore
 */
//print_r($args);
?>
<div class="item">
	<div class="card card-author">
		<div class="card-side card-front">
			<div class="card-thumb">
				<a href="<?php echo get_permalink($args); ?>">
					<?php echo get_the_post_thumbnail( $args->ID, 'medium-square' ); ?>
				</a>
			</div><!-- /card-thumb -->
			<div class="card-body">
				<h3><a href="<?php echo get_permalink($args); ?>"><?php echo $args->post_title; ?></a></h3>
			</div><!-- /card-body -->
		</div>
		<div class="card-side card-back">
			<div class="card-body">
				<p>di questo autore</p>
                <div class="book-container">
                <?php
                $libri = elle_get_libri($args);
                $libro = $libri[0];
                echo get_the_post_thumbnail( $libro->ID, 'card-book' );
                ?>
                </div>
			</div><!-- /card-body -->
			<div class="card-footer">
				<a class="btn btn-outline-dark btn-sm w-100" href="<?php echo get_permalink($args); ?>">Vedi tutti i libri</a>
			</div>
		</div>
	</div><!-- /card -->
</div><!-- /item -->

