<?php
/**
 * card generica
 */
//print_r($args);
?>
<div class="item">
    <div class="card card-product">
        <div class="card-thumb">
            <?php
            $image = get_the_post_thumbnail( $args->ID, 'card-corso' );
            echo $image ?: '<img src="'  . get_bloginfo('template_directory') . '/assets/placeholders/placeholder-1200x870.jpg" title="" alt="">';
            ?>
        </div><!-- /card-thumb -->
        <div class="card-body">
            <h3><a href="<?php echo get_permalink($args); ?>"><?php echo $args->post_title; ?></a></h3>
        </div><!-- /card-body -->
    </div><!-- /card -->
</div>
