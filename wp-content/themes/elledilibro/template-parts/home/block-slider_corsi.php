<?php
//print_r($args);
?>

<section class="section section-carousel pt-3 pt-md-4 pt-lg-5">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="section-title text-center">
                    <h2><?php echo $args["titolo"]; ?></h2>
                </div><!-- /section-title -->
                <div class="owl-carousel owl-theme carousel-three-items">
	                <?php
	                foreach ( $args["corsi"] as $corso ) {
		                get_template_part("template-parts/card/card", "corso", $corso);
	                }
	                ?>
                </div><!-- /carousel-three-items -->
            </div><!-- /col -->
        </div><!-- /row -->
    </div><!-- /container -->
</section><!-- /section -->
