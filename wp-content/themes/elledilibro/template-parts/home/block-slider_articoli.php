<?php
//print_r($args);
?>


<section class="section section-list py-3 py-md-4 py-lg-5">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="section-title text-center">
                    <h2><?php echo $args["titolo"]; ?></h2>
                </div><!-- /section-title -->
            </div><!-- /col -->
        </div><!-- /row -->
        <div class="row row-list">
	            <?php foreach ( $args["articoli"] as $articolo ) { ?>
                <div class="col-lg-3 col-sm-6">
                    <?php get_template_part("template-parts/card/card", "post", $articolo); ?>
                </div><!-- /col-lg-3 -->
                <?php } ?>
        </div><!-- /row -->
    </div><!-- /container -->
</section><!-- /section -->
