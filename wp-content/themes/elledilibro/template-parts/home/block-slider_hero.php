<?php
// var_dump( $args );	// Everything
?>
<section class="section section-hero-carousel">
    <div class="container-fluid">
        <div class="owl-carousel owl-theme carousel-single-nav-dots">
            <?php
            foreach ( $args["slide"] as $slide ) {
                ?>
                <div class="item">
                    <div class="card card-featured thumb-right">
                        <div class="card-thumb" style="background-image: url('<?php echo $slide["immagine"]["sizes"]["big-square"]; ?>');"></div>
                        <div class="card-body">
                            <div class="card-content">
                                <h2><a href="<?php echo $slide["link"]; ?>"><?php echo $slide["titolo"]; ?></a></h2>
	                            <?php echo $slide["testo"]; ?>
                            </div><!-- /card-content -->
                            <div class="card-footer">
                                <a class="btn btn-lg btn-outline-dark" href="<?php echo $slide["link"]; ?>"><?php echo $slide["cta"]; ?></a>
                            </div><!-- /card-footer -->
                        </div><!-- /card-body -->
                    </div><!-- /card -->
                </div><!-- /item -->
            <?php
            }
            ?>
        </div><!-- /carousel-single-nav-dots -->
    </div><!-- /container-fluid -->
</section><!-- /section -->
