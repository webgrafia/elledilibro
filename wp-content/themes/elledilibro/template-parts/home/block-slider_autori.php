<?php

?>

<section class="section section-carousel py-3 py-md-4 py-lg-5">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="section-title text-center">
                    <h2><?php echo $args["titolo"]; ?></h2>
                </div><!-- /section-title -->
                <div class="owl-carousel owl-theme carousel-four-items carousel-author">
	                <?php
	                foreach ( $args["autori"] as $autore ) {
		                get_template_part("template-parts/card/card", "autore", $autore);
	                }
	                ?>
                </div><!-- /carousel-four-items -->
            </div><!-- /col -->
        </div><!-- /row -->
    </div><!-- /container -->
</section><!-- /section -->