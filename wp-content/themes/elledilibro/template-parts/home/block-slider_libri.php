<?php

?>
<section class="section section-carousel pt-3 pt-md-4 pt-lg-5">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="section-title text-center">
                    <h2><?php echo $args["titolo"]; ?></h2>
                </div><!-- /section-title -->
                <div class="owl-carousel owl-theme carousel-four-items">
	                <?php
	                foreach ( $args["libri"] as $libro ) {
		                get_template_part("template-parts/card/card", "libro", $libro);
	                }
	                ?>
                </div><!-- /carousel-four-items -->
            </div><!-- /col -->
        </div><!-- /row -->
    </div><!-- /container -->
</section><!-- /section -->

