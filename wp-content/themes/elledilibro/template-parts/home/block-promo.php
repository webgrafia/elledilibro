<section class="section section-featured">
    <div class="container-fluid">
        <div class="card card-featured">
            <div class="card-thumb" style="background-image: url('<?php echo $args["immagine"]["sizes"]["big-square"]; ?>');"></div>
            <div class="card-body">
                <div class="card-content">
                    <h2><a href="<?php echo $args["link"]; ?>"><?php echo $args["titolo"]; ?></a></h2>
	                <?php echo $args["testo"]; ?>
                </div><!-- /card-content -->
                <div class="card-footer">
                    <a class="btn btn-lg <?php if($args["cta_primaria"]) echo "btn-purple"; else echo "btn-outline-dark"; ?>" href="<?php echo $args["link"]; ?>"><?php echo $args["cta"]; ?></a>
                </div><!-- /card-footer -->
            </div><!-- /card-body -->
        </div><!-- /card -->
    </div><!-- /container-fluid -->
</section><!-- /section -->
