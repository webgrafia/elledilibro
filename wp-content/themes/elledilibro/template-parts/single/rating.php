<?php
$rating = $args->get_average_rating() ;
$rating_count = $args->get_rating_count();
$review_count = $args->get_review_count();
if($rating){
?>
    <div class="review-stars">
        <svg class="svg-star-<?php echo ($rating >= 1) ? "full" : "empty"; ?>"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-star-<?php echo ($rating >= 1) ? "full" : "empty"; ?>"></use></svg>
        <svg class="svg-star-<?php echo ($rating >= 2) ? "full" : "empty"; ?>"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-star-<?php echo ($rating >= 2) ? "full" : "empty"; ?>"></use></svg>
        <svg class="svg-star-<?php echo ($rating >= 3) ? "full" : "empty"; ?>"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-star-<?php echo ($rating >= 3) ? "full" : "empty"; ?>"></use></svg>
        <svg class="svg-star-<?php echo ($rating >= 4) ? "full" : "empty"; ?>"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-star-<?php echo ($rating >= 4) ? "full" : "empty"; ?>"></use></svg>
        <svg class="svg-star-<?php echo ($rating >= 5) ? "full" : "empty"; ?>"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-star-<?php echo ($rating >= 5) ? "full" : "empty"; ?>"></use></svg>
    </div><!-- /review-stars -->
    <p>(<?php echo $rating_count; ?> votazioni)</p>
<?php
}