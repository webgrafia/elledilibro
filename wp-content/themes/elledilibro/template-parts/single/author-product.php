<?php
$autori = elle_get_autori($args);
foreach ($autori  as $autore ) {
?>
<div class="author-wrapper mb-1">
	<div class="author-avatar">
		<a href="<?php echo get_permalink($autore->ID); ?>">
			<?php echo get_the_post_thumbnail( $autore->ID ); ?>
		</a>
	</div><!-- /author-avatar -->
	<div class="author-body">
		<span class="role">Autore</span>
		<span class="name">
			<a href="<?php echo get_permalink($autore->ID); ?>"><?php echo $autore->post_title; ?></a>
		</span>
	</div><!-- /author-body -->
</div><!-- /author-wrapper -->
<?php
}
