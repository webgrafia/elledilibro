<?php
global $product;

$related_products = $args;

//echo "<pre>".print_r($related_products, 1)."</pre>";
?>

<section class="section section-list py-3 py-md-4 py-lg-5">
	<div class="container">
		<div class="row">
			<div class="col">
                <div class="section-title text-center">
                    <?php
                    $visible_items_number = 'four';
                    switch ($product->get_type()) {
                        case 'libro':
                            $heading = __('Potrebbero interessarti');
                            break;
                        case 'servizio':
                        case 'pacchetto':
                            $heading = __('Servizi che potrebbero interessarti');
                            break;
                        case 'course':
                            $visible_items_number = 'three';
                            $heading = __('Altri corsi');
                            break;
                    }

                    if ( $heading ) : ?>
                        <h2><?php echo esc_html( $heading ); ?></h2>
                    <?php endif; ?>
                </div><!-- /section-title -->
			</div><!-- /col -->
		</div><!-- /row -->
        <div class="owl-carousel owl-theme carousel-<?php echo $visible_items_number; ?>-items">
            <?php foreach ( $related_products as $related_product ) : ?>

                <?php
                $post_object = get_post( $related_product->get_id() );

                setup_postdata( $GLOBALS['post'] =& $post_object ); // phpcs:ignore WordPress.WP.GlobalVariablesOverride.Prohibited, Squiz.PHP.DisallowMultipleAssignments.Found

                get_template_part("template-parts/card/card", $product->get_type(), $post_object);
                ?>
            <?php endforeach; ?>
        </div>
	</div><!-- /container -->
</section><!-- /section -->