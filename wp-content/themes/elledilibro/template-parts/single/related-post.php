<?php
global $product;
$post_type = $args->post_type;
$related_query = new WP_Query(array(
	'post_type' => $post_type,
//	'category__in' => wp_get_post_categories(get_the_ID()),
	'post__not_in' => array($args->ID),
	'posts_per_page' => 4,
	'orderby' => 'date',
));


//echo "<pre>".print_r($related_products, 1)."</pre>";
?>

<?php if ($related_query->have_posts()) { ?>
    <section class="section section-carousel py-3 py-md-4 py-lg-5">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="section-title text-center">
                        <h2>Altri articoli</h2>
                    </div><!-- /section-title -->
                    <div class="owl-carousel owl-theme carousel-four-items carousel-author">
						<?php while ($related_query->have_posts()) { ?>
							<?php $related_query->the_post(); ?>
							<?php get_template_part("template-parts/card/card", $post_type, $related_query->post); ?>
						<?php } ?>
                    </div><!-- /carousel-four-items -->
                </div><!-- /col -->
            </div><!-- /row -->
        </div><!-- /container -->
    </section><!-- /section -->
	<?php
}