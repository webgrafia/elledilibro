<?php
$today = date("Ymd");
$meta_query = array(
	'inizio' => array(
		'key' => 'data_inizio',
		'value' => $today,
		'compare' => '>='
	)
);
$related_query = new WP_Query(array(
	'post_type' => "bookroom",
	'post__not_in' => array($args->ID),
	'posts_per_page' => 4,
	'meta_key' => 'data_inizio',
	'orderby' => 'meta_value',
	'order' => 'ASC',
	'meta_query' => $meta_query
));


?>

<?php if ($related_query->have_posts()) { ?>
    <section class="section section-list py-3 py-md-4 py-lg-5">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="section-title text-center">
                        <h2>Prossimi Eventi</h2>
                    </div><!-- /section-title -->
                </div><!-- /col -->
            </div><!-- /row -->
            <div class="row">
	            <?php while ($related_query->have_posts()) { ?>
                <div class="col-lg-3">
		            <?php $related_query->the_post(); ?>
		            <?php get_template_part("template-parts/card/card", "bookroom", $related_query->post); ?>
                </div>
	            <?php } ?>
            </div><!-- /row -->
        </div><!-- /container -->
    </section>
	<?php
}