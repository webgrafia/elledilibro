<?php
$full = (get_field( 'larghezza' ) == "full") ? "vw-100" : "";

$immagine = get_field( 'immagine' );
$titolo   = get_field( 'titolo' );
$testo    = get_field( 'testo' );
$cta      = get_field( 'cta' );
$link     = get_field( 'link' );
$align    = get_field( 'align' ) ?? "left";
?>

<div class="block block-featured thumb-<?php echo $align; ?> <?php echo $full; ?>">
    <div class="block-thumb" style="background-image: url('<?php echo $immagine["sizes"]["big-square"]; ?>');"></div>
    <div class="block-body">
        <div class="block-content">
            <h3><a href="<?php echo $link; ?>"><?php echo $titolo; ?></a></h3>
			<?php echo $testo; ?>
        </div><!-- /block-content -->
        <div class="block-footer">
            <a class="btn btn-outline-white" href="<?php echo $link; ?>"><?php echo $cta; ?></a>
        </div><!-- /block-footer -->
    </div><!-- /block-body -->
</div>