<?php
$full = (get_field( 'larghezza' ) == "full") ? "vw-100" : "";

$titolo = get_field( 'titolo' );
$teams = get_field( 'teams' );
?>
<div class="block <?php echo $full; ?> block-team">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12 col-xl-9">
                <div class="block-title text-center">
                    <h3><?php echo $titolo; ?></h3>
                </div><!-- /block-title -->
                <div class="row row-list justify-content-center mb-2">
                    <?php
                    foreach ( $teams as $team ) {
                        ?>
                        <div class="col-lg-4">
                            <div class="card card-team">
                                <div class="card-thumb">
                                     <img src="<?php echo $team["immagine"]["sizes"]["medium-square"]; ?>" title="<?php echo $team["nome"]; ?>" alt="<?php echo $team["ruolo"]; ?>">
                                </div><!-- /card-thumb -->
                                <div class="card-body">
                                    <h3><?php echo $team["nome"]; ?></h3>
                                    <p class="role"><?php echo $team["ruolo"]; ?></p>
                                    <div class="social-links">
                                        <?php
                                        if($team["link_facebook"]){
                                        ?>
                                        <a href="<?php echo $team["link_facebook"]; ?>" target="_blank">
                                            <svg class="svg-social-facebook"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-social-facebook"></use></svg>
                                        </a>
                                        <?php
                                        }
                                        ?>
	                                    <?php
	                                    if($team["link_instagram"]){
	                                    ?>
                                        <a href="<?php echo $team["link_instagram"]; ?>" target="_blank">
                                            <svg class="svg-social-instagram"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-social-instagram"></use></svg>
                                        </a>
		                                    <?php
	                                    }
	                                    ?>
	                                    <?php
	                                    if($team["link_linkedin"]){
	                                    ?>
                                        <a href="<?php echo $team["link_linkedin"]; ?>" target="_blank">
                                            <svg class="svg-social-linkedin"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-social-linkedin"></use></svg>
                                        </a>
		                                    <?php
	                                    }
	                                    ?>
                                    </div><!-- /social-links -->
                                </div><!-- /card-body -->
                            </div><!-- /card -->
                        </div><!-- /col-lg-4 -->
                    <?php
                    }
                    ?>
                </div><!-- /row -->
            </div><!-- /col-lg-12 -->
        </div><!-- /row -->
    </div><!-- /container -->
</div><!-- /block -->