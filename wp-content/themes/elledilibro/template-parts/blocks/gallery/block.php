<?php
$full = (get_field( 'larghezza' ) == "full") ? "vw-100" : "";

$titolo             = get_field( 'titolo' ) ?: 'Gallery';
$descrizione           = get_field( 'descrizione' );
$gallery           = get_field( 'galleria' );
?>
<div class="block block-photo-gallery <?php echo $full; ?>">
	<h3><?php echo $titolo; ?></h3>
	<div class="photo-gallery lightgallery">
        <?php foreach ( $gallery as $item ) { ?>
            <a href="<?php echo $item["sizes"]["featured-post"]; ?>">
                <img src="<?php echo $item["sizes"]["medium-square"]; ?>">
            </a>
        <?php }  ?>
    </div><!-- /photo-gallery -->
    <div class="carousel-gallery-caption">
	    <?php echo $descrizione; ?>
    </div><!-- /carousel-gallery-caption -->
</div><!-- /block -->
