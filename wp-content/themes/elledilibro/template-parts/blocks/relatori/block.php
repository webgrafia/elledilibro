<?php
$full = (get_field( 'larghezza' ) == "full") ? "vw-100" : "";

$relatori = get_field( 'relatori' );
?>

<div class="block block-carousel-persons <?php echo $full; ?>">
    <div class="owl-carousel owl-theme carousel-double-nav">
        <?php
        foreach ( $relatori as $item ) {
            ?>
            <div class="item">
                <div class="card card-person card-person-simple">
                    <div class="card-thumb">
                        <img src="<?php echo $item["immagine"]["sizes"]["medium-square"]; ?>">
                    </div><!-- /card-thumb -->
                    <div class="card-body">
                        <h3 class="mb-0"><?php echo $item["nome"]; ?></h3>
                        <p class="role"><?php echo $item["ruolo"]; ?></p>
                        <div class="social-links">
	                        <?php if($item["facebook"]){ ?>
                            <a href="<?php echo $item["facebook"]; ?>">
                                <svg class="svg-social-facebook"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-social-facebook"></use></svg>
                            </a>
                            <?php } ?>
	                        <?php if($item["instagram"]){ ?>
                            <a href="<?php echo $item["instagram"]; ?>">
                                <svg class="svg-social-instagram"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-social-instagram"></use></svg>
                            </a>
	                        <?php } ?>
	                        <?php if($item["linkedin"]){ ?>
                            <a href="<?php echo $item["linkedin"]; ?>">
                                <svg class="svg-social-linkedin"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-social-linkedin"></use></svg>
                            </a>
	                        <?php } ?>
                        </div><!-- /social-links -->
                    </div><!-- /card-body -->
                </div><!-- /card -->
            </div><!-- /item -->
	        <?php
        }
        ?>
    </div><!-- /carousel-single-item -->
</div><!-- /block-title-line -->