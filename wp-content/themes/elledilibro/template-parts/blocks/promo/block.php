<?php
$full = (get_field( 'larghezza' ) == "full") ? "vw-100" : "";

$immagine = get_field( 'immagine' );
$titolo = get_field( 'titolo' );
$testo = get_field( 'testo' );
$cta = get_field( 'cta' );
$link = get_field( 'link' );
$align = get_field( 'align' ) ?? "left";
if($align == "left"){
	$class1 = "offset-md-6 col-lg-4 offset-lg-6";
	$class2 = "justify-content-center";
} else{
	$class1 = "col-md-5 offset-md-0 col-lg-4 offset-lg-1";
	$class2 = "";
}

?>

<div class="block block-card-featured <?php echo $full; ?>">
    <div class="card card-featured thumb-<?php echo $align; ?>">
        <div class="card-thumb" style="background-image: url('<?php echo $immagine["sizes"]["big-square"]; ?>');"></div>
        <div class="container">
            <div class="row <?php echo $class2; ?>">
                <div class="col-md-5 <?php echo $class1; ?>">
                    <div class="card-body">
                        <div class="card-content">
                            <h3><a href="<?php echo $link; ?>"><?php echo $titolo; ?></a></h3>
	                        <?php echo $testo; ?>
                        </div><!-- /card-content -->
                        <div class="card-footer">
                            <a class="btn btn-lg btn-purple" href="<?php echo $link; ?>"><?php echo $cta; ?></a>
                        </div><!-- /card-footer -->
                    </div><!-- /card-body -->
                </div><!-- /col-lg-4 -->
            </div><!-- /row -->
        </div><!-- /container -->
    </div><!-- /card -->
</div>

