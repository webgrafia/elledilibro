<?php
$full = (get_field( 'larghezza' ) == "full") ? "vw-100" : "";

$descrizione = get_field( 'descrizione' );
$gallery = get_field( 'galleria' );
?>
<div class="block block-carousel-single-photo <?php echo $full; ?>">
    <div class="owl-carousel owl-theme carousel-single-photo">
        <?php foreach ( $gallery as $item ) { ?>
            <div class="item">
                <img src="<?php echo $item["sizes"]["single-book"]; ?>">
            </div><!-- /item -->
        <?php }  ?>
    </div><!-- /carousel-single-photo -->
    <div class="carousel-single-photo-caption">
	    <?php echo $descrizione; ?>
    </div><!-- /carousel-single-photo-caption -->
</div><!-- /block -->