<?php
$full = (get_field( 'larghezza' ) == "full") ? "vw-100" : "";

$lista = get_field( 'lista' );
?>
<div class="block <?php echo $full; ?> block-cards-ol pb-3 pb-md-4 pb-lg-5">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-9 col-xl-7">
                <div class="cards-ol">
	                <?php
	                foreach ($lista as $item)
                    {
		            ?>
                        <div class="card card-border">
                            <div class="card-body">
                                <?php echo $item["testo"]; ?>
                            </div><!-- /card-body -->
                        </div><!-- /card -->
		            <?php
	                }
	                ?>
                </div><!-- /cards-ol -->
            </div><!-- /col-lg-9 -->
        </div><!-- /row -->
    </div><!-- /container -->
</div>