<?php
$full = (get_field( 'larghezza' ) == "full") ? "vw-100" : "";

?><div class="block block-form <?php echo $full; ?>">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-10 col-xl-8">
                <div class="block block-map">
                    <div class="block-map-embed">
                        <?php the_field("iframe"); ?>
                    </div>
                    <div class="block-body">
                        <h3><?php the_field("titolo"); ?></h3>
                        <address>
	                        <?php echo wpautop(get_field("indirizzo")); ?>
                        </address>
                        <ul>
                            <?php if($telefono = get_field("telefono")){ ?>
                                <li class="icon-phone">
                                    <span><?php echo $telefono; ?></span>
                                </li>
                            <?php } ?>
	                        <?php if($fax = get_field("fax")){ ?>
                            <li class="icon-fax">
                                <span><?php echo $fax; ?></span>
                            </li>
	                        <?php } ?>
	                        <?php if($email = get_field("email")){ ?>
                            <li class="icon-email">
                                <a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a>
                            </li>
	                        <?php } ?>
                        </ul>
                    </div><!-- /block-body -->
                </div><!-- /block -->
            </div><!-- /col-lg-10 -->
        </div><!-- /row -->
    </div><!-- /container -->
</div><!-- /block -->