<?php
$full = (get_field( 'larghezza' ) == "full") ? "vw-100" : "";

$citazione             = get_field( 'citazione' ) ?: 'Citazione';
$autore           = get_field( 'autore' );
?>
<div class="block block-quote <?php echo $full; ?>">
    <div class="block-body">
        <blockquote><?php echo $citazione; ?></blockquote>
        <span class="author"><?php echo $autore; ?></span>
    </div><!-- /block-body -->
</div>