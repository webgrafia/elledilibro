<?php
$full = (get_field( 'larghezza' ) == "full") ? "vw-100" : "";
?>
<div class="block <?php echo $full; ?> block-double-col">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-5">
                <?php the_field("colonna_1") ?>
            </div><!-- /col-lg-5 -->
            <div class="col-lg-5">
                <?php the_field("colonna_2") ?>
            </div><!-- /col-lg-5 -->
        </div><!-- /row -->
    </div><!-- /container -->
</div>