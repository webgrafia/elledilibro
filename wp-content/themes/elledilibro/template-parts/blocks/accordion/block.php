<?php
$full = (get_field( 'larghezza' ) == "full") ? "vw-100" : "";

$lista = get_field( 'lista' );
?>

<div class="block <?php echo $full; ?> block-accordion pb-3 pb-md-4 pb-lg-5">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-9 col-xl-7">
                <div class="accordion-wrapper">
	                <?php
	                foreach ($lista as $item)
	                {
		                ?>
                        <div class="accordion-header">
                            <h3><?php echo $item["titolo"]; ?></h3>
                        </div><!-- /accordion-header -->
                        <div class="accordion-content">
    		                <?php echo $item["testo"]; ?>
                        </div><!-- /accordion-content -->
		                <?php
	                }
	                ?>
                </div><!-- /accordion-wrapper -->
            </div><!-- /col-lg-9 -->
        </div><!-- /row -->
    </div><!-- /container -->
</div><!-- /block -->

