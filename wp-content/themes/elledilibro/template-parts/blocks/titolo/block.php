<?php
$full = (get_field( 'larghezza' ) == "full") ? "vw-100" : "";

if((get_field( 'larghezza' ) == "full")){
    ?>
    <div class="block <?php echo $full; ?> block-h2-page">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-10">
                    <h2><?php the_field("titolo"); ?></h2>
                </div><!-- /col-lg-10 -->
            </div><!-- /row -->
        </div><!-- /container -->
    </div>
<?php
}else{
    ?>
    <div class="block block-title-line">
        <h3><?php the_field("titolo"); ?></h3>
    </div><!-- /block-title-line -->
<?php
}
?>
