<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package elledilibro
 */
global $woocommerce;

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<?php  get_template_part("template-parts/common/svg");  ?>

<!-- Left menu element-->
<nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left perfect-scrollbar">
    <div class="main-nav-mobile">

    </div>
    <div class="secondary-nav-mobile">

    </div>
    <div class="utility-nav-mobile">

    </div>
    <div class="px-1 pb-1 d-sm-none mt-auto">
        <a class="btn btn-purple btn-sm w-100" href="<?php the_field("url_cta_primaria", "options"); ?>"><?php the_field("label_cta_primaria", "options"); ?></a>
    </div>
</nav>
<!-- End Left menu element-->

<div id="main-wrapper" class="push_container">
    <header id="main-header" class="header-container">
        <div class="container top-header-content">
            <div class="top-header-socials">
                <p><?php _e("Seguici su:", "elledilibro"); ?></p>
	            <?php
	            if($fburl = get_field("facebook_url", "options")){
		            echo '<a href="'.$fburl.'"><svg class="svg-social-facebook"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-social-facebook"></use></svg></a>';
	            }
	            if($igurl = get_field("instagram_url", "options")){
		            echo '<a href="'.$igurl.'"><svg class="svg-social-instagram"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-social-instagram"></use></svg></a>';
	            }
	            /*
				if($twurl = get_field("twitter_url", "options")){
					echo '<a href="'.$twurl.'"><svg class="svg-social-twitter"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-social-twitter"></use></svg></a>';
				}
				*/
	            ?>
            </div><!-- /top-header-socials -->
            <div class="top-header-utilities">
                <?php if(is_user_logged_in()){ ?>
                <a href="<?php echo home_url("my-account/my-favourite"); ?>">
                    <svg class="svg-wishlist"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-wishlist"></use></svg>
                </a>
                <?php } ?>
                <a class="top-header-cart" href="<?php echo  function_exists( 'wc_get_cart_url' ) ? wc_get_cart_url() : $woocommerce->cart->get_cart_url(); ?>">
	                <?php
	                $count_cart = $woocommerce->cart->get_cart_contents_count();
	                if($count_cart) echo "<span>".$count_cart."</span>";
	                ?>
                    <svg class="svg-cart"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-cart"></use></svg>
                </a>
                <?php if(is_user_logged_in()){ ?>
                    <a href="<?php echo get_permalink( wc_get_page_id( 'myaccount' ) ); ?>">
                        <svg class="svg-profile"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-profile"></use></svg>
                    </a>
                <?php } ?>
                <?php
                echo do_shortcode("[learndash_login]");
                ?>

            </div><!-- /top-header-utilities -->
        </div><!-- /top-header-content -->
        <div class="container header-content">
            <button class="hamburger hamburger--spin-r toggle-menu menu-left push-body d-xl-none" type="button">
            <span class="hamburger-box">
              <span class="hamburger-inner"></span>
            </span>
            </button>
            <a class="logo-wrapper" href="<?php echo home_url(); ?>">
                <svg width="100%" height="100%" viewBox="0 0 200 39" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" xmlns:serif="http://www.serif.com/" style="fill-rule:evenodd;clip-rule:evenodd;stroke-linejoin:round;stroke-miterlimit:2;"><path id="Fill-1" d="M187.209,15.978c-0,-0.948 0.162,-1.819 0.487,-2.614c0.324,-0.794 0.77,-1.481 1.338,-2.059c0.567,-0.579 1.239,-1.028 2.017,-1.349c0.777,-0.32 1.625,-0.481 2.544,-0.481c0.905,0 1.749,0.161 2.534,0.481c0.783,0.321 1.462,0.77 2.037,1.349c0.574,0.578 1.024,1.265 1.348,2.059c0.324,0.795 0.486,1.666 0.486,2.614l0,6.272c0,0.934 -0.162,1.798 -0.486,2.592c-0.324,0.795 -0.77,1.485 -1.338,2.07c-0.567,0.586 -1.24,1.043 -2.017,1.37c-0.777,0.327 -1.618,0.491 -2.524,0.491c-0.905,0 -1.75,-0.16 -2.534,-0.481c-0.783,-0.32 -1.463,-0.77 -2.037,-1.348c-0.574,-0.579 -1.028,-1.261 -1.358,-2.049c-0.332,-0.788 -0.497,-1.655 -0.497,-2.603l-0,-6.314Zm-52.441,-6.252l3.04,0l0,15.66l6.548,0l-0,3.136l-9.588,0l-0,-18.796Zm-52.827,0l3.04,0l0,15.66l6.548,0l0,3.136l-9.588,0l-0,-18.796Zm-12.832,0l3.041,0l-0,15.66l6.548,0l-0,3.136l-9.589,0l-0,-18.796Zm114.857,5.478c0,0.613 -0.102,1.188 -0.304,1.725c-0.203,0.537 -0.483,1.028 -0.841,1.474c-0.359,0.446 -0.774,0.833 -1.247,1.16c-0.473,0.328 -0.98,0.582 -1.52,0.763l3.912,6.545l-2.594,1.651l-4.663,-7.819l-2.514,-0l0,7.819l-3.04,0l-0,-18.817l6.406,0c0.783,0 1.557,0.119 2.321,0.356c0.763,0.237 1.449,0.585 2.057,1.045c0.608,0.46 1.098,1.035 1.47,1.725c0.371,0.69 0.557,1.481 0.557,2.373Zm-128.256,-2.342l-0,4.684l6.548,-0l-0,3.136l-6.548,-0l-0,4.704l9.75,0l0,3.136l-12.791,0l-0,-18.796l12.791,0l0,3.136l-9.75,0Zm42.103,0l-0,4.684l6.547,-0l0,3.136l-6.547,-0l-0,4.704l9.75,0l0,3.136l-12.791,0l0,-18.796l12.791,0l0,3.136l-9.75,0Zm49.787,15.66l3.04,0l-0,-18.817l-3.04,0l-0,18.817Zm18.122,-9.408c0.527,0.53 0.956,1.115 1.287,1.756c0.331,0.641 0.496,1.359 0.496,2.153c0,0.907 -0.185,1.701 -0.557,2.384c-0.372,0.683 -0.859,1.254 -1.46,1.714c-0.601,0.46 -1.286,0.809 -2.057,1.046c-0.77,0.237 -1.548,0.355 -2.331,0.355l-6.405,0l-0,-18.796l6.405,0c0.783,0 1.557,0.119 2.321,0.356c0.764,0.237 1.449,0.585 2.058,1.045c0.608,0.46 1.097,1.031 1.469,1.714c0.372,0.683 0.557,1.471 0.557,2.363c0,0.753 -0.161,1.467 -0.486,2.143c-0.324,0.676 -0.757,1.265 -1.297,1.767Zm27.873,-6.503c-0.487,0 -0.936,0.081 -1.349,0.241c-0.412,0.161 -0.763,0.388 -1.054,0.681c-0.291,0.293 -0.52,0.649 -0.689,1.068c-0.169,0.419 -0.253,0.88 -0.253,1.382l0,6.282c0,0.503 0.084,0.964 0.253,1.382c0.169,0.419 0.398,0.775 0.689,1.069c0.291,0.293 0.642,0.52 1.054,0.68c0.413,0.161 0.862,0.241 1.349,0.241c0.486,-0 0.935,-0.08 1.347,-0.241c0.413,-0.16 0.768,-0.387 1.065,-0.68c0.298,-0.294 0.53,-0.646 0.699,-1.058c0.169,-0.412 0.253,-0.876 0.253,-1.393l0,-6.282c0,-0.516 -0.084,-0.981 -0.253,-1.393c-0.169,-0.411 -0.401,-0.764 -0.699,-1.057c-0.297,-0.293 -0.652,-0.52 -1.065,-0.681c-0.412,-0.16 -0.861,-0.241 -1.347,-0.241Zm-32.495,12.775c0.351,0 0.722,-0.038 1.115,-0.115c0.391,-0.076 0.756,-0.209 1.094,-0.397c0.338,-0.188 0.615,-0.432 0.832,-0.732c0.216,-0.3 0.324,-0.672 0.324,-1.119c-0,-0.376 -0.098,-0.71 -0.294,-1.003c-0.196,-0.293 -0.44,-0.54 -0.73,-0.742c-0.291,-0.202 -0.611,-0.352 -0.963,-0.45c-0.351,-0.097 -0.689,-0.146 -1.013,-0.146l-3.731,0.021l0,4.683l3.366,0Zm-3.366,-12.524l0,4.705l3.731,-0c0.324,-0 0.662,-0.049 1.013,-0.148c0.352,-0.097 0.676,-0.244 0.974,-0.44c0.297,-0.197 0.54,-0.441 0.729,-0.736c0.19,-0.294 0.284,-0.637 0.284,-1.028c-0,-0.448 -0.112,-0.823 -0.335,-1.124c-0.222,-0.301 -0.499,-0.542 -0.831,-0.725c-0.33,-0.182 -0.696,-0.311 -1.094,-0.388c-0.399,-0.077 -0.767,-0.116 -1.105,-0.116l-3.366,0Zm16.461,4.704l3.366,0c0.351,0 0.722,-0.038 1.115,-0.114c0.391,-0.077 0.756,-0.21 1.094,-0.398c0.337,-0.188 0.614,-0.432 0.831,-0.731c0.215,-0.3 0.324,-0.673 0.324,-1.119c0,-0.446 -0.109,-0.819 -0.324,-1.119c-0.217,-0.299 -0.494,-0.543 -0.831,-0.731c-0.338,-0.189 -0.703,-0.321 -1.094,-0.398c-0.393,-0.076 -0.764,-0.115 -1.115,-0.115l-3.366,0l0,4.725Z" style="fill:#191616;"/><path id="Fill-12" d="M0,4.5l9.375,-4.5l16.198,33.749l-9.375,4.499l-16.198,-33.748Z" style="fill:url(#_Linear1);"/><path id="Fill-15" d="M35.298,17.547l4.5,9.375l-23.6,11.326l-4.499,-9.375l23.599,-11.326Zm92.374,10.975l3.041,0l-0,-18.817l-3.041,0l0,18.817Zm-16.459,0l-0,-18.817l6.405,0c0.906,0 1.696,0.125 2.372,0.377c0.676,0.25 1.249,0.596 1.723,1.035c0.473,0.439 0.858,0.961 1.156,1.568c0.296,0.606 0.534,1.258 0.709,1.955c0.176,0.697 0.294,1.425 0.355,2.184c0.061,0.76 0.091,1.523 0.091,2.29c0,1.157 -0.075,2.299 -0.223,3.429c-0.149,1.129 -0.449,2.136 -0.902,3.021c-0.453,0.885 -1.102,1.599 -1.946,2.143c-0.845,0.543 -1.956,0.815 -3.335,0.815l-6.405,0Zm3.04,-3.136l3.365,0c0.864,0 1.528,-0.198 1.987,-0.596c0.459,-0.397 0.79,-0.906 0.993,-1.526c0.203,-0.62 0.317,-1.296 0.345,-2.028c0.027,-0.732 0.041,-1.433 0.041,-2.101c-0,-0.655 -0.018,-1.352 -0.051,-2.091c-0.034,-0.739 -0.153,-1.418 -0.355,-2.039c-0.203,-0.62 -0.535,-1.136 -0.994,-1.547c-0.459,-0.411 -1.114,-0.617 -1.966,-0.617l-3.365,0l0,12.545Z" style="fill:#aa4282;"/><defs><linearGradient id="_Linear1" x1="0" y1="0" x2="1" y2="0" gradientUnits="userSpaceOnUse" gradientTransform="matrix(6.91988,31.4604,-31.4604,6.91988,9.43082,4.38884)"><stop offset="0" style="stop-color:#9b4b89;stop-opacity:1"/><stop offset="1" style="stop-color:#792765;stop-opacity:1"/></linearGradient></defs></svg>
            </a>

	        <?php wp_nav_menu( array( 'theme_location' => 'primary_menu', 'container' => 'nav', 'container_class' => 'main-nav', 'container_id' => 'main-nav','items_wrap' => '<ul class="main-menu" id="%1$s" class="%2$s">%3$s</ul>' ) ); ?>

            <div class="header-utilities">
                <div class="header-cta">
                    <a class="btn btn-purple d-none d-sm-block" href="<?php the_field("url_cta_primaria", "options"); ?>"><?php the_field("label_cta_primaria", "options"); ?></a>
                </div><!-- /header-cta -->
                <div class="header-search">
                    <button id="btn-search" class="btn--search"><svg class="svg-search"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-search"></use></svg></button>
                </div><!-- /header-search -->
            </div><!-- /header-utilities -->
        </div><!-- /header-content -->

    </header><!-- /header -->
