<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package elledilibro
 */

get_header();
global $product;

get_template_part("template-parts/common/breadcrumb", "", array("type" => "white"));

$queried_object = get_queried_object();
if ( $queried_object ) {
	$tax   = get_taxonomy( $queried_object->taxonomy );
	$title = single_term_title( '', false );
}
?>
<?php
//print_r($queried_object);
?>
	<section class="section section-archive">
		<div class="container">
			<div class="row justify-content-center sticky-sidebar-container">
				<div class="col-lg-9 order-lg-1">
					<?php
                    if(elle_is_taxonomy_corsi($queried_object)){
	                    get_template_part("template-parts/archive/products", "corsi");
                    }else if(elle_is_taxonomy_servizi($queried_object)){
	                    get_template_part("template-parts/archive/products", "servizi");
                    }else{
	                    get_template_part("template-parts/archive/products", $queried_object->slug);
                    }

					?>
				</div><!-- /col-lg-9 -->
				<div class="col-lg-3 order-lg-0">
					<?php
					// trovo la categoria principale per governare la sidebar, assumento massimo 2 livelli
					$mainslug = $queried_object->slug;
					if($queried_object->parent){
						$parent = get_term_by("id", $queried_object->parent, "product_cat");
						$mainslug = $parent->slug;
					}
//                    echo "----".$mainslug;
					switch($mainslug){
						case "servizi":
							get_sidebar("servizi");
							break;
						case "corsi":
							get_sidebar("corsi");
							break;
						case "libri":
							get_sidebar("generi");
							break;
						default:
                            break;
					}
					?>
				</div><!-- /col-lg-3 -->
			</div><!-- /row -->
		</div><!-- /container -->
	</section><!-- /section -->

<?php // get_template_part("template-parts/common/promo"); ?>

<?php get_template_part("template-parts/common/newsletter"); ?>

<?php
get_footer();


