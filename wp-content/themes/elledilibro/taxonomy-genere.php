<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package elledilibro
 */

get_header();

get_template_part("template-parts/common/breadcrumb", "", array("type" => "white"));
?>

    <section class="section section-archive">
        <div class="container">
            <div class="row justify-content-center sticky-sidebar-container">
                <div class="col-lg-9 order-lg-1">
					<?php
					if($_REQUEST["post_type"] == "autore"){
                        get_template_part("template-parts/archive/autori");
					}else{
						get_template_part("template-parts/archive/products", "genere");
					}
					?>
                </div><!-- /col-lg-9 -->
                <div class="col-lg-3 order-lg-0">
					<?php  get_sidebar("generi", array("origin" => $_REQUEST["post_type"])); ?>
                </div><!-- /col-lg-3 -->
            </div><!-- /row -->
        </div><!-- /container -->
    </section><!-- /section -->

<?php get_template_part("template-parts/common/promo"); ?>

<?php get_template_part("template-parts/common/newsletter"); ?>

<?php
get_footer();
