/* Scripts Functions */

/* Responsive DOM */
var $movenav = $('#main-nav');
$movenav.responsiveDom({
  appendTo: '.main-nav-mobile',
  mediaQuery: '(max-width: 1170px)'
});
var $movesecondarynav = $('#secondary-nav');
$movesecondarynav.responsiveDom({
  appendTo: '.secondary-nav-mobile',
  mediaQuery: '(max-width: 1170px)'
});
var $moveutilitynav = $('#utility-nav');
$moveutilitynav.responsiveDom({
  appendTo: '.utility-nav-mobile',
  mediaQuery: '(max-width: 1170px)'
});
/* End Responsive DOM */

/* jPush Menu */
jQuery(document).ready(function($) {
	$('.toggle-menu').jPushMenu({
  	activeClass: 'is-active',
  	closeOnClickLink: false
	});
});
/* End jPush Menu */

/* Perfect Scrollbar */
$(document).ready(function ($) {
  $('.perfect-scrollbar').perfectScrollbar();
});
/* End Perfect Scrollbar */

/* Header Scroll */
$(document).ready(function() {
  function header_relocate() {
    var window_top = $(window).scrollTop();
    if (window_top >= 80) {
      $('.header-container').addClass('scrolled');
      $('body').addClass('scrolled-header');
    } else {
      $('.header-container').removeClass('scrolled');
      $('body').removeClass('scrolled-header');
    }
  }
  $(function() {
    $(window).scroll(header_relocate);
    header_relocate();
  });
});
/* Home Header Scroll */

/* Carousel */
jQuery(document).ready(function($) {
  if ($(".owl-theme").length) {
    $(".carousel-single-nav-dots").owlCarousel({
      nav: true,
      navText: ['<svg class="svg-arrow-left"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-arrow-left"></use></svg>','<svg class="svg-arrow-right"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-arrow-right"></use></svg>'],
      dots: true,
      margin: 0,
      loop: true,
      autoplay: false,
      autoplayTimeout: 4000,
      autoplayHoverPause: true,
      items: 1,
      stagePadding: 0,
      margin: 0,
      autoHeight: true
    });
    $(".carousel-single-nav").owlCarousel({
      nav: true,
      navText: ['<svg class="svg-arrow-left"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-arrow-left"></use></svg>','<svg class="svg-arrow-right"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-arrow-right"></use></svg>'],
      dots: false,
      margin: 0,
      loop: true,
      autoplay: false,
      autoplayTimeout: 4000,
      autoplayHoverPause: true,
      items: 1,
      stagePadding: 0,
      margin: 0,
      autoHeight: true
    });
    $(".carousel-double-nav").owlCarousel({
      nav: true,
      navText: ['<svg class="svg-arrow-left"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-arrow-left"></use></svg>','<svg class="svg-arrow-right"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-arrow-right"></use></svg>'],
      dots: false,
      margin: 30,
      loop: false,
      autoplay: false,
      autoplayTimeout: 4000,
      autoplayHoverPause: true,
      responsive: {
        0: {
        items: 1,
        stagePadding: 0,
        margin: 0,
        },
        576: {
        items: 2,
        stagePadding: 0,
        },
        768: {
        items: 2,
        stagePadding: 0,
        },
        992: {
        items: 2,
        stagePadding: 0,
        },
        1420: {
        items: 2,
        stagePadding: 0,
        }
      }
    });
    $(".carousel-four-items").owlCarousel({
      nav: true,
      navText: ['<svg class="svg-arrow-left"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-arrow-left"></use></svg>','<svg class="svg-arrow-right"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-arrow-right"></use></svg>'],
      dots: false,
      margin: 30,
      loop: false,
      autoplay: false,
      autoplayTimeout: 4000,
      autoplayHoverPause: true,
      responsive: {
        0: {
        items: 1,
        stagePadding: 0,
        margin: 0,
        },
        576: {
        items: 2,
        stagePadding: 0,
        },
        768: {
        items: 3,
        stagePadding: 0,
        },
        992: {
        items: 4,
        stagePadding: 0,
        },
        1420: {
        items: 4,
        stagePadding: 0,
        }
      }
    });
    $(".carousel-three-items").owlCarousel({
      nav: true,
      navText: ['<svg class="svg-arrow-left"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-arrow-left"></use></svg>','<svg class="svg-arrow-right"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-arrow-right"></use></svg>'],
      dots: false,
      margin: 30,
      loop: false,
      autoplay: false,
      autoplayTimeout: 4000,
      autoplayHoverPause: true,
      responsive: {
        0: {
        items: 1,
        stagePadding: 0,
        margin: 0,
        },
        576: {
        items: 2,
        stagePadding: 0,
        },
        768: {
        items: 2,
        stagePadding: 0,
        },
        992: {
        items: 3,
        stagePadding: 0,
        },
        1420: {
        items: 3,
        stagePadding: 0,
        }
      }
    });
    $(".carousel-single-photo").owlCarousel({
      nav: true,
      navText: ['<svg class="svg-arrow-left"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-arrow-left"></use></svg>','<svg class="svg-arrow-right"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-arrow-right"></use></svg>'],
      dots: false,
      margin: 0,
      loop: true,
      autoplay: false,
      autoplayTimeout: 4000,
      autoplayHoverPause: true,
      items: 1,
      stagePadding: 0,
      autoHeight: true,
      responsive: {
        0: {
        items: 1,
        stagePadding: 12,
        margin: 12,
        },
        767: {
        items: 1,
        stagePadding: 0,
        margin: 0,
        }
      }
    });
  }
});
/* End Carousel */

/* Share Buttons */
jQuery(document).ready(function($) {
  if (jQuery("#share-buttons").length) {
    jQuery("#share-buttons").jsSocials({
      shares: ["facebook", "linkedin", "twitter", "whatsapp"],
      showCount: false,
      showLabel: false
    });
  }
});
/* End Share Buttons */

/* LightGallery */
$('.lightgallery').lightGallery({
  videojs: true,
  thumbnail: true,
  rotate: false,
  flipHorizontal: false,
  flipVertical: false,
  actualSize: false,
  autoplayFirstVideo: false,
  autoplay: false,
  autoplayControls: false,
  download: false,
});
/* End LightGallery */

/* Accordion */
function accordion() {
  // Add "inactive" class to all headers.
  $('.accordion-header').toggleClass('accordion-inactive');
  // Add "closed" class to all content divs.
  $('.accordion-content').toggleClass('accordion-closed');
  // Open the first content section when the page loads.
  $('.accordion-header').first().toggleClass('accordion-active accordion-inactive');
  $('.accordion-content').first().toggleClass('accordion-open accordion-closed').slideDown();
  // The Accordion Effect
  $('.accordion-header').click(function () {
    if($(this).is('.accordion-inactive')) {
      $('.accordion-active').toggleClass('accordion-active accordion-inactive').next().slideToggle().toggleClass('accordion-open');
      $(this).toggleClass('accordion-active accordion-inactive');
      $(this).next().slideToggle().toggleClass('accordion-open');
    }
    else {
      $(this).toggleClass('accordion-active accordion-inactive');
      $(this).next().slideToggle().toggleClass('accordion-open');
    }
  });
}
$(document).ready(function() {
  accordion();
});
/* End Accordion */

/* Sticky Sidebar */
jQuery(document).ready(function($) {
  if ($(".sticky-sidebar").length) {
    var window_width = jQuery( window ).width();
    if (window_width < 992) {
      jQuery(".sticky-sidebar").trigger("sticky_kit:detach");
    } else {
      make_sticky();
    }
    jQuery( window ).resize(function() {
      window_width = jQuery( window ).width();
      if (window_width < 992) {
        jQuery(".sticky-sidebar").trigger("sticky_kit:detach");
      } else {
        make_sticky();
      }
    });
    function make_sticky() {
      jQuery(".sticky-sidebar").stick_in_parent({
        parent: ".sticky-sidebar-container",
        offset_top: 120
      });
      jQuery(".sticky-sidebar")
      .on("sticky_kit:bottom", function(e) {
          jQuery(this).parent().css("position", "static");
      })
      .on("sticky_kit:unbottom", function(e) {
          jQuery(this).parent().css("position", "relative");
      })
    }
  }
});
/* End Sticky Sidebar */

/* Search */
;(function(window) {

  'use strict';

  var mainContainer = document.querySelector('#main-wrapper'),
    openCtrl = document.getElementById('btn-search'),
    closeCtrl = document.getElementById('btn-search-close'),
    searchContainer = document.querySelector('.search'),
    inputSearch = searchContainer.querySelector('.search__input');

  function init() {
    initEvents();	
  }

  function initEvents() {
    openCtrl.addEventListener('click', openSearch);
    closeCtrl.addEventListener('click', closeSearch);
    document.addEventListener('keyup', function(ev) {
      // escape key.
      if( ev.keyCode == 27 ) {
        closeSearch();
      }
    });
  }

  function openSearch() {
    mainContainer.classList.add('main-wrap--move');
    searchContainer.classList.add('search--open');
    setTimeout(function() {
      inputSearch.focus();
    }, 600);
  }

  function closeSearch() {
    mainContainer.classList.remove('main-wrap--move');
    searchContainer.classList.remove('search--open');
    inputSearch.blur();
    inputSearch.value = '';
  }

  init();

})(window);
/* End Search */

/* Tippy */
tippy('.tippy-dark', {
  duration: 0,
  arrow: true,
  delay: [200, 200],
  placement: 'left',
});
tippy('.tippy-purple', {
  duration: 0,
  arrow: true,
  delay: [200, 200],
  placement: 'right',
  theme: 'material'
});
/* End Tippy */

/* Toggle Share */
$(document).ready(function() {
  if ($(".toggle-share").length) {
    $(".toggle-share").on("click", function(e){
      if ( $(this).next(".share-buttons-container").hasClass("active") ) {
        $(this).next(".share-buttons-container").slideUp(50);
        $(this).next(".share-buttons-container").removeClass("active");
        $(this).removeClass("active");
      } else {
        $(this).addClass("active");
        $(this).next(".share-buttons-container").slideDown(50);
        $(this).next(".share-buttons-container").addClass("active");
      }
      e.stopPropagation();
      e.preventDefault();
    });
    $(document).click(function (e) {
      $('.share-buttons-container').slideUp(50);
      $('.share-buttons-container').removeClass('active');
      $('.toggle-share').removeClass('active');
    });
    $('.share-buttons-container').click(function (e) {
      e.stopPropagation();
    });
  }
});
/* End Toggle Share */

/* Input Number Increment Decrement */
function incrementValue(e) {
  e.preventDefault();
  var fieldName = $(e.target).data('field');
  var parent = $(e.target).closest('.plus-minus-group');
  var currentVal = parseInt(parent.find('input[name=' + fieldName + ']').val(), 10);
  if (!isNaN(currentVal)) {
    parent.find('input[name=' + fieldName + ']').val(currentVal + 1);
  } else {
    parent.find('input[name=' + fieldName + ']').val(0);
  }
}
function decrementValue(e) {
  e.preventDefault();
  var fieldName = $(e.target).data('field');
  var parent = $(e.target).closest('.plus-minus-group');
  var currentVal = parseInt(parent.find('input[name=' + fieldName + ']').val(), 10);
  if (!isNaN(currentVal) && currentVal > 0) {
    parent.find('input[name=' + fieldName + ']').val(currentVal - 1);
  } else {
    parent.find('input[name=' + fieldName + ']').val(0);
  }
}
$('.button-plus').on('click', function(e) {
  incrementValue(e);
});
$('.button-minus').on('click', function(e) {
  decrementValue(e);
});
/* End Input Number Increment Decrement */

/* Datepicker */
$(".datepicker-single").caleran({
  singleDate: true,
  inline: true,
  calendarCount: 1,
  showHeader: false,
  showFooter: false,
  locale: "it",
  startOnMonday: true
});
/* End Datepicker */

/* FitVids */
jQuery(document).ready(function($) {
  if (jQuery(".block-content-video").length) {
    $(".block-content-video").fitVids();
  }
});
/* End FitVids */