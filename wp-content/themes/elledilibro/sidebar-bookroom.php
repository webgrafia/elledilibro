<?php

?>
<aside class="sticky-sidebar">
    <div class="datepicker-wrapper mb-3 mb-lg-1">
        <div class="form-group">
            <input type="text" class="form-control datepicker-single1" placeholder="Scegli la data">
        </div><!-- /form-group -->
    </div><!-- /datepicker-wrapper -->
	<?php dynamic_sidebar( 'sidebar-1' ); ?>
</aside>
