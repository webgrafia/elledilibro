<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package elledilibro
 */

get_header();

// get_template_part("template-parts/common/breadcrumb");

?>

	<section class="section section-main-title section-padding-bottom bg-gray-light">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-lg-7">
					<h1><?php the_title(); ?></h1>
				</div><!-- /col-lg-7 -->
			</div><!-- /row -->
		</div><!-- /container -->
	</section><!-- /section -->

<?php while ( have_posts() ) : the_post();  ?>
	<main role="main" class="article-wrapper">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-lg-7 sticky-sidebar-container">
					<article class="article-container">

						<?php the_content(); ?>
					</article>
				</div><!-- /col-lg-7 -->
			</div><!-- /row -->
		</div><!-- /container -->
	</main><!-- /main -->


<?php
endwhile;

get_footer();
