<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package elledilibro
 */

get_header();

get_template_part("template-parts/common/breadcrumb");

?>

    <section class="section section-main-title section-padding-bottom bg-gray-light">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-7">
                    <h1><?php the_title(); ?></h1>
                    <span class="category">
						<?php
						the_category(", ");
						?>
                    </span>
                </div><!-- /col-lg-7 -->
            </div><!-- /row -->
        </div><!-- /container -->
    </section><!-- /section -->


<?php
$tipologia_apertura = get_field("tipologia_apertura");
if($tipologia_apertura == "podcast"){
	$subclass = $tipologia_apertura;
	$content = get_field("embed");
}else if($tipologia_apertura == "video"){
	$subclass = $tipologia_apertura;
	$content = wp_oembed_get(get_field("url_video"));
}else{
	$subclass = "image";
	$content = get_the_post_thumbnail( $post->ID, 'featured-post' );
}
while ( have_posts() ) : the_post();
	?>
    <main role="main" class="article-wrapper">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-7 sticky-sidebar-container">
                    <article class="article-container">
                        <div class="block block-content-<?php echo $subclass; ?> block-overlayed">
							<?php echo $content; ?>
                            <div class="content-details">
                                <span class="date"><?php the_date(); ?></span>
                                <span class="author">Articolo di <?php the_author(); ?></span>
                            </div><!-- /content-details -->
                        </div><!-- /block -->
						<?php the_content(); ?>

                        <?php
                        if(is_user_logged_in()){
                        ?>
                        <hr/>
                        <div class="d-flex pt-2">
                            <span class="link-icon" href="#">
                                <div class="favourite">
                                    <?php the_favorites_button($args->ID); ?>
                                </div>
                            <span>Aggiungi ai preferiti</span>
                          </span>
                        </div>
                            <?php
                        }
                            ?>
                    </article>
					<?php get_template_part("template-parts/common/share", "floating"); ?>
                </div><!-- /col-lg-7 -->
            </div><!-- /row -->
        </div><!-- /container -->
    </main><!-- /main -->


	<?php get_template_part("template-parts/single/related", get_post_type(), $post); ?>

	<?php get_template_part("template-parts/common/newsletter"); ?>

<?php
endwhile;

get_footer();
