<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package elledilibro
 */

get_header();
$slider_featured_autori = get_field("slider_featured_autori", "options");
$slider_archivio_autori = get_field("slider_archivio_autori", "options");

get_template_part("template-parts/common/breadcrumb", "", array("type" => "white"));
?>

    <section class="section section-archive">
        <div class="container">
            <div class="row justify-content-center sticky-sidebar-container">
                <div class="col-lg-9 order-lg-1">
					<?php
					if($_REQUEST["order"]){
                        get_template_part("template-parts/archive/autori");
					}else{
						if($slider_featured_autori){
                            get_template_part("template-parts/common/slider", "", array("slider_featured" => $slider_featured_autori));
						}
						if($slider_archivio_autori){
							foreach ($slider_archivio_autori as $item ) {
								?>
                                <div class="block block-carousel-authors pb-2 pb-lg-3 pb-xl-4">
                                    <div class="section-title text-center">
                                        <h2><?php echo $item["titolo"]; ?></h2>
                                    </div><!-- /section-title -->
                                    <div class="owl-carousel owl-theme carousel-four-items">
										<?php
										foreach ( $item["autori"] as $autore ) {
											get_template_part("template-parts/card/card", "autore", $autore);
										}
										?>
                                    </div><!-- /carousel-four-items -->
                                </div><!-- /block -->
								<?php
							}
						}
					}
					?>
                </div><!-- /col-lg-9 -->
                <div class="col-lg-3 order-lg-0">
					<?php  get_sidebar("generi", array("origin" => "autore")); ?>
                </div><!-- /col-lg-3 -->
            </div><!-- /row -->
        </div><!-- /container -->
    </section><!-- /section -->

<?php get_template_part("template-parts/common/promo"); ?>

<?php get_template_part("template-parts/common/newsletter"); ?>

<?php
get_footer();
