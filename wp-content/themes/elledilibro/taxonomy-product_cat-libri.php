<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package elledilibro
 */

get_header();
$slider_featured_libri = get_field("slider_featured_libri", "options");

get_template_part("template-parts/common/breadcrumb", "", array("type" => "white"));
?>

    <section class="section section-archive">
        <div class="container">
            <div class="row justify-content-center sticky-sidebar-container">
                <div class="col-lg-9 order-lg-1">
					<?php
					if($_REQUEST["order"]){
                        if($_REQUEST["order"] == "asc")
                            $subtitle = "A-Z";
                        else
	                        $subtitle = "Z-A";
						get_template_part("template-parts/archive/products", "libri", array("title" => "Ordine Alfabetico ".$subtitle));
					}else{
						if($slider_featured_libri){
                            get_template_part("template-parts/common/slider", "", array("slider_featured" => $slider_featured_libri));
						}


					//get_template_part("template-parts/archive/products", "libri");
					?>
	                <?php
	                $fasce_libri = get_field("fasce_libri", "options");
	                foreach ($fasce_libri as $libri){
		                ?>
                        <div class="block block-carousel-books pb-2 pb-lg-3 pb-xl-4">
                            <div class="section-title text-center">
                                <h2><?php echo $libri["titolo"]; ?></h2>
                            </div><!-- /section-title -->
                            <div class="owl-carousel owl-theme carousel-four-items">
				                <?php
				                foreach ( $libri  ["libri"] as $libro ) { ?>
						                <?php get_template_part("template-parts/card/card", "libro", $libro); ?>
					                <?php
				                }
				                ?>
                            </div>
                        </div><!-- /block -->
		                <?php
	                }
					}
	                ?>
                </div><!-- /col-lg-9 -->
                <div class="col-lg-3 order-lg-0">
					<?php  get_sidebar("generi"); ?>
                </div><!-- /col-lg-3 -->
            </div><!-- /row -->
        </div><!-- /container -->
    </section><!-- /section -->

<?php // get_template_part("template-parts/common/promo"); ?>

<?php get_template_part("template-parts/common/newsletter"); ?>

<?php
get_footer();
