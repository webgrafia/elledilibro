<?php


/**
 * @param $TEMPLATE_NAME
 * @return string|null
 */
if(!function_exists("get_url_by_template")){
	function get_url_by_template($TEMPLATE_NAME){
		$url = null;
		$pages = get_pages(array(
			'meta_key' => '_wp_page_template',
			'meta_value' => $TEMPLATE_NAME
		));
		if(isset($pages[0])) {
			$url = get_page_link($pages[0]->ID);
		}
		return $url;
	}
}

/**
 * lista autori correlati al libro
 * @param $book
 *
 * @return array
 */
if(!function_exists("elle_get_autori")) {
    function elle_get_autori($book)
    {
        $ret = array();
        $correlati = get_field("relazione_libro_autore", $book->ID) ?: [];
        foreach ($correlati as $item) {
            if ($item->post_type == "autore") {
                $ret[] = $item;
            }
        }
        return $ret;
    }
}

/**
 * lista libri correlati all'autore
 * @param $autore
 *
 * @return array
 */
if(!function_exists("elle_get_libri")) {
    function elle_get_libri($autore)
    {
        $ret = array();
        $correlati = get_field("relazione_libro_autore", $autore->ID);
        foreach ($correlati as $item) {
            if ($item->post_type == "product") {
                $ret[] = $item;
            }
        }
        return $ret;
    }
}

/**
 *
 * @return mixed|null
 */
if(!function_exists("elle_get_price_html")) {
    function elle_get_price_html($product)
    {
        $price = 0;
        $label_from = false;

        switch ($product->get_type()) {
            case 'libro':
                foreach ($product->edl_get_available_variations() as $variation) {
                    if ($variation['name'] == 'Cartaceo') {
                        $price = $variation['price'];
	                    $regular_price = $variation['regular_price'];

	                    if ( '' !== (string) $price && $regular_price > $price ) {
		                    $on_sale = true;
	                    } else {
		                    $on_sale = false;
	                    }
                    }
                }
                break;
            case 'pacchetto':
            case 'servizio':
                $label_from = $product->get_type() == 'pacchetto';
                if ($product->get_folders_enabled()) {
                    $folders = $product->get_folders();
                    $price = array_values($folders)[0]['price'];
                } elseif (!empty($ranges = $product->get_ranges())) {
                    $label_from = true;
                    $price = array_values($ranges)[0]['price'];
                } else {
                    $price = $product->get_price();
                }
                break;
            case 'course':
                $price = $product->get_price();
                break;
            default:
                $price = wc_get_price_to_display($product);
        }

        $price = str_replace(",", ".", $price);

		if(!$regular_price)
	        $regular_price = $product->get_regular_price();

		if($on_sale == "")
			$on_sale = $product->is_on_sale();


        $giftcard = class_exists('WC_GC_Gift_Card_Product') && WC_GC_Gift_Card_Product::is_gift_card( $product );

        $label_from = $giftcard ? true : $label_from;

        $html_price = '<small>'. ( $label_from ? ($product->get_type() == 'servizio' ? 'A cartella' : 'A partire da ') : '') .'</small><span class="price-main">' .  wc_price($price) . $product->get_price_suffix() . '</span>';
        if ('' === $price) {
            $html_price = apply_filters('woocommerce_empty_price_html', '', $product);
        } elseif ($on_sale && $regular_price != 0) {
            $html_price .= '<span class="price-full">' . wc_price($regular_price) . $product->get_price_suffix() . '</span>';
        }

        return apply_filters('woocommerce_get_price_html', $html_price, $product);
    }
}

if(!function_exists("elle_get_product_categories")) {
    function elle_get_terms($post_id, $post_type = 'post')
    {
        $transient = get_transient( 'post_taxonomies_' . $post_id );
        if ( $transient ) {
            return $transient;
        }

        $categories = [];
        $subcategories = [];
        $terms = wp_get_object_terms( $post_id, get_object_taxonomies( $post_type ) );

        if ( $terms && ! is_wp_error( $terms ) ) {
            foreach ($terms as $term) {
                $term_link = get_term_link( $term, $post_type ); // The term link
                $data = [
                    'id' => $term->term_id,
                    'name' => $term->name,
                    'slug' => $term->slug,
                    'taxonomy' => $term->taxonomy,
                    'link' => $term_link
                ];
                if($term->parent == 0) {
                    $categories[$term->taxonomy][$term->slug] = $data;
                } else {
                    $subcategories[$term->parent] = $data;
                }
            }

            foreach ($categories as $taxonomy => $terms) {
                foreach ($terms as $term) {
                    if(!empty($subcategories[$term['id']])) {
                        $categories[$taxonomy][$term['slug']]['subcategories'] = $subcategories[$term['id']];
                    }
                }
            }
        }

        set_transient( 'post_taxonomies_' . $post_id, $categories, WEEK_IN_SECONDS ); // change expiration date as needed

        return $categories;
    }
}

if(!function_exists("elle_get_product_cat")) {
    function elle_get_product_cat($product_id, $category, $subcategory = false)
    {
        $cat = [];
        $terms = elle_get_terms($product_id, 'product');
        $terms = $terms['product_cat'][$category] ?? [];
        if(!empty($terms)) {
            $cat = $subcategory ? $terms['subcategories'] : $terms;
        }

        return $cat;
    }
}

/**
 * @param $queried_object
 *
 * @return bool
 */
function elle_is_taxonomy_corsi($queried_object){

	$corso = get_term_by("slug", "corsi", "product_cat");
	$idcorso = $corso->term_id;
	if($queried_object->term_id == $idcorso || $queried_object->parent == $idcorso)
		return true;

	return false;
}

function elle_is_taxonomy_libri($queried_object){

	$corso = get_term_by("slug", "libri", "product_cat");
	$idcorso = $corso->term_id;
	if($queried_object->term_id == $idcorso || $queried_object->parent == $idcorso)
		return true;

	return false;
}


function elle_is_taxonomy_servizi($queried_object){

	$corso = get_term_by("slug", "servizi", "product_cat");
	$idcorso = $corso->term_id;
	if($queried_object->term_id == $idcorso || $queried_object->parent == $idcorso)
		return true;

	return false;
}