<?php


add_filter('wpcf7_autop_or_not', '__return_false');


/**
 * @param $value
 * @param $post_id
 * @param $field
 *
 * @return mixed
 */
function bidirectional_acf_update_value( $value, $post_id, $field  ) {

	// vars
	$field_name = $field['name'];
	$field_key = $field['key'];
	$global_name = 'is_updating_' . $field_name;


	// bail early if this filter was triggered from the update_field() function called within the loop below
	// - this prevents an inifinte loop
	if( !empty($GLOBALS[ $global_name ]) ) return $value;


	// set global variable to avoid inifite loop
	// - could also remove_filter() then add_filter() again, but this is simpler
	$GLOBALS[ $global_name ] = 1;


	// loop over selected posts and add this $post_id
	if( is_array($value) ) {

		foreach( $value as $post_id2 ) {

			// load existing related posts
			$value2 = get_field($field_name, $post_id2, false);


			// allow for selected posts to not contain a value
			if( empty($value2) ) {

				$value2 = array();

			}


			// bail early if the current $post_id is already found in selected post's $value2
			if( in_array($post_id, $value2) ) continue;


			// append the current $post_id to the selected post's 'related_posts' value
			$value2[] = $post_id;


			// update the selected post's value (use field's key for performance)
			update_field($field_key, $value2, $post_id2);

		}

	}


	// find posts which have been removed
	$old_value = get_field($field_name, $post_id, false);

	if( is_array($old_value) ) {

		foreach( $old_value as $post_id2 ) {

			// bail early if this value has not been removed
			if( is_array($value) && in_array($post_id2, $value) ) continue;


			// load existing related posts
			$value2 = get_field($field_name, $post_id2, false);


			// bail early if no value
			if( empty($value2) ) continue;


			// find the position of $post_id within $value2 so we can remove it
			$pos = array_search($post_id, $value2);


			// remove
			unset( $value2[ $pos] );


			// update the un-selected post's value (use field's key for performance)
			update_field($field_key, $value2, $post_id2);

		}

	}


	// reset global varibale to allow this filter to function as per normal
	$GLOBALS[ $global_name ] = 0;


	// return
	return $value;

}

add_filter('acf/update_value/name=relazione_libro_autore', 'bidirectional_acf_update_value', 10, 3);


/**
 * @param $query
 *
 * @return void
 */
function elle_modify_query_order( $query ) {
	if( is_admin() ) {
		return $query;
	}

	if ( is_main_query() ) {
		if ( is_post_type_archive( "autore" ) ) {
			if ( $_REQUEST["order"] ) {
				$args = array( 'title' => strtoupper( $_REQUEST["order"] ) );
				$query->set( 'orderby', $args );
			}
		}

		if ( is_tax( "product_cat","libri" ) ) {
			if ( $_REQUEST["order"] ) {
				$args = array( 'title' => strtoupper( $_REQUEST["order"] ) );
				$query->set( 'orderby', $args );
			}
		}

		if( ( isset( $query->query_vars['genere'] ) && $query->query_vars['genere'] != '' ) ){
			if(!$_REQUEST["post_type"]){
				$query->set( 'post_type', array( 'product' ) );
			}
		}

		if (
			( isset( $query->query_vars['post_type'] ) && $query->query_vars['post_type'] == 'bookroom' ) ||
			( isset( $query->query_vars['event_type'] ) && $query->query_vars['event_type'] != '' )
		) {
			$query->set( 'orderby', 'meta_value' );
			$query->set( 'meta_key', 'data_inizio' );
			$query->set( 'order', 'DESC' );
		}
	}
	return $query;

}
add_action( 'pre_get_posts', 'elle_modify_query_order' );

function custom_search_post_type($query) {
	if ( !is_admin() && $query->is_main_query() ) {
		if ($query->is_search) {
			if(!isset($_GET["post_type"]) || ($_GET["post_type"] == "any"))
				$query->set('post_type', array("post", "product", "autore", "bookroom"));
			if($_GET["post_type"] == "corso"){
				$query->set('post_type', array("product"));
				$taxquery = array(
					array(
						'taxonomy' => 'product_type',
						'field' => 'slug',
						'terms' => array( "course" )
					)
				);
				$query->set( 'tax_query', $taxquery );
			}
			if($_GET["post_type"] == "libro"){
				$query->set('post_type', array("product"));
				$taxquery = array(
					array(
						'taxonomy' => 'product_type',
						'field' => 'slug',
						'terms' => array( "libro" )
					)
				);
				$query->set( 'tax_query', $taxquery );
			}
			if($_GET["post_type"] == "servizio"){
				$query->set('post_type', array("product"));
				$taxquery = array(
					array(
						'taxonomy' => 'product_type',
						'field' => 'slug',
						'terms' => array( "servizio", "pacchetto" )
					)
				);
				$query->set( 'tax_query', $taxquery );
			}

		}
	}
}

add_action('pre_get_posts','custom_search_post_type');


add_filter ( 'woocommerce_account_menu_items', 'wpsh_custom_endpoint', 40 );
function wpsh_custom_endpoint( $menu_links ){

	$menu_links = array_slice( $menu_links, 0, 1, true )
	              // Add your own slug (support, for example) and tab title here below
	              + array( 'my-courses' => 'I miei corsi' )
	              + array_slice( $menu_links, 1, NULL, true );

	$menu_links = array_slice( $menu_links, 0, 5, true )
	              // Add your own slug (support, for example) and tab title here below
	              + array( 'my-favourite' => 'I miei preferiti' )
                 + array_slice( $menu_links, 5, NULL, true );




	return $menu_links;

}
// Let’s register this new endpoint permalink

add_action( 'init', 'wpsh_new_endpoint' );
function wpsh_new_endpoint() {
	add_rewrite_endpoint( 'my-courses', EP_PAGES ); // Don’t forget to change the slug here
	add_rewrite_endpoint( 'my-favourite', EP_PAGES ); // Don’t forget to change the slug here

}

// Now let’s add some content inside your endpoint

add_action( 'woocommerce_account_my-courses_endpoint', 'wpsh_endpoint_content' ); // If you change your slug above then don’t foget to chagne it alse inside this function
function wpsh_endpoint_content() {

	// At the moment I will add Learndash profile with the shordcode

	/*
	 echo (
	'<h3>Here are your courses</h3>
	<p>Lorem ipsum dolor sit amet consectetur adipiscing elit facilisis tincidunt, nisi sociosqu lacinia auctor inceptos libero conubia accumsan</p>'
	);
	*/
	echo do_shortcode('[ld_profile]');
}



add_action( 'woocommerce_account_my-favourite_endpoint', 'favourite_endpoint_content' ); // If you change your slug above then don’t foget to chagne it alse inside this function
function favourite_endpoint_content() {

	// At the moment I will add Learndash profile with the shordcode
	global $current_user;

	?>
	 <section class="section pb-1 pb-md-2 pb-lg-3">
        <div class="container">
            <div class="row justify-content-center sticky-sidebar-container">
                <div class="col-lg-12">
	<?php
	 echo (
	'<h3>I tuoi preferiti</h3>	'
	);
     if(function_exists("get_user_favorites"))
    	$favourites = get_user_favorites($current_user->ID);
	if(count($favourites)){
?>
		<div class="block block-list block-list-three">
			<div class="row row-list list-services">
				<?php foreach ($favourites as $favourite){
					$post = get_post($favourite);
					?>
					<div class="col-lg-4 col-sm-6">
						<?php
						if($post->post_type == "product"){
							$product = wc_get_product( $post->ID );
							get_template_part("template-parts/card/card", $product->get_type(), $post);
						}else{
							get_template_part("template-parts/card/card", $post->post_type, $post);
						}
						?>
					</div>
				<?php } ?>
			</div><!-- /row -->
			<?php get_template_part("template-parts/archive/paginate"); ?>
		</div><!-- /block -->
<?php

	}else{
		?>
		<p>Non hai selezionato nessun preferito</p>
		<?php

	}

	?>
                </div>
            </div>
        </div>
	 </section>
	                <?php

}

// NB! In order to make it work you need to go to Settings > Permalinks and just push "Save Changes" button.

function elle_body_class( $wp_classes, $extra_classes )
{
	// List of the only WP generated classes allowed
//	$whitelist = array( 'home', 'blog', 'archive', 'single', 'category', 'tag', 'error404', 'logged-in', 'admin-bar' );

	// List of the only WP generated classes that are not allowed
	$blacklist = array( 'search');

	// Filter the body classes
	// Whitelist result: (comment if you want to blacklist classes)
//	$wp_classes = array_intersect( $wp_classes, $whitelist );
	// Blacklist result: (uncomment if you want to blacklist classes)
	 $wp_classes = array_diff( $wp_classes, $blacklist );

	// Add the extra classes back untouched
	return array_merge( $wp_classes, (array) $extra_classes );
}
add_filter( 'body_class', 'elle_body_class', 10, 2 );