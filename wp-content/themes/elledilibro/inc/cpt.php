<?php

function elledilibro_cpt_tax() {
	register_post_type( 'autore',
		array(
			'labels' => array(
				'name' => __( 'Autori' ),
				'singular_name' => __( 'Autore' ),
			),
			'public' => true,
			'menu_icon' => 'dashicons-groups',
			'has_archive' => true,
			'rewrite' => array('slug' => 'autore'),
			'supports' => array('title', 'editor', 'excerpt', 'page-attributes', 'thumbnail')
		)
	);

	register_taxonomy('genere',array('autore', 'product'), array(
		'hierarchical' => true,
		'labels' => array(
			'name' => __( 'Generi' ),
			'singular_name' => __( 'Genere' ),
		),
		'show_ui' => true,
		'show_in_rest' => true,
		'show_admin_column' => true,
		'query_var' => true,
		'rewrite' => array( 'slug' => 'genere' ),
	));

	register_post_type( 'bookroom',
		array(
			'labels' => array(
				'name' => __( 'BookRoom' ),
				'singular_name' => __( 'BookRoom' ),
			),
			'public' => true,
			'menu_icon' => 'dashicons-calendar-alt',
			'has_archive' => true,
			'show_in_rest' => true,
			'rewrite' => array('slug' => 'bookroom'),
			'supports' => array('title', 'editor', 'excerpt', 'page-attributes', 'thumbnail')
		)
	);

	register_taxonomy('event_type',array('bookroom'), array(
		'hierarchical' => true,
		'labels' => array(
			'name' => __( 'Tipo Evento' ),
			'singular_name' => __( 'Tipo Evento' ),
		),
		'show_ui' => true,
		'show_in_rest' => true,
		'show_admin_column' => true,
		'query_var' => true,
		'rewrite' => array( 'slug' => 'bookroom-type' ),
	));

}

add_action( 'init', 'elledilibro_cpt_tax' );
