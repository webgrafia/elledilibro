<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package elledilibro
 */

get_header();

get_template_part("template-parts/common/breadcrumb");
while ( have_posts() ) : the_post();

?>

	<section class="section section-main-title section-padding-bottom bg-gray-light">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-lg-7">
					<h1><?php the_title(); ?></h1>
					<span class="category">
						<?php
                        echo get_the_term_list($post->ID, "event_type", "",", ");
						?>
                    </span>
				</div><!-- /col-lg-7 -->
			</div><!-- /row -->
		</div><!-- /container -->
	</section><!-- /section -->


	<main role="main" class="article-wrapper">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-lg-7 sticky-sidebar-container">
					<article class="article-container">
						<div class="block block-content-image block-overlayed">
							<?php echo get_the_post_thumbnail( $post->ID, 'featured-post' ); ?>
                            <div class="content-details p-1 ps-lg-2">
                                <span class="date"><?php
                                    echo get_field("data_inizio");
                                    if($fine = get_field("data_fine")){
                                        echo "<br>".$fine;
                                    }
                                ?></span>
                                <?php
                                if($orario = get_field("orario"))
                                    echo '<span class="time">'.$orario.'</span>';

                                if($link_diretta = get_field("link_diretta"))
                                    echo '<a class="btn btn-purple" href="'.$link_diretta.'">Segui la diretta</a>';
                                ?>

                            </div><!-- /content-details -->
						</div><!-- /block -->
						<?php the_content(); ?>

						<?php
						if(is_user_logged_in()){
							?>
                            <hr/>
                            <div class="d-flex pt-2">
                            <span class="link-icon" href="#">
                                <div class="favourite">
                                    <?php the_favorites_button($args->ID); ?>
                                </div>
                            <span>Aggiungi ai preferiti</span>
                          </span>
                            </div>
							<?php
						}
						?>
					</article>
					<?php get_template_part("template-parts/common/share", "floating"); ?>
				</div><!-- /col-lg-7 -->
			</div><!-- /row -->
		</div><!-- /container -->
	</main><!-- /main -->





<?php get_template_part("template-parts/single/related", get_post_type(), $post); ?>

<?php get_template_part("template-parts/common/newsletter"); ?>

<?php
endwhile;

get_footer();
