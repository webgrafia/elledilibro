/**
 * The Laravel Mix configuration for the LearnDash plugin.
 *
 * @link https://laravel-mix.com
 */
const mix = require( 'laravel-mix' );
const webpack = require( 'webpack' );
const banner = 'This file is part of the LearnDash plugin and was generated automatically';

require( 'laravel-mix-eslint' );

// Customize Mix options.
mix.setPublicPath( process.env.MIX_BUILD_DIR || 'assets' )
	.options( {
		manifest: false,
		terser: {
			extractComments: false,
			terserOptions: {
				format: {
					comments: false,
					preamble: `/** ${banner} */`,
				},
			},
		},
		postCss: [ require( 'postcss-preset-env' ) ],

		// Disable CSS rewrites.
		processCssUrls: false,
	} )
	.sourceMaps( false );

// Customize the Webpack configuration.
mix.webpackConfig( {
	plugins: [ new webpack.BannerPlugin( banner ) ],
	externals: {
		'@wordpress/element': 'wp.element',
		'@wordpress/components': 'wp.components',
		'@wordpress/hooks': 'wp.hooks',
		'@wordpress/i18n': 'wp.i18n',
	},
	resolve: {
		alias: {

		},
	},
} );

// Bundle CSS.
mix.sass( 'assets/css/src/setup.scss', 'assets/css/setup.css' )
	.sass( 'assets/css/src/design-wizard.scss', 'assets/css/design-wizard.css' )
	.sass( 'assets/css/src/help.scss', 'assets/css/help.css' );

// Bundle JavaScript.
mix.js( 'assets/js/src/setup.js', 'assets/js/setup.js' )
	.js( 'assets/js/src/design-wizard.js', 'assets/js/design-wizard.js' )
	.js( 'assets/js/src/help.js', 'assets/js/help.js' )
	.eslint();
