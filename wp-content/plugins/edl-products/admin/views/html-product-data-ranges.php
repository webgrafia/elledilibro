<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}
?>
<div class="edl_product_ranges wc-metabox postbox closed <?php echo esc_attr( implode( ' ', $metabox_class ) ); ?>">
    <h3>
        <a href="#" class="remove_row delete"><?php esc_html_e( 'Remove', 'woocommerce' ); ?></a>
        <div class="handlediv" title="<?php esc_attr_e( 'Click to toggle', 'woocommerce' ); ?>"></div>
        <div class="tips sort" data-tip="<?php esc_attr_e( 'Drag and drop to set admin attribute order', 'woocommerce' ); ?>"></div>
        <strong class="edl_range_range">
            <?php echo  $range['name'] ?: ''; ?>
        </strong>
    </h3>
    <div class="edl_product_ranges_data wc-metabox-content hidden">
        <table cellpadding="0" cellspacing="0">
            <tbody>
            <tr>
                <td class="range_name">
                    <?php
                    woocommerce_wp_text_input(
                        array(
                            'id'    => 'range_name_' . esc_attr( $i ),
                            'class' => 'short range_name',
                            'value' => $range['name'],
                            'label' => 'Nome',
                            'desc_tip'  => true,
                            'description'   => 'Nome della fascia',
                            'name'  => 'range_name[' . esc_attr( $i ) . ']'
                        )
                    );
                    ?>
                </td>
                <td class="range_description">
                    <?php
                    woocommerce_wp_textarea_input(
                        array(
                            'id'    => 'range_description_' . esc_attr( $i ),
                            'class' => 'short range_description',
                            'value' => $range['description'],
                            'label' => 'Descrizione',
                            'desc_tip'  => true,
                            'description'   => 'Breve descrizione della fascia',
                            'name'  => 'range_description[' . esc_attr( $i ) . ']'
                        )
                    );
                    ?>
                </td>
                <td class="range_price">
                    <?php
                    woocommerce_wp_text_input(
                        array(
                            'id'    => 'range_price_' . esc_attr( $i ),
                            'value' => $range['price'],
                            'label' => 'Prezzo',
                            'type'   => 'wc_input_price',
                            'name'  => 'range_price[' . esc_attr( $i ) . ']'
                        )
                    );
                    ?>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
