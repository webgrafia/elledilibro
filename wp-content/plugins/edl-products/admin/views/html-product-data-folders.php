<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}
?>
<div class="edl_product_folders wc-metabox postbox closed <?php echo esc_attr( implode( ' ', $metabox_class ) ); ?>">
    <h3>
        <a href="#" class="remove_row delete"><?php esc_html_e( 'Remove', 'woocommerce' ); ?></a>
        <div class="handlediv" title="<?php esc_attr_e( 'Click to toggle', 'woocommerce' ); ?>"></div>
        <div class="tips sort" data-tip="<?php esc_attr_e( 'Drag and drop to set admin attribute order', 'woocommerce' ); ?>"></div>
        <strong class="edl_folder_range">
            <?php echo  $folder['from'] ? "Da " . $folder['from'] : ''; ?>
            <?php echo  $folder['to'] ? " A " . $folder['to'] : ''; ?>
        </strong>
    </h3>
    <div class="edl_product_folders_data wc-metabox-content hidden">
        <table cellpadding="0" cellspacing="0">
            <tbody>
            <tr>
                <td class="folder_from">
                    <?php
                    woocommerce_wp_text_input(
                        array(
                            'id'    => 'folder_from_' . esc_attr( $i ),
                            'class' => 'short folder_from',
                            'value' => $folder['from'],
                            'label' => 'Da',
                            'desc_tip'  => true,
                            'description'   => 'Dal numero di cartelle...',
                            'type'   => 'number',
                            'name'  => 'folder_from[' . esc_attr( $i ) . ']'
                        )
                    );
                    ?>
                </td>
                <td class="folder_to">
                    <?php
                    woocommerce_wp_text_input(
                        array(
                            'id'    => 'folder_to_' . esc_attr( $i ),
                            'class' => 'short folder_to',
                            'value' => $folder['to'],
                            'label' => 'A',
                            'desc_tip'  => true,
                            'description'   => 'al numero di cartelle...',
                            'type'   => 'number',
                            'name'  => 'folder_to[' . esc_attr( $i ) . ']'
                        )
                    );
                    ?>
                </td>
                <td class="folder_price">
                    <?php
                    woocommerce_wp_text_input(
                        array(
                            'id'    => 'folder_price_' . esc_attr( $i ),
                            'value' => $folder['price'],
                            'label' => 'Prezzo',
                            'type'   => 'wc_input_price',
                            'name'  => 'folder_price[' . esc_attr( $i ) . ']'
                        )
                    );
                    ?>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
