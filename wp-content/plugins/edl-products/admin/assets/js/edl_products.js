(function($) {
    // Blocco js carelle
    function folders_row_indexes() {
        $( '.product_folders .edl_product_folders' ).each( function( index, el ) {
            $( '.folder_position', el ).val( parseInt( $( el ).index( '.product_folders .edl_product_folders' ), 10 ) );
        });
    }

    $( '.product_folders' ).sortable({
        items: '.edl_product_folders',
        cursor: 'move',
        axis: 'y',
        handle: 'h3',
        scrollSensitivity: 40,
        forcePlaceholderSize: true,
        helper: 'clone',
        opacity: 0.65,
        placeholder: 'wc-metabox-sortable-placeholder',
        start: function( event, ui ) {
            ui.item.css( 'background-color', '#f6f6f6' );
        },
        stop: function( event, ui ) {
            ui.item.removeAttr( 'style' );
            folders_row_indexes();
        }
    });

    $("#folders_enabled").change(function() {
        if($(this).is(':checked')) {
            $('.add_folders_box').removeClass('hidden');
            $('.add_folders_input').addClass('hidden');
        } else {
            $('.add_folders_box').addClass('hidden');
            $('.add_folders_input').removeClass('hidden');
        }
    });

    $( '.product_folders' ).on( 'blur', 'input.folder_from', function() {
        $( this ).closest( '.edl_product_folders' ).find( 'strong.edl_folder_range' ).text( "Da " + $( this ).val() );
    });

    $( '.product_folders' ).on( 'blur', 'input.folder_to', function() {
        var closest = $( this ).closest( '.edl_product_folders' );
        closest.find( 'strong.edl_folder_range' ).text( "Da" + closest.find( 'input.folder_from' ).val() + " A " + $( this ).val() );
    });

    // Add rows.
    $( 'button.add_folders' ).on( 'click', function() {
        var size         = $( '.product_folders .edl_product_folders' ).length;
        var $wrapper     = $( this ).closest( '#product_folders' );
        var $folders  = $wrapper.find( '.product_folders' );
        var data         = {
            action:   'edl_products_add_element',
            type: 'folder',
            i: size,
            security: edl_products_admin_meta_boxes.nonce
        };

        $wrapper.block({
            message: null,
            overlayCSS: {
                background: '#fff',
                opacity: 0.6
            }
        });

        $.post( edl_products_admin_meta_boxes.ajax_url, data, function( response ) {
            $folders.append( response );

            folders_row_indexes();

            $folders.find( '.edl_product_folders' ).last().find( 'h3' ).trigger( 'click' );

            $('.woocommerce-help-tip').tipTip({
                'attribute': 'data-tip',
                'fadeIn':    50,
                'fadeOut':   50,
                'delay':     200
            });

            $wrapper.unblock();
        });

        return false;
    });

    $( '#product_folders' ).on( 'click', '.remove_row', function() {
        if ( window.confirm( edl_products_admin_meta_boxes.remove_folders ) ) {
            var $parent = $( this ).parent().parent();

            $parent.find( 'select, input[type=text], input[type=number]' ).val( '' );
            $parent.hide();
            folders_row_indexes();
        }
        return false;
    });

    $( '.save_folders' ).on( 'click', function() {
        $( '#product_folders' ).block({
            message: null,
            overlayCSS: {
                background: '#fff',
                opacity: 0.6
            }
        });
        var original_data = $( '#product_folders' ).find( 'input, select, textarea' );
        var data = {
            post_id     : woocommerce_admin_meta_boxes.post_id,
            product_type: $( '#product-type' ).val(),
            data        : original_data.serialize(),
            action      : 'edl_products_save_elements',
            type:   'folders',
            security    : edl_products_admin_meta_boxes.nonce
        };

        $.post( edl_products_admin_meta_boxes.ajax_url, data, function( response ) {
            if ( !response.success ) {
                // Error.
                window.alert( response.data.error );
            } else if ( response.data ) {
                // Success.
                $( '.product_folders' ).html( response.data.html );
            }
            $( '#product_folders' ).unblock();
        });
    });

    // Blocco js fasce
    function ranges_row_indexes() {
        $( '.product_ranges .edl_product_ranges' ).each( function( index, el ) {
            $( '.range_position', el ).val( parseInt( $( el ).index( '.product_ranges .edl_product_ranges' ), 10 ) );
        });
    }

    $( '.product_ranges' ).sortable({
        items: '.edl_product_ranges',
        cursor: 'move',
        axis: 'y',
        handle: 'h3',
        scrollSensitivity: 40,
        forcePlaceholderSize: true,
        helper: 'clone',
        opacity: 0.65,
        placeholder: 'wc-metabox-sortable-placeholder',
        start: function( event, ui ) {
            ui.item.css( 'background-color', '#f6f6f6' );
        },
        stop: function( event, ui ) {
            ui.item.removeAttr( 'style' );
            ranges_row_indexes();
        }
    });

    $( '.product_ranges' ).on( 'blur', 'input.range_name', function() {
        $( this ).closest( '.edl_product_ranges' ).find( 'strong.edl_range_range' ).text( $( this ).val() );
    });

    // Add rows.
    $( 'button.add_ranges' ).on( 'click', function() {
        var size         = $( '.product_ranges .edl_product_ranges' ).length;
        var $wrapper     = $( this ).closest( '#product_ranges' );
        var $ranges  = $wrapper.find( '.product_ranges' );
        var data         = {
            action:   'edl_products_add_element',
            type: 'range',
            i: size,
            security: edl_products_admin_meta_boxes.nonce
        };

        $wrapper.block({
            message: null,
            overlayCSS: {
                background: '#fff',
                opacity: 0.6
            }
        });

        $.post( edl_products_admin_meta_boxes.ajax_url, data, function( response ) {
            $ranges.append( response );

            ranges_row_indexes();

            $ranges.find( '.edl_product_ranges' ).last().find( 'h3' ).trigger( 'click' );

            $('.woocommerce-help-tip').tipTip({
                'attribute': 'data-tip',
                'fadeIn':    50,
                'fadeOut':   50,
                'delay':     200
            });

            $wrapper.unblock();
        });

        return false;
    });

    $( '#product_ranges' ).on( 'click', '.remove_row', function() {
        if ( window.confirm( edl_products_admin_meta_boxes.remove_ranges ) ) {
            var $parent = $( this ).parent().parent();

            $parent.find( 'select, input[type=text], input[type=number]' ).val( '' );
            $parent.hide();
            ranges_row_indexes();
        }
        return false;
    });

    $( '.save_ranges' ).on( 'click', function() {
        $( '.product_ranges' ).block({
            message: null,
            overlayCSS: {
                background: '#fff',
                opacity: 0.6
            }
        });
        var original_data = $( '#product_ranges' ).find( 'input, select, textarea' );
        var data = {
            post_id     : woocommerce_admin_meta_boxes.post_id,
            product_type: $( '#product-type' ).val(),
            data        : original_data.serialize(),
            action      : 'edl_products_save_elements',
            type:   'ranges',
            security    : edl_products_admin_meta_boxes.nonce
        };

        $.post( edl_products_admin_meta_boxes.ajax_url, data, function( response ) {
            if ( !response.success ) {
                // Error.
                window.alert( response.data.error );
            } else if ( response.data ) {
                // Success.
                $( '.product_ranges' ).html( response.data.html );
            }
            $( '.product_ranges' ).unblock();
        });
    });
    //
    // $( 'body').on('click', '.save_attributes', function(e) {
    //     if($( '#product-type' ).val() == 'libro') {
    //         $('.enable_variation').each(function() {
    //             $(this).css('display', 'block');
    //             console.log('ok');
    //             $(this).find('.checkbox').prop('checked', true);
    //             $(this).find('.checkbox').each(function() {
    //                 $(this).prop('checked', true);
    //             });
    //         });
    //     }
    // });
})(jQuery);