<?php
/**
 * Plugin Name: Elledilibro -  Prodotti Custom
 * Plugin URI: http://www.bititup.it
 * Description: Plugin che abilita i prodotti custom di Elledilibro (servizi e pacchetti)
 * Version: 0.3
 * Author: BititUp
 * License: GPL2
 */


if(!class_exists('EDL_Products')) {
    class EDL_Products {
        // class instance
        static $instance;

        public function __construct()
        {

            register_activation_hook( __FILE__, [ $this, 'install' ] );

            add_action( 'plugins_loaded', [ $this, 'action_plugins_loaded' ] );
            add_action( 'admin_enqueue_scripts', [ $this, 'edl_product_admin_enqueue_assets' ] );
            add_action( 'woocommerce_product_data_panels', [ $this, 'edl_cartelle_product_tab_content'] );
            add_action( 'woocommerce_product_data_panels', [ $this, 'edl_fasce_product_tab_content'] );
            add_action( 'woocommerce_single_product_summary', [&$this, 'edl_product_template'], 99 );

            add_action( 'wp_ajax_nopriv_edl_products_add_element', [&$this, 'edl_products_add_element'] );
            add_action( 'wp_ajax_edl_products_add_element', [&$this, 'edl_products_add_element'] );

            add_action( 'wp_ajax_nopriv_edl_products_save_elements', [&$this, 'edl_products_save_elements'] );
            add_action( 'wp_ajax_edl_products_save_elements', [&$this, 'edl_products_save_elements'] );

            add_action( 'wp_enqueue_scripts', [&$this, 'edl_product_frontend_enqueue_assets'] );

            add_filter( 'product_type_selector', [&$this, 'edl_products_type'], 99, 1 );
            add_filter( 'woocommerce_product_data_tabs', [&$this, 'edl_products_tab'], 99, 1 );

            add_filter( 'woocommerce_add_to_cart_validation', [&$this, 'edl_products_add_to_cart_validation'], 10, 3 );
            add_filter( 'woocommerce_add_cart_item_data', [&$this, 'edl_products_add_cart_item_data'], 5, 3 );
            add_filter( 'woocommerce_get_cart_item_from_session', [&$this, 'edl_products_get_cart_item_from_session'], 11, 3 );
            add_action( 'woocommerce_checkout_create_order_line_item', [&$this, 'edl_product_checkout_create_order_line_item'], 10, 4 );
            add_action( 'woocommerce_before_calculate_totals', [&$this, 'edl_product_add_custom_price'], 10 );
            add_filter( 'woocommerce_cart_item_price', [&$this, 'edl_product_cart_item_price'], 10, 2 );

            add_filter( 'woocommerce_get_item_data', [&$this, 'edl_products_get_item_data'], 11, 2 );
            add_filter( 'woocommerce_order_item_get_formatted_meta_data', [&$this, 'edl_products_order_item_get_formatted_meta_data'], 10, 3 );
            add_filter( 'woocommerce_order_get_downloadable_items', [&$this, 'edl_products_order_get_downloadable_items'], 10, 2 );

            add_filter( 'woocommerce_customer_available_downloads', [&$this, 'edl_customer_available_downloads'], 10, 2 );

            add_action( 'wp_ajax_woocommerce_save_attributes', [&$this, 'edl_ajax_woocommerce_save_attributes'], 0 );

            add_filter( 'upload_mimes', [&$this, 'allow_ebook_and_audiobook' ] );
            add_filter( 'wp_check_filetype_and_ext', [&$this, 'fix_mime_type_ebook_and_audiobook'], 75, 4 );
        }

        public function init() {
            if ( isset( $_GET['download_file'], $_GET['order'] ) && ( isset( $_GET['email'] ) || isset( $_GET['uid'] ) ) ) { // WPCS: input var ok, CSRF ok.
                add_action( 'init',  [&$this, 'edl_download_product'] );
            }
        }

        public function edl_download_product()
        {
            if($_GET['pty'] && $_GET['pty']=='edl') {
                $product_id = absint( $_GET['download_file'] ); // phpcs:ignore WordPress.VIP.SuperGlobalInputUsage.AccessDetected, WordPress.VIP.ValidatedSanitizedInput.InputNotValidated, WordPress.Security.ValidatedSanitizedInput.InputNotValidated
                $product    = wc_get_product( $product_id );
                $downloads  = $product ? $product->get_downloads() : array();

                $key = empty( $_GET['key'] ) ? '' : sanitize_text_field( wp_unslash( $_GET['key'] ) );

                if($downloads[$key]) {
                    if($fileurl = $product->get_file_download_path($key)) {
                        $filepath = str_replace(home_url(), ABSPATH, $fileurl);
                        if (file_exists($filepath)) {
                            header('Content-Description: File Transfer');
                            header('Content-Type: application/octet-stream');
                            header('Content-Disposition: attachment; filename="' . basename($filepath) . '"');
                            header('Expires: 0');
                            header('Cache-Control: must-revalidate');
                            header('Pragma: public');
                            header('Content-Length: ' . filesize($filepath));
                            flush(); // Flush system output buffer
                            readfile($filepath);
                            die();
                        } else {
                            die($filepath);
                            self::download_error( __( 'Invalid download link.', 'woocommerce' ) );
                        }
                    } else {
                        die('3');
                        self::download_error( __( 'Invalid download link.', 'woocommerce' ) );
                    }
                } else {
                    self::download_error( __( 'Invalid download link.', 'woocommerce' ) );
                }
            }
        }

        private static function download_error( $message, $title = '', $status = 404 ) {
            /*
             * Since we will now render a message instead of serving a download, we should unwind some of the previously set
             * headers.
             */
            if ( headers_sent() ) {
                wc_get_logger()->log( 'warning', __( 'Headers already sent when generating download error message.', 'woocommerce' ) );
            } else {
                header( 'Content-Type: ' . get_option( 'html_type' ) . '; charset=' . get_option( 'blog_charset' ) );
                header_remove( 'Content-Description;' );
                header_remove( 'Content-Disposition' );
                header_remove( 'Content-Transfer-Encoding' );
            }

            if ( ! strstr( $message, '<a ' ) ) {
                $message .= ' <a href="' . esc_url( wc_get_page_permalink( 'shop' ) ) . '" class="wc-forward">' . esc_html__( 'Go to shop', 'woocommerce' ) . '</a>';
            }
            wp_die( $message, $title, array( 'response' => $status ) ); // WPCS: XSS ok.
        }

        public function install() {
            // If there is no advanced product type taxonomy, add it.
            if ( ! get_term_by( 'slug', 'servizio', 'product_type' ) ) {
                wp_insert_term( 'servizio', 'product_type' );
            }
            if ( ! get_term_by( 'slug', 'pacchetto', 'product_type' ) ) {
                wp_insert_term( 'pacchetto', 'product_type' );
            }
        }

        public function allow_ebook_and_audiobook( $mimes ) {
            $mimes['epub']  = 'application/epub+zip';
            $mimes['mobi'] = 'application/x-mobipocket-ebook';

            return $mimes;
        }

        public function fix_mime_type_ebook_and_audiobook( $data = null, $file = null, $filename = null, $mimes = null ) {
            $ext = isset( $data['ext'] ) ? $data['ext'] : '';
            if ( strlen( $ext ) < 1 ) {
                $exploded = explode( '.', $filename );
                $ext      = strtolower( end( $exploded ) );
            }
            if ( 'svg' === $ext ) {
                $data['epub'] = 'application/epub+zip';
                $data['ext']  = 'epub';
            } elseif ( 'mobi' === $ext ) {
                $data['type'] = 'application/x-mobipocket-ebook';
                $data['ext']  = 'mobi';
            }

            return $data;
        }

        public function action_plugins_loaded()
        {
            require 'products/edl_product.php';
            require 'products/servizio.php';
            require 'products/pacchetto.php';
            require 'products/libro.php';
        }

        public function edl_products_type ( $type )
        {
            $type[ 'servizio' ] = __( 'Servizio' );
            $type[ 'pacchetto' ] = __( 'Pacchetto' );
            $type[ 'libro' ] = __( 'Libro' );
            return $type;
        }

        public function edl_products_tab( $tabs)
        {
            $tabs['cartelle'] = [
                'label'	 => __( 'Cartelle' ),
                'target' => 'product_folders',
                'class'  => [ 'show_if_servizio', 'show_if_pacchetto' ],
                'priority' => 5
            ];

            $tabs['fasce'] = [
                'label'	 => __( 'Fasce' ),
                'target' => 'product_ranges',
                'class'  => [ 'show_if_servizio', 'show_if_pacchetto'],
                'priority' => 6
            ];


            $tabs['shipping']['class'][] = 'hide_if_servizio';
            $tabs['shipping']['class'][] = 'hide_if_pacchetto';
            $tabs['attribute']['class'][] = 'hide_if_servizio';
            $tabs['attribute']['class'][] = 'hide_if_pacchetto';
            $tabs['variations']['class'][] = 'hide_if_servizio';
            $tabs['variations']['class'][] = 'hide_if_pacchetto';

            $tabs['inventory']['class'][] = 'show_if_libro';
            $tabs['variations']['class'][] = 'show_if_libro';

            return $tabs;
        }

        public function edl_cartelle_product_tab_content()
        {
            global $post, $thepostid, $product_object;

            $thepostid      = $post->ID;
            $product_object = $thepostid ? wc_get_product( $thepostid ) : new WC_Product();
            $is_edl_product_enabled = ($product_object->get_type() == 'servizio' || $product_object->get_type() == 'pacchetto') ? 1 : 0;
            $is_range_enabled = $is_edl_product_enabled ? $product_object->get_folders_enabled() : 0;
            ?>
            <div id="product_folders" class="panel">
                <div class="woocommerce_options_panel">
                    <div class="options_group">
                        <p class="form-field">
                            <?php
                            woocommerce_wp_checkbox( [
                                'id' 	=> 'folders_enabled',
                                'label' => 'Abilita range di cartelle',
                                'class' => 'edl_folder_range',
                                'value' => 'yes',
                                'cbvalue' => wc_bool_to_string($is_range_enabled),
                                'description' => 'in caso contrario l\'utente potrà inserirne liberamente la quantità'
                            ] );
                            ?>
                        </p>

                        <p class="form-field price_by_folders_field<?php echo $is_range_enabled ? ' hidden' : ''; ?> add_folders_input">
                            <label for="price_by_folders">Prezzo per cartella</label>
                            <span class="woocommerce-help-tip" data-tip="Una cartella corrisponde a 1800 battute. Se non si specifica un prezzo fisso questo verrà calcolato in base alle fasce."></span>
                            <input type="text" class="short wc_input_price" style="" name="price_by_folders" id="price_by_folders" value="<?php echo $product_object->get_price(); ?>" placeholder="">
                        </p>
                    </div>
                </div>

                <div class="wc-metaboxes-wrapper<?php echo $is_range_enabled ? '' : ' hidden'?> add_folders_box">
                    <div class="toolbar toolbar-top">
                        <div class="options_group<?php echo $is_range_enabled ? '' : ' hidden'?> add_folders_box">
                            <button class="button plus add_folders"><?php esc_html_e( 'Add new', 'woocommerce' ); ?></button>
                        </div>
                        <span class="expand-close">
                            <a href="#" class="expand_all">Espandi</a> / <a href="#" class="close_all">Chiudi</a>
                        </span>
                    </div>

                    <div class="product_folders wc-metaboxes ui-sortable">
                        <?php
                        if($is_edl_product_enabled) {
                            $folders = $product_object->get_folders( 'edit' );
                            $i  = -1;
                            if ( ! empty( $folders ) ) {
                                foreach ( $folders as $folder ) {
                                    $i++;
                                    $metabox_class = [];

                                    include __DIR__ . '/admin/views/html-product-data-folders.php';
                                }
                            }
                        }
                        ?>
                    </div>

                    <div class="toolbar">
                        <div class="options_group<?php echo $is_range_enabled ? '' : ' hidden'?> add_folders_box">
                            <button class="button plus add_folders"><?php esc_html_e( 'Add new', 'woocommerce' ); ?></button>
                        </div>
                        <span class="expand-close">
                            <a href="#" class="expand_all">Espandi</a> / <a href="#" class="close_all">Chiudi</a>
                        </span>
                    </div>
                </div>

               <div class="woocommerce_options_panel<?php echo $is_range_enabled ? '' : ' hidden'?> add_folders_box">
                    <div class="options_group">
                        <p class="form-field text-center">
                             <button type="button" class="button save_folders button-primary">Salva cartrelle</button>
                        </p>
                    </div>
               </div>
            </div>
            <?php
        }

        public function edl_fasce_product_tab_content()
        {
            global $post, $thepostid, $product_object;

            $thepostid      = $post->ID;
            $product_object = $thepostid ? wc_get_product( $thepostid ) : new WC_Product();
            $is_edl_product_enabled = ($product_object->get_type() == 'servizio' || $product_object->get_type() == 'pacchetto') ? 1 : 0;
            ?>
            <div id="product_ranges" class="panel wc-metaboxes-wrapper">
                <div class="toolbar toolbar-top">
                    <div class="text-center add_ranges_box">
                        <button class="button plus add_ranges"><?php esc_html_e( 'Add new', 'woocommerce' ); ?></button>
                    </div>
                    <span class="expand-close">
                        <a href="#" class="expand_all">Espandi</a> / <a href="#" class="close_all">Chiudi</a>
                    </span>
                </div>

                <div class="product_ranges wc-metaboxes ui-sortable">
                    <?php
                    if($is_edl_product_enabled) {
                        $ranges = $product_object->get_ranges( 'edit' );
                        $i  = -1;
                        if ( ! empty( $ranges ) ) {
                            foreach ( $ranges as $range ) {
                                $i++;
                                $metabox_class = [];

                                include __DIR__ . '/admin/views/html-product-data-ranges.php';
                            }
                        }
                    }
                    ?>
                </div>

                <div class="toolbar">
                    <div class="text-center add_ranges_box">
                        <button class="button plus add_ranges"><?php esc_html_e( 'Add new', 'woocommerce' ); ?></button>
                    </div>
                    <span class="expand-close">
                        <a href="#" class="expand_all">Espandi</a> / <a href="#" class="close_all">Chiudi</a>
                    </span>
                    <p class="form-field text-center">
                        <button type="button" class="button save_ranges button-primary">Salva fasce</button>
                    </p>
                </div>
            </div>
            <?php
        }

        public function edl_products_add_element()
        {
            ob_start();

            check_ajax_referer( 'edl_products_admin_ajax', 'security' );

            if ( ! current_user_can( 'edit_products' ) ) {
                wp_die( -1 );
            }

            $i = absint( $_POST['i'] );
            $metabox_class = [];
            switch ($_POST['type']) {
                case 'range':
                    $file = __DIR__ . '/admin/views/html-product-data-ranges.php';
                    $range['name'] = sanitize_text_field( wp_unslash( '' ) );
                    break;
                default:
                    $file = __DIR__ . '/admin/views/html-product-data-folders.php';
                    $folder['from'] = sanitize_text_field( wp_unslash( '' ) );
            }

            include $file;
            wp_die();
        }

        public function edl_products_save_elements()
        {
            check_ajax_referer( 'edl_products_admin_ajax', 'security' );

            if ( ! current_user_can( 'edit_products' ) || ! isset( $_POST['data'], $_POST['post_id'] ) ) {
                wp_die( -1 );
            }

            $response = [];

            try {
                parse_str( wp_unslash( $_POST['data'] ), $data ); // phpcs:ignore WordPress.Security.ValidatedSanitizedInput.InputNotSanitized

                $product_id   = absint( wp_unslash( $_POST['post_id'] ) );
                $product_type = ! empty( $_POST['product_type'] ) ? wc_clean( wp_unslash( $_POST['product_type'] ) ) : 'simple';
                $classname    = WC_Product_Factory::get_product_classname( $product_id, $product_type );
                $product      = new $classname( $product_id );

                switch ($_POST['type']) {
                    case 'ranges':
                        $elements = $product->set_ranges( $data );
                        break;
                    default:
                        $product->set_folders_enabled( $data );
                        $elements =$product->set_folders( $data );
                }

                $product->save();

                ob_start();
                $i  = -1;
                if ( ! empty( $elements ) ) {
                    foreach ( $elements as $element ) {
                        switch ($_POST['type']) {
                            case 'ranges':
                                $file = __DIR__ . '/admin/views/html-product-data-ranges.php';
                                $range = $element;
                                break;
                            default:
                                $file = __DIR__ . '/admin/views/html-product-data-folders.php';
                                $folder = $element;
                        }

                        $i++;
                        $metabox_class = [];

                        include $file;
                    }
                }

                $response['html'] = ob_get_clean();
            } catch ( Exception $e ) {
                wp_send_json_error( [ 'error' => $e->getMessage() ] );
            }

            wp_send_json_success( $response );
        }

        function edl_ajax_woocommerce_save_attributes() {
            check_ajax_referer( 'save-attributes', 'security' );
            parse_str( $_POST['data'], $data );

            if(isset($_POST['product_type']) && $_POST['product_type'] == 'libro') {
                $_POST['data'] .= '&attribute_variation%5B0%5D=1';
            }
        }

        public function edl_product_template()
        {
            global $product;
            $template_path = __DIR__ . '/frontend/views/';

            if ( 'servizio' == $product->get_type() || 'pacchetto' == $product->get_type() || 'libro' == $product->get_type() ) {
                $args = [
                    'product_type' => $product->get_type(),
                    'price' => $product->get_price(),
                    'id' => $product->get_id(),
                    'permalink' => $product->get_permalink()
                ];

                if('libro' == $product->get_type()) {
                    $args['available_variations'] = $product->edl_get_available_variations();
                } else {
                    $args['are_folders_enabled'] =  $product->get_folders_enabled() ? 1 : 0;
                    $args['folders'] = $product->get_folders();
                    $args['ranges'] = $product->get_ranges();
                }

                wc_get_template( 'html-edl-product.php', $args, '', trailingslashit( $template_path ) );
            }
        }

        public function edl_products_add_to_cart_validation($passed, $product_id, $request_quantity )
        {
            $product = wc_get_product( $product_id );

            if($passed) {
                if(is_a( $product, 'WC_Product' ) && ($product->get_type() == 'servizio' || $product->get_type() == 'paccheto')) {
                    if(!is_numeric(str_replace(",", ".", $_POST['folders']))) {
                        $passed = false;
                        wc_add_notice( __( 'Impossibile aggiungere il prodotto a carrello, specificare un numero di cartelle valido.' ), 'error' );
                    }

                    if(!is_numeric(str_replace(",", ".", $_POST['ranges']))) {
                        $passed = false;
                        wc_add_notice( __( 'Impossibile aggiungere il prodotto a carrello, selezionare una fascia valida.' ), 'error' );
                    }
                }
            }

            return $passed;
        }

        public function edl_products_add_cart_item_data( $cart_item_data, $product_id, $variation_id )
        {
            if( isset( $_REQUEST[ 'folders' ] ) ) {
                $cart_item_data['folders'] = $_REQUEST[ 'folders' ];
            }

            if( isset( $_REQUEST[ 'folders_label' ] ) ) {
                $cart_item_data['folders_label'] = $_REQUEST[ 'folders_label' ];
            }

            if( isset( $_REQUEST[ 'ranges' ] ) ) {
                $cart_item_data['ranges'] = $_REQUEST[ 'ranges' ];
            }

            if( isset( $_REQUEST[ 'ranges_label' ] ) ) {
                $cart_item_data['ranges_label'] = $_REQUEST[ 'ranges_label' ];
            }

            if( isset( $_REQUEST[ 'are_folders_enabled' ] ) ) {
                $cart_item_data['are_folders_enabled'] = $_REQUEST[ 'are_folders_enabled' ];
            }

            if( isset( $_REQUEST[ 'edl_variation_id' ] ) ) {
                $cart_item_data['edl_variation_id'] = $_REQUEST[ 'edl_variation_id' ];
            }

            return $cart_item_data;
        }

        public function edl_products_get_cart_item_from_session( $cart_item, $values, $key )
        {
            if ( isset( $values['folders'] ) ) {
                $cart_item['folders'] = $values['folders'];
            }

            if ( isset( $values['folders_label'] ) ) {
                $cart_item['folders_label'] = $values['folders_label'];
            }

            if ( isset( $values['ranges'] ) ) {
                $cart_item['ranges'] = $values['ranges'];
            }

            if ( isset( $values['ranges_label'] ) ) {
                $cart_item['ranges_label'] = $values['ranges_label'];
            }

            if ( isset( $values['are_folders_enabled'] ) ) {
                $cart_item['are_folders_enabled'] = $values['are_folders_enabled'];
            }

            if ( isset( $values['edl_variation_id'] ) ) {
                $cart_item['edl_variation_id'] = $values['edl_variation_id'];
            }

            return $cart_item;
        }

        public function edl_products_get_item_data( $item_data, $cart_item )
        {
            if(isset($cart_item['folders_label']))
            {
                $item_data[] = array(
                    'key'   => 'Cartelle',
                    'value' => $cart_item['folders_label'],
                );
            }

            if(isset($cart_item['ranges_label']))
            {
                if($cart_item['ranges_label']) {
                    $item_data[] = array(
                        'key'   => 'Fascia',
                        'value' => $cart_item['ranges_label']
                    );
                }
            }

            return $item_data;
        }

        public function edl_product_checkout_create_order_line_item( $item, $cart_item_key, $values, $order )
        {
            if ( isset( $values['folders'] ) )
            {
                $item->add_meta_data( 'folders', $values['folders'], true );
            }

            if ( isset( $values['folders_label'] ) )
            {
                $item->add_meta_data( 'folders_label', $values['folders_label'], true );
            }

            if ( isset( $values['ranges'] ) )
            {
                $item->add_meta_data( 'ranges', $values['ranges'], true );
            }

            if ( isset( $values['ranges_label'] ) )
            {
                $item->add_meta_data( 'ranges_label', $values['ranges_label'], true );
            }

            if ( isset( $values['are_folders_enabled'] ) )
            {
                $item->add_meta_data( 'are_folders_enabled', $values['are_folders_enabled'], true );
            }

            if ( isset( $values['edl_variation_id'] ) )
            {
                $item->add_meta_data( 'edl_variation_id', $values['edl_variation_id'], true );
            }
        }

        public function edl_product_add_custom_price($cart_object )
        {
            foreach ( $cart_object->cart_contents as $key => $value ) {
                $_product = $value['data'];
                if($variation = wc_get_product( $value['edl_variation_id'] )) {
                    if($variation->is_virtual()) {
                        $_product->set_virtual(1);
                    }
                }

                if(isset($value['folders']) && $value['folders']!='') $args['folders'] = $value['folders'];

                $args['iva'] = $value['data']->product_type == 'servizio' ? 1 : 0;
                if(isset($value['are_folders_enabled'])) {
                    $args['are_folders_enabled'] = $value['are_folders_enabled'];
                    if(isset($value['ranges']) && $value['ranges']!='') $args['ranges'] = $value['ranges'];
                    $value['data']->price = $_product->get_price('view', $args);
                } else {
                    $args['edl_variation_id'] = $value['edl_variation_id'] ?: 0;
                    $args['ranges'] = $_product->get_price();
                    $value['data']->price = $_product->get_price('view', $args);
                }
            }
        }

        function edl_product_cart_item_price( $price, $cart_item )
        {
            $args = [];
            if(isset($cart_item['folders']) && $cart_item['folders']!='') $args['folders'] = $cart_item['folders'];

            $args['iva'] = $cart_item['data']->product_type == 'servizio' ? 1 : 0;
            if(isset($cart_item['are_folders_enabled']) && $cart_item['are_folders_enabled']!='') {
                $args['are_folders_enabled'] = $cart_item['are_folders_enabled'];
                if(isset($cart_item['ranges']) && $cart_item['ranges']!='') $args['ranges'] = $cart_item['ranges'];
                $price = wc_price($cart_item['data']->get_price('view', $args));
            } elseif(!$cart_item['are_folders_enabled'] && $cart_item['folders']) {
                $args['edl_variation_id'] = $cart_item['edl_variation_id'] ?: 0;
                $args['ranges'] = $cart_item['data']->get_price();
                $price = wc_price($cart_item['data']->get_price('view', $args));
            }

            return $price;
        }

        public function edl_products_order_item_get_formatted_meta_data($formatted_meta)
        {
            foreach ($formatted_meta as $index => $meta) {
                switch ($meta->key) {
                    case 'folders':
                    case 'are_folders_enabled':
                    case 'ranges':
                        unset($formatted_meta[$index]);
                        break;
                    case 'folders_label':
                        $formatted_meta[$index]->display_key = "Cartelle";
                        break;
                    case 'ranges_label':
                        $formatted_meta[$index]->display_key = "Fascia";
                        break;
                }
            }
            return $formatted_meta;
        }

        public function edl_products_order_get_downloadable_items($downloads, $order)
        {

            foreach ( $order->get_items() as $item ) {
                if ( ! is_object( $item ) ) {
                    continue;
                }

                if ( $item->is_type( 'line_item' ) ) {
                    $email_hash         = function_exists( 'hash' ) ? hash( 'sha256', $order->get_billing_email() ) : sha1( $order->get_billing_email() );
                    $product        = $item->get_product();
                    if($product->get_type() == 'libro') {
                        $variation_id = $item->get_meta('edl_variation_id');
                        $variation = wc_get_product($variation_id);
                        if($variation->is_downloadable() && $order->is_download_permitted()) {
                            $dowloadable_files = get_post_meta($variation_id, '_downloadable_files', 1);
                            if($dowloadable_files) {
                                foreach ($dowloadable_files as $file) {
                                    $downloads[] = array(
                                        'download_url'        => add_query_arg(
                                            array(
                                                'download_file' => $variation_id,
                                                'order'         => $order->get_order_key(),
                                                'uid'           => $email_hash,
                                                'pty'           => 'edl',
                                                'key'           => $file['id'],
                                            ),
                                            trailingslashit( home_url() )
                                        ),
                                        'download_id'         => $file['id'],
                                        'product_id'          => $product->get_id(),
                                        'product_name'        => $product->get_name(),
                                        'product_url'         => $product->is_visible() ? $product->get_permalink() : '', // Since 3.3.0.
                                        'download_name'       => $file['name'],
                                        'order_id'            => $order->get_id(),
                                        'order_key'           => $order->get_order_key(),
                                        'downloads_remaining' => '',
                                        'access_expires'      => '',
                                        'file'                => array(
                                            'name' => $file['name'],
                                            'file' => $file['file'],
                                        ),
                                    );
                                }
                            }
                        }
                    }
                }
            }

            return $downloads;
        }

        public function edl_customer_available_downloads($downloads, $customer_id)
        {
            // Get results from valid orders only.
            $args = [
                'customer_id' => $customer_id,
                'limit' => -1, // to retrieve _all_ orders by this user
            ];
            $orders = wc_get_orders($args);

            foreach($orders as $order) {
                $downloads = $this->edl_products_order_get_downloadable_items($downloads, $order);
            }
            return $downloads;
        }

        public function edl_product_admin_enqueue_assets($hook)
        {
            global $post;

            if ('post.php' !== $hook && 'post-new.php' !== $hook)
                return;

            if ( 'product' != $post->post_type )
                return;

            $product = wc_get_product($post->ID);

            wp_register_style( 'edl_products_admin_css', plugin_dir_url(__FILE__) . '/admin/assets/css/edl_products.css', false, '0.1' );
            wp_enqueue_style( 'edl_products_admin_css' );
            wp_enqueue_script('edl_products_admin_js', plugin_dir_url(__FILE__) . '/admin/assets/js/edl_products.js', ['jquery'], '0.1', true);
            wp_localize_script( 'edl_products_admin_js', 'edl_products_admin_meta_boxes', [
                 'ajax_url' => admin_url( 'admin-ajax.php' ),
                'nonce' => wp_create_nonce('edl_products_admin_ajax'),
                'remove_folders' => 'Rimuovere il range di cartelle?',
                'product_type' => $product->get_type(),
                'post_id' => $post->ID ?? ''
            ]);
        }

        public function edl_product_frontend_enqueue_assets()
        {
            global $post;

            if ( 'product' != $post->post_type )
                return;

            $product = wc_get_product($post->ID);

            if ( 'servizio' == $product->get_type() || 'pacchetto' == $product->get_type() || 'libro' == $product->get_type() ) {
                wp_enqueue_script('edl_products_js', plugin_dir_url(__FILE__) . 'frontend/assets/js/edl_products.js', ['jquery'], '0.1', true);
            }
        }
    }
}

// Instantiate the class
if (class_exists("EDL_Products")) {
    $EDL_Products = new EDL_Products();
    $EDL_Products->init();
}