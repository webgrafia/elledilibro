<?php
if(!class_exists('WC_Product_Servizio')) {
    class WC_Product_Servizio extends WC_Product_Edl
    {
        public function __construct($product)
        {
            $this->product_type = 'servizio'; // name of your custom product type
            parent::__construct($product);
        }

        /**
         * Return the product type
         * @return string
       */
        public function get_type()
        {
            return 'servizio';
        }
    }
}