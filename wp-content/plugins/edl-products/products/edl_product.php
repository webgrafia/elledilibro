<?php
if(!class_exists('WC_Product_Edl')) {
    class WC_Product_Edl extends WC_Product
    {
        public function __construct($product)
        {
            parent::__construct($product);

            $this->data['folders'] = get_post_meta($this->id, '_edl_product_folders', true);
            $this->data['folders_enabled'] = get_post_meta($this->id, '_edl_product_folders_enabled', true) ? 1 : 0;
            $this->data['ranges'] = get_post_meta($this->id, '_edl_product_ranges', true);
        }

        /**
         * Set folders. Unlike the parent product which uses terms, variations and attributes
         * are specific attributes using from to and price.
         *
         * @param array $raw_folders array of raw afolders.
         */
        public function set_folders( $raw_folders )
        {
            $raw_folders = (array) $raw_folders;
            $folders     = array();

            foreach ( $raw_folders['folder_from'] as $key => $value ) {
                if(!is_numeric($value) || !$value || !is_numeric($raw_folders['folder_to'][ $key ]) || !$raw_folders['folder_to'][ $key ] || $raw_folders['folder_price'][ $key ] == '') continue;
                $folders[ $key ]['from'] = $value;
                $folders[ $key ]['to'] = $raw_folders['folder_to'][ $key ];
                $folders[ $key ]['price'] = $raw_folders['folder_price'][ $key ];
            }
            $this->set_prop( 'folders', $folders );

            return $folders;
        }

        public function set_folders_enabled( $raw_folders )
        {
            $raw_folders = (array) $raw_folders;
            $this->set_prop( 'folders_enabled', $raw_folders['folders_enabled'] ? 1 : 0 );
        }

        public function set_ranges( $raw_ranges )
        {
            $raw_ranges = (array) $raw_ranges;
            $ranges     = array();

            foreach ( $raw_ranges['range_name'] as $key => $value ) {
                if($value == '' || $raw_ranges['range_price'][ $key ] == '') continue;
                $ranges[ $key ]['name'] = $value;
                $ranges[ $key ]['description'] = $raw_ranges['range_description'][ $key ];
                $ranges[ $key ]['price'] = $raw_ranges['range_price'][ $key ];
            }
            $this->set_prop( 'ranges', $ranges );

            return $ranges;
        }

        /**
         * Returns product attributes.
         *
         * @param  string $context What the value is for. Valid values are view and edit.
         * @return array
         */
        public function get_folders( $context = 'view' )
        {
            return $this->get_prop( 'folders', $context );
        }

        public function get_folders_enabled( $context = 'view' )
        {
            return $this->get_prop( 'folders_enabled', $context );
        }

        public function get_ranges( $context = 'view' )
        {
            return $this->get_prop( 'ranges', $context );
        }

        public function save()
        {
            update_post_meta( $this->id, '_edl_product_ranges', $this->get_prop( 'ranges' ) );
            update_post_meta( $this->id, '_edl_product_folders', $this->get_prop( 'folders' ) );
            $folders_enabled = $this->get_prop( 'folders_enabled' );
            if(isset($_REQUEST['price_by_folders'])) {
                $folders_enabled = isset($_REQUEST['folders_enabled']) ? 1 : 0;
                update_post_meta( $this->id, '_price', $_REQUEST['price_by_folders'] ?: 0 );
            }
            update_post_meta( $this->id, '_edl_product_folders_enabled', $folders_enabled );
            parent::save();
        }

        public function has_file( $download_id = '' ) {
            $has_file = false;
            $variations = $this->edl_get_available_variations();

            foreach ($variations as $variation) {
                if($variation['downloadable']) {
                    $has_file = true;
                    break;
                }
            }

            return $has_file;
        }

        public function get_price( $context = 'view', $args = [] ) {
            $price = $this->price ?: $this->get_prop( 'price', $context );

            if ( 'servizio' == $this->get_type() || 'pacchetto' == $this->get_type()) {
                if(!empty($args)) {
                    if($args['are_folders_enabled'] == 1) {
                        $price = str_replace(",", ".", $args['folders']) + str_replace(",", ".", $args['ranges']);
                    } else {
                        $price = str_replace(",", ".", $args['folders']) * str_replace(",", ".", $args['ranges']);
                    }
                }
            } elseif('libro' == $this->get_type()) {
                if($args['edl_variation_id']) {
                    $variation = wc_get_product( $args['edl_variation_id'] );
                    $price = $variation->get_prop('price', $context);
                }
            }

            if($args['iva']) $price = $price+($price*22/100);

            return $price ? number_format((float) $price, 2, '.', '') : $this->price;
        }
    }
}