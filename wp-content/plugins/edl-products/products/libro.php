<?php
if(!class_exists('WC_Product_Libro')) {
    class WC_Product_Libro extends WC_Product_Edl
    {
        /**
         * Return the product type
         * @return string
         */
        public function get_type()
        {
            return 'libro';
        }

        public function edl_get_available_variations()
        {
            $revisions = [];
            $children = $this->edl_read_children();

            foreach ($children['visible'] as $child) {
                $variation = wc_get_product( $child );
                $revisions[$child] = [
                    'name' => get_post_meta($child, 'attribute_tipologia', true),
                    'price' => $variation->get_price(),
                    'regular_price' => $variation->get_regular_price(),
                    'virtual' => $variation->is_downloadable() || $variation->is_virtual(),
                    'downloadable' => $variation->is_downloadable()
                ] ;
            }

            return $revisions;
        }

        public function edl_read_children( $force_read = false ) {
            $children_transient_name = 'wc_product_children_' . $this->get_id();
            $children = get_transient( $children_transient_name );
            if ( false === $children ) {
                $children = array();
            }

            if ( empty( $children ) || ! is_array( $children ) || ! isset( $children['all'] ) || ! isset( $children['visible'] ) || $force_read ) {
                $all_args = array(
                    'post_parent' => $this->get_id(),
                    'post_type'   => 'product_variation',
                    'orderby'     => array(
                        'menu_order' => 'ASC',
                        'ID'         => 'ASC',
                    ),
                    'fields'      => 'ids',
                    'post_status' => array( 'publish', 'private' ),
                    'numberposts' => -1, // phpcs:ignore WordPress.VIP.PostsPerPage.posts_per_page_numberposts
                );

                $visible_only_args                = $all_args;
                $visible_only_args['post_status'] = 'publish';

                if ( 'yes' === get_option( 'woocommerce_hide_out_of_stock_items' ) ) {
                    $visible_only_args['tax_query'][] = array(
                        'taxonomy' => 'product_visibility',
                        'field'    => 'name',
                        'terms'    => 'outofstock',
                        'operator' => 'NOT IN',
                    );
                }
                $children['all']     = get_posts( apply_filters( 'woocommerce_variable_children_args', $all_args, $this, false ) );
                $children['visible'] = get_posts( apply_filters( 'woocommerce_variable_children_args', $visible_only_args, $this, true ) );

                set_transient( $children_transient_name, $children, DAY_IN_SECONDS * 30 );
            }

            $children['all']     = wp_parse_id_list( (array) $children['all'] );
            $children['visible'] = wp_parse_id_list( (array) $children['visible'] );

            return $children;
        }
    }
}