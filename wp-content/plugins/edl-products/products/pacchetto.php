<?php
if(!class_exists('WC_Product_Pacchetto')) {
    class WC_Product_Pacchetto extends WC_Product_Edl
    {
        public function __construct($product)
        {
            $this->product_type = 'pacchetto'; // name of your custom product type
            parent::__construct($product);
        }

        /**
         * Return the product type
         * @return string
         */
        public function get_type()
        {
            return 'pacchetto';
        }
    }
}