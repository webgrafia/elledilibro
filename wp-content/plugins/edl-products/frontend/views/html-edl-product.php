<?php
/**
 * Simple custom product
 */
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}
do_action( 'edl_product_add_to_cart_form' );  ?>

<form class="<?php echo $product_type; ?>_form cart" action="<?php echo esc_url( apply_filters( 'woocommerce_add_to_cart_form_action', $permalink ) ); ?>" method="post" enctype='multipart/form-data' data-product_id="<?php echo absint( $id ); ?>">
<?php if( 'libro' == $product_type ): ?>
    <div class="purchase-configurator">
        <div class="purchase-configurator-top">
            <div class="form-group configurator-format variations">
                <label class="form-label">Formato</label>
                <select id="tipologia" class="form-select form-select-lg attribute_tipologia" name="attribute_tipologia">
                    <option value="0">Scegli il formato</option>
                    <?php
                    global $product;

                    foreach ($available_variations as $variation => $value) :
                        ?><option value="<?php echo esc_attr($variation); ?>" data-price="<?php echo esc_attr($value['price']); ?>" data-regular_price="<?php echo esc_attr($value['regular_price']); ?>" data-virtual="<?php echo $value['virtual'] ? 1 : 0; ?>"><?php echo esc_attr($value['name']); ?></option><?php
                    endforeach;
                    ?>
                </select>
            </div><!-- /form-group -->
            <div class="form-group configurator-copies">
                <label class="form-label">Numero copie</label>
                <div class="plus-minus-group">
                    <span data-field="quantity" class="button-minus edl-quantity-action"></span>
                    <input
                            type="number"
                            id="quantity"
                            class="form-control form-control-lg quantity-field"
                            step="1"
                            min="<?php echo esc_attr( $product->get_min_purchase_quantity() ); ?>"
                            max="1000"
                            name="quantity"
                            value="<?php echo isset( $_POST['quantity'] ) ? wc_stock_amount( wp_unslash( $_POST['quantity'] ) ) : $product->get_min_purchase_quantity(); ?>"
                            title="<?php echo esc_attr_x( 'Qty', 'Product quantity input tooltip', 'woocommerce' ); ?>"
                            inputmode="number"
                            autocomplete="of"
                    />
                    <span data-field="quantity" class="button-plus edl-quantity-action"></span>
                </div><!-- /plus-minus-group -->
            </div><!-- /form-group -->
        </div><!-- /purchase-configurator-top -->
        <div class="purchase-configurator-bottom single_variation_wrap">
            <!--<div class="woocommerce-variation-description"></div>-->
            <div class="price-group">
                <label class="price-label">prezzo finale</label>
                <div class="price-input disabled price">
                    <span><?php echo get_woocommerce_currency_symbol(); ?></span>
                    <input type="text" class="form-control form-control-lg edl-amount" readonly="" value="0,00">
                </div><!-- /price-input -->
            </div><!-- /price-group -->

            <div class="woocommerce-variation single_variation hidden"></div>
            <button type="submit" class="single_add_to_cart_button alt disabled wc-variation-selection-needed btn btn-lg btn-outline-dark">Aggiungi al carrello</button>
            <input type="hidden" name="add-to-cart" value="<?php echo esc_attr($id); ?>">
            <input type="hidden" name="product_id" value="<?php echo esc_attr($id); ?>">
            <input type="hidden" name="edl_variation_id" class="edl_variation_id" value="0">
        </div><!-- /purchase-configurator-bottom -->
    </div><!-- /purchase-configurator -->
<?php else: ?>

    <?php
        $richiedi_contatto = get_field("richiedi_contatto");
        if(!$richiedi_contatto){ ?>
            <input type="hidden" id="are_folders_enabled" name="are_folders_enabled" value="<?php echo $are_folders_enabled; ?>">
            <input type="hidden" id="folders_label" name="folders_label" value="">
            <input type="hidden" id="ranges_label" name="ranges_label" value="">

            <div class="purchase-configurator pt-1">
                <div class="purchase-configurator-top">
                    <div class="form-group configurator-cartelle"<?php echo (!$are_folders_enabled &&  $price) ? ' style="width:100%; padding-right: 0;"' : ''; ?>>
                        <label class="form-label">Cartelle</label>
				        <?php if($are_folders_enabled) : ?>
                            <select class="form-select form-select-lg" aria-label="form-select" name="folders" id="folders">
                                <option value="0">Seleziona il numero di cartelle</option>
						        <?php foreach ($folders as $folder) : ?>
                                    <option value="<?php echo esc_attr($folder['price']); ?>"><?php echo $folder['from'] ? 'da ' . $folder['from'] : ''; ?><?php echo $folder['to'] ? ' A ' . $folder['to'] : ''; ?></option>
						        <?php endforeach; ?>
                            </select>
				        <?php else: ?>
                            <input type="number" name="folders" id="folders" placeholder="Numero di cartelle" min="1" class="form-control form-control-lg">
				        <?php endif; ?>
                    </div><!-- /form-group -->

			        <?php if(!$are_folders_enabled &&  $price): ?>
                        <input type="hidden" name="ranges" id="ranges" value="<?php echo esc_attr($price); ?>">
			        <?php else: ?>
                        <div class="form-group configurator-fasce">
                            <label class="form-label">Fasce</label>
                            <select class="form-select form-select-lg" aria-label="form-select" name="ranges" id="ranges">
                                <option value="0">Scegli la fascia</option>
						        <?php foreach ($ranges as $range) : ?>
                                    <option value="<?php echo esc_attr($range['price']); ?>" data-description="<?php echo esc_attr($range['description'] ?? ''); ?>"><?php echo $range['name']; ?></option>
						        <?php endforeach; ?>
                            </select>
                        </div><!-- /form-group -->
			        <?php endif; ?>

                </div><!-- /purchase-configurator-top -->

                <div class="purchase-configurator-bottom">
                    <div class="price-group">
                        <label class="price-label">prezzo finale</label>
                        <div class="price-input disabled">
                            <span><?php echo get_woocommerce_currency_symbol(); ?></span>
                            <input type="text" class="form-control form-control-lg edl-amount" readonly="" value="0,00">
                        </div><!-- /price-input -->
                    </div><!-- /price-group -->
                    <button type="submit" class="single_add_to_cart_button btn btn-lg btn-outline-dark disabled">Aggiungi al carrello</button>
                    <input type="hidden" name="add-to-cart" value="<?php echo esc_attr($id); ?>">
                    <input type="hidden" name="product_id" value="<?php echo esc_attr($id); ?>">
                </div><!-- /purchase-configurator-bottom -->
            </div>
                <?php
        }
    ?>


	<?php if(('pacchetto' == $product_type) &&  ($abilita_modale = get_field("abilita_modale_personalizzazione"))) : ?>
        <div class="customize-wrapper">
            <a class="trigger-customize" href="#modal02">
                <svg width="100%" height="100%" viewBox="0 0 32 32" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" xmlns:serif="http://www.serif.com/" style="fill-rule:evenodd;clip-rule:evenodd;stroke-linejoin:round;stroke-miterlimit:2;"><rect id="Rectangle-path" x="0" y="0.035" width="32" height="32" style="fill:none;"></rect><path d="M10.804,4.373l-2.261,1.302l-2.084,-0.315c-1.542,-0.228 -3.066,0.508 -3.847,1.857l-0.905,1.566c-0.78,1.356 -0.651,3.05 0.324,4.273c0,-0.001 1.31,1.641 1.31,1.641c-0,0 -0,2.606 -0,2.606c-0,-0 -1.31,1.642 -1.31,1.642c-0.975,1.222 -1.104,2.916 -0.324,4.272l0.946,1.581c0.783,1.339 2.302,2.069 3.838,1.842l2.081,-0.315c0,0 2.24,1.3 2.24,1.3l0.761,1.957c0.571,1.459 1.978,2.419 3.544,2.418c-0.001,-0 1.799,-0 1.799,-0c1.565,0.001 2.972,-0.959 3.543,-2.418l0.761,-1.957c-0,-0 2.24,-1.3 2.24,-1.3l2.081,0.315c1.542,0.228 3.066,-0.508 3.847,-1.857l0.905,-1.566c0.78,-1.356 0.651,-3.05 -0.324,-4.273c-0,0.001 -1.31,-1.641 -1.31,-1.641c0,-0 0,-2.606 0,-2.606c0,0 1.31,-1.642 1.31,-1.642c0.975,-1.222 1.104,-2.916 0.324,-4.272l-0.905,-1.566c-0.781,-1.349 -2.305,-2.085 -3.847,-1.857l-2.081,0.315c0,-0 -2.24,-1.3 -2.24,-1.3l-0.761,-1.957c-0.571,-1.459 -1.977,-2.419 -3.544,-2.418c0.001,0 -1.799,0 -1.799,0c-1.565,-0.008 -2.98,0.953 -3.552,2.418l-0.76,1.955Zm2.067,2.469c0.313,-0.181 0.555,-0.463 0.686,-0.8l0.96,-2.47c0.095,-0.243 0.33,-0.402 0.591,-0.401l1.809,0c0.26,0 0.494,0.159 0.588,0.401c0,0 0.96,2.47 0.96,2.47c0.131,0.336 0.371,0.617 0.683,0.798l3.203,1.857c0.312,0.181 0.676,0.25 1.032,0.197l2.622,-0.397c0.256,-0.037 0.508,0.085 0.638,0.308c-0,0 0.901,1.561 0.901,1.561c0.13,0.225 0.109,0.508 -0.054,0.711l-1.656,2.076c-0.224,0.281 -0.346,0.63 -0.346,0.989l-0,3.716c-0,0.359 0.122,0.708 0.346,0.989l1.656,2.076c0.163,0.203 0.184,0.486 0.054,0.712c0,-0 -0.901,1.56 -0.901,1.56c-0.13,0.223 -0.382,0.345 -0.638,0.308c0,-0 -2.622,-0.396 -2.622,-0.396c-0.356,-0.054 -0.72,0.015 -1.032,0.196l-3.203,1.858c-0.312,0.18 -0.552,0.461 -0.683,0.797l-0.96,2.47c-0.094,0.242 -0.328,0.401 -0.588,0.401l-1.802,-0c-0.26,0 -0.494,-0.159 -0.588,-0.401c-0,-0 -0.96,-2.47 -0.96,-2.47c-0.131,-0.336 -0.371,-0.617 -0.683,-0.797l-3.203,-1.858c-0.312,-0.181 -0.676,-0.25 -1.032,-0.196l-2.622,0.396c-0.256,0.037 -0.508,-0.085 -0.638,-0.308l-0.937,-1.568c-0.126,-0.224 -0.103,-0.503 0.058,-0.704l1.656,-2.076c0.224,-0.281 0.346,-0.63 0.346,-0.989l0,-3.716c0,-0.359 -0.122,-0.708 -0.346,-0.989l-1.656,-2.076c-0.163,-0.203 -0.184,-0.486 -0.054,-0.711c-0,-0 0.901,-1.561 0.901,-1.561c0.13,-0.223 0.382,-0.345 0.638,-0.308c-0,0 2.622,0.397 2.622,0.397c0.354,0.053 0.717,-0.015 1.028,-0.194l3.226,-1.858Zm-2.445,9.158c0,3.087 2.503,5.59 5.59,5.59c3.087,-0 5.59,-2.503 5.59,-5.59c-0,-3.087 -2.503,-5.59 -5.59,-5.59c-3.087,0 -5.59,2.503 -5.59,5.59Zm3.172,0c-0,-1.336 1.082,-2.418 2.418,-2.418c1.336,-0 2.418,1.082 2.418,2.418c0,1.336 -1.082,2.418 -2.418,2.418c-1.336,0 -2.418,-1.082 -2.418,-2.418Z"></path></svg>
                <span><?php the_field("titolo_modale_personalizzazione"); ?></span>
            </a>

            <div class="remodal" data-remodal-id="modal02">
                <button data-remodal-action="close" class="remodal-close" aria-label="Close">
                    <svg class="svg-close"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-close"></use></svg>
                </button>
                <div class="remodal-body">
                    <h3 class="text-center"><?php the_field("titolo_modale_personalizzazione"); ?></h3>
	                <?php the_field("testo_modale_personalizzazione"); ?>
                </div>
            </div>

        </div>
	<?php endif; ?>
<?php endif; ?>
</form>

<?php do_action( 'edl_product_after_add_to_cart_form' );