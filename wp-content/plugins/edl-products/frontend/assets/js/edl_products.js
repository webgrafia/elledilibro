(function($) {
    Number.prototype.format = function(n, x, s, c) {
        var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
            num = this.toFixed(Math.max(0, ~~n));

        return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
    };

    $('#folders').on('change', function(){
        let price = 0;
        let range = Number($('#ranges').val().replace(',', '.'));
        if($('#are_folders_enabled').val() == 1) {
            $('#folders_label').val($(this).children("option:selected").text());
            price = Number($(this).val().replace(',', '.')) + range;
        } else {
            $('#folders_label').val($(this).val());
            price = Number($(this).val().replace(',', '.')) * range;
        }

        $('.edl-amount').val(price.format(2, 3, '.', ','));
        if(price == 0) {
            $('.single_add_to_cart_button').addClass('disabled');
            $('.price-input').addClass('disabled');
        } else {
            $('.single_add_to_cart_button').removeClass('disabled');
            $('.price-input').removeClass('disabled');
        }
    });

    $('#ranges').on('change', function(){
        $('.woocommerce-variation').addClass('hidden');
        let price;
        let folder = Number($('#folders').val().replace(',', '.'));
        if($('#are_folders_enabled').val() == 1) {
            price = Number($(this).val().replace(',', '.')) + folder;
        } else {
            price = Number($(this).val().replace(',', '.')) * folder;
        }

        $('#ranges_label').val($(this).children("option:selected").text());

        if($(this).children("option:selected").data('description'))
            $('.edl-description').html($(this).children("option:selected").data('description'));

        $('.edl-amount').val(price.format(2, 3, '.', ','));
        if(price == 0) {
            $('.single_add_to_cart_button').addClass('disabled');
            $('.price-input').addClass('disabled');
        } else {
            $('.single_add_to_cart_button').removeClass('disabled');
            $('.price-input').removeClass('disabled');
        }
    });

    $('.attribute_tipologia').on('change', function() {
        let option = $(this).find(":selected");
        $('[name="edl_variation_id"]').val(option.val());
        let price = option.data('price');
        if(option.data('virtual') == 1) {
            $('#quantity').val(1);
            $('.configurator-copies').addClass('hidden');
        } else {
            $('.configurator-copies').removeClass('hidden');
        }

        $('.edl-amount').val((price*$('#quantity').val()).format(2, 3, '.', ','));
        let html = $.parseHTML($('.edl-product-price').find('bdi').html());
        $('.edl-product-price').find('bdi').html('<span class="' + $(html[0]).attr('class') + '">' + $(html[0]).text() + '</span>' + price.format(2, 3, '.', ','));

        if(option.val() == 0) {
            $('.single_add_to_cart_button').addClass('disabled');
            $('.price-input').addClass('disabled');
        } else {
            $('.single_add_to_cart_button').removeClass('disabled');
            $('.price-input').removeClass('disabled');
        }
    });

    $('.edl-quantity-action').each(function() {
        $(this).on('click', function () {
            let option = $('.attribute_tipologia').find(":selected");
            let price = option.data('price');
            let qty = $(this).hasClass('button-minus') ? Number($('#quantity').val())-1 : Number($('#quantity').val())+1;
            if(typeof price != 'undefined' && qty >= 0 ) {
                $('.edl-amount').val((price * qty).format(2, 3, '.', ','));
            }
        });
    });
})(jQuery);