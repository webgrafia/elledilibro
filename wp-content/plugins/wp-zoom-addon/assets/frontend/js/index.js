function websdkready() {
    var testTool = window.testTool;
    const meetingConfig = {};
    meetingConfig.name = testTool.b64EncodeUnicode(zvc_array.display_name);
    meetingConfig.mn = zvc_array.meeting_number;
    meetingConfig.pwd = zvc_array.meeting_pwd;
    meetingConfig.email = testTool.b64EncodeUnicode(zvc_array.user_email);
    meetingConfig.signature = zvc_array.signature;
    meetingConfig.apiKey = zvc_array.zoom_api_key;
    meetingConfig.is_webinar = zvc_array.is_webinar;
    meetingConfig.zvc_security = zvc_array.zvc_security;
    meetingConfig.lang = zvc_array.lang;
    meetingConfig.ld_bind = zvc_array.is_ld_bind;
    meetingConfig.thankyou_redirect = zvc_array.thankyou_redirect;
    meetingConfig.self_view = zvc_array.self_view;
    meetingConfig.join_web_target = zvc_array.join_web_target;
    
    const joinUrl =
        zvc_array.meeting_path +
        testTool.serialize(meetingConfig);
    var zoomIframe = testTool.createZoomNode("websdk-iframe", joinUrl);
        
   
    // Join via Web in a new window
    if( '_blank' === zvc_array.join_web_target ) {
        // If auto join then open in new window else form already opened in new window so join here 
        if( 1 == zvc_array.auto_join ) {
            var join_target = '_blank';
        } else {
            var join_target = '_self';
        }
        
        var win = window.open(joinUrl, join_target);
       
        if (win) {
            //Browser has allowed it to be opened
            win.focus();
        } else {
            //Browser has blocked it
            alert(zvc_array.new_tab_join_msg);
        }

        return;
    }

    var sandbox_attributes = zoomIframe.sandbox;
    sandbox_attributes += " allow-top-navigation";
    zoomIframe.sandbox = sandbox_attributes;
    //zoomIframe.setAttribute('scrolling', 'yes');

    const iframe_container = document.getElementsByClassName("zoom-iframe-container");
    Array.prototype.forEach.call(iframe_container, function (el) {
        el.appendChild(zoomIframe);
    });
    
    iFrameResize({
        //log: true,
        autoResize: true,
        warningTimeout: 10000,
        heightCalculationMethod: 'lowestElement',
        //minHeight: 500,
        //tolerance: 300,
        resizeFrom: 'child',
        scrolling: true,
        // maxHeight: 700,
        //widthCalculationMethod: 'taggedElement',
        // onResized: function (messageData) {
        //     //console.log(messageData)
        // },
    }, '#websdk-iframe');
}

websdkready();

// Scroll to Zoom window after page load
if (1 != zvc_array.no_scroll_window) {
    window.addEventListener("load", function () {
        const iframe_container = document.getElementsByClassName("zoom-iframe-container");
        if (iframe_container[0].length != 0) {
            iframe_container[0].scrollIntoView({ behavior: 'smooth', block: 'end' });
        }
    });
}

/*!
 * Sanitize and encode all HTML in a user-submitted string
 * (c) 2018 Chris Ferdinandi, MIT License, https://gomakethings.com
 * @param  {String} str  The user-submitted string
 * @return {String} str  The sanitized string
 */
var sanitizeHTML = function (str) {
    var temp = document.createElement('div');
    temp.textContent = str;
    return temp.innerHTML;
};