��    ^            �      �     �                    .     :     H  9   Z  >   �     �     �     �     �       
             )     .     E     V     _     g     v     |     �     �  
   �  "   �     �     �     �     �  
     U        m     z  
   �  	   �  !   �  #   �     �     �     	  
   	     	     /	     @	     Q	  &   a	     �	     �	     �	  -   �	     �	  	   �	     �	  �   
     �
     �
     �
     �
  +   �
     �
  
   �
     �
               &     /     H  
   Z  #   e  +   �  1   �  )   �                    #     9     L     R     j     {     �     �     �     �     �     �     �     �     �  �  �     f     n     }     �     �     �     �  ?   �  I        a     w     �     �     �  	   �  
   �     �     �     �  	                  '     -     @     P     o      t     �  #   �     �  !   �     �  j   �     i     �     �  	   �  %   �  (   �     �          ,     ?     N     ^     p     �  1   �  $   �  	   �       4        I     N     ^  �   l               ,     C  .   O     ~  	   �     �     �     �     �     �     �     �  6     <   I  0   �  1   �     �     �     �          !     8     @     `  !   u     �     �     �     �     �     �     �     �     �   API Key API Secret Key Action Action (Required). Add Meeting Add a Meeting Alternative Hosts Alternative hosts IDs. Multiple value separated by comma. An error occured. Host ID and Meeting ID not defined properly. Auto Create Auto Recording Back to List Back to Meeting Create Created On Cust Create Date Deleted %d Meeting(s). Deleted meeting. Duration ERROR:  Edit a Meeting Email Email Address Enforce Login Enter the date to check: First Name First Name of the User (Required). From Hide Join Early Host ID Host join start Join Early Join meeting before host start the meeting. Only for scheduled or recurring meetings. Join via App Last Client Last Login Last Name Last Name of the User (Required). Meeting Added To Zoom Successfully! Meeting Agenda Meeting Description. Meeting Host * Meeting ID Meeting Minutes Meeting Password Meeting Timezone Meeting Topic * Meeting duration (minutes). (optional) Meeting topic. (Required). Meetings Move to Trash Mutes Participants when entering the meeting. Name New Users Participants Password to join the meeting. Password may only contain the following characters: [a-z A-Z 0-9]. Max of 10 characters.( Leave blank for no Password ) Print Recording Date Register Reports Restricted access, please login to continue SN SSO Create Saved ! Select Recording Select bulk action Settings Start After Participants Start Date/Time * Start Time Start video when host join meeting. Start video when participants join meeting. Starting Date and Time of the Meeting (Required). This address is used for zoom (Required). Timezone To Topic Total Meeting Minutes Total Participants Trash Type of User (Required) Updated meeting. User Type (Required). Watch Recording day days hour hours minute minutes second seconds Project-Id-Version: 
POT-Creation-Date: 2020-10-12 15:41+0000
PO-Revision-Date: 2022-04-19 03:41+0000
Last-Translator: Adeel Raza
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-KeywordsList: __;_e;esc_html__;esc_html_e;esc_attr_e;esc_attr__;esc_attr_x;esc_html_x
X-Poedit-SearchPath-0: .
X-Poedit-SearchPath-1: includes
X-Poedit-SearchPath-2: includes/video-conferencing-with-zoom-shortcodes.php
Report-Msgid-Bugs-To: 
Language-Team: Portuguese (Brazil)
X-Loco-Version: 2.4.3; wp-5.5.1 API Key API Secret Key Açao Ação (Obrigatório). Adicionar Reunião Adicionar uma Reunião Hosts Alternativos ID Host alternativo. Múltiplos valores separados por vírgula. Um erro ocorreu. Id do Host e ID da reunião não definida adequadamente. Criar automaticamente Gravação Automática Retornar a Lista Voltar à reunião Criar Criado em Criar Cust Data %d Reunião(s) Cancelada(i) Reunião Deletada. Duração Erro Modificar uma reunião Email Endereço de Email Forçar o Login Entre uma data para verificar: Nome Nome do Usuário (Obrigatório). De Ocultar associação antes do tempo Host-ID Iniciar ao ingresso do anfitrião Junte-se antes do tempo Ingressar na reunião antes de o host iniciar a reunião. Somente para reuniões agendadas ou recorrentes. Participar usando o App do Zoom Ultimo Cliente Ultimo Login Sobrenome Sobrenome do Usuário (Obrigatório). Reunião adicionada ao Zoom com sucesso! Programa da Reunião Descrição da Reunião. Host da Reunião * ID da Reunião Reunão Minutos Senha da Reunião Fuso horário da reunião Assunto da Reunião * Duração da reunião (em minutos). (facultativo) Assunto da Reunião. (Obrigatório). Reuniões Mover para Lixeira Silenciar Participantes quando entrarem na reunião. Nome Novos Usuários Participantes Senha para ingressar na reunião. A senha pode conter apenas os seguintes caracteres: [a-z A-Z 0-9]. Máximo de 10 caracteres (deixe em branco para não ter senha) Imprimir Data de Gravação Inscreva-se no Webinar Relatórios Acesso limitado, efetue o login para continuar SN Criar SSO Salvo ! Selecione Gravação Selecionar ação em massa Ajustes Iniciar Após Participantes Data/Hora de Início * Horário de Início Iniciar vídeo quando o anfitrião entrar na reunião. Iniciar vídeo quando os participantes ingressam na reunião Data e ora di inizio del Meeting (Obbligatorio). Este endereço é usado pelo zoom (Obrigatório). Fuso Horário Para Tópico Total de minutos da reunião Total de participantes Lixeira Tipo de Usuário (Obrigatório) Reunião atualizada. Tipo de Usuário (Obrigratório). Assistir Gravação dia dias hora horas minuto minutos segundo segundos 