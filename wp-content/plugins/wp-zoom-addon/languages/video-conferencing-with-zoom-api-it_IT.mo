��    a      $      ,      ,     -     5     D     K     ^     j     x  9   �  >   �                    +     ;  
   B     M     R     i     z     �     �     �     �     �     �  
   �  "   �                          0  
   A  U   L     �     �  
   �  	   �  !   �  #   �     	     %	     :	  
   I	     T	     d	     u	     �	  &   �	     �	     �	     �	     �	  -   
     :
  	   ?
     I
  �   V
     �
     �
          
  +        >  
   A     L     T     e     x     �     �  
   �  #   �  +   �  1        9     I  )   i     �     �     �     �     �     �     �     �     �          #     '     ,     1     7     >     F     M  �  U     �     �     �     �          "     6  9   G  Q   �     �     �     �          !  	   &     0     5     Q     e     l     s     �     �     �  !   �     �      �     �  "   �          "     @     Z  f   t     �     �            #     %   ?     e     {     �  
   �     �     �     �     �  -     &   9     `     i  .   |  D   �     �     �       �        �     �     �     �  2   �     0     3  	   <     F     ^     }     �     �     �  5   �  @     0   B     s  4   �  7   �     �       	             *     >     F     d     x     �     �     �     �     �     �     �     �     �   API Key API Secret Key Action Action (Required). Add Meeting Add a Meeting Alternative Hosts Alternative hosts IDs. Multiple value separated by comma. An error occured. Host ID and Meeting ID not defined properly. Auto Create Auto Recording Back to List Back to Meeting Create Created On Date Deleted %d Meeting(s). Deleted meeting. Duration ERROR:  Edit a Meeting Email Email Address Enforce Login Enter the date to check: First Name First Name of the User (Required). From Hide Join Early Host ID Host join start Join Before Host Join Early Join meeting before host start the meeting. Only for scheduled or recurring meetings. Join via App Last Client Last Login Last Name Last Name of the User (Required). Meeting Added To Zoom Successfully! Meeting Agenda Meeting Description. Meeting Host * Meeting ID Meeting Minutes Meeting Password Meeting Timezone Meeting Topic * Meeting duration (minutes). (optional) Meeting topic. (Required). Meetings Move to Trash Mute Participants upon entry Mutes Participants when entering the meeting. Name New Users Participants Password to join the meeting. Password may only contain the following characters: [a-z A-Z 0-9]. Max of 10 characters.( Leave blank for no Password ) Print Recording Date Register Reports Restricted access, please login to continue SN SSO Create Saved ! Select Recording Select bulk action Settings Start After Participants Start Date/Time * Start Time Start video when host join meeting. Start video when participants join meeting. Starting Date and Time of the Meeting (Required). Sync Zoom Users Thankyou Message on Meeting End This address is used for zoom (Required). Timezone To Topic Total Meeting Minutes Total Participants Trash Type of User (Required) Updated meeting. User Type (Required). Watch Recording day days hour hours minute minutes second seconds Project-Id-Version: 
POT-Creation-Date: 2020-10-12 15:41+0000
PO-Revision-Date: 2022-04-19 03:42+0000
Last-Translator: Adeel Raza
Language-Team: Italian
Language: it_IT
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-KeywordsList: __;_e;esc_html__;esc_html_e;esc_attr_e;esc_attr__;esc_attr_x;esc_html_x
X-Poedit-SearchPath-0: .
X-Poedit-SearchPath-1: includes
X-Poedit-SearchPath-2: includes/video-conferencing-with-zoom-shortcodes.php
Report-Msgid-Bugs-To: 
X-Loco-Version: 2.4.3; wp-5.5.1 API Key API Secret Key Azione Azione (Obbligatorio). Aggiungi Meeting Aggiungi un Meeting Host Alternativi ID Host alternativi. Valori multipli separati da virgole. Si è verificato un errore. ID Host e ID Meeting non sono definiti correttamente. Crea in automatico Registrazione Automatica Torna a Lista Torna alla riunione Crea Creato il Data %d Meeting(s) Cancellato(i) Meeting cancellato. Durata Errore Modifica un Meeting Email Indirizzo Email Forza il Login Inserisci la data da controllare: Nome Nome dell'Utente (Obbligatorio). Da Nascondi Iscriviti prima del tempo Host-ID Inizio all'ingresso dell'Host Partecipa Prima dell'Host Partecipa prima del tempo Partecipa alla riunione prima che l'Host inizi il meeting. Solo per riunioni programmate o ricorrenti. Partecipa usando l'App Zoom Ultimo Cliente Ultimo Login Cognome Cognome dell'Utente (Obbligatorio). Meeting Aggiunto a Zoom con successo! Programma del Meeting Descrizione del Meeting. Host del Meeting * Meeting ID Minuti del Meeting Password del Meeting Fuso Orario del Meeting Argomento del Meeting  * Durata del Meeting (in minuti). (facoltativo) Argomento del Meeting. (Obbligatorio). Meetings Sposta nel Cestino Disattiva l'audio dei partecipanti all'entrata Disattiva l'audio dei partecipanti quando partecipano alla riunione. Nome Nuovi Utenti Partecipanti Password per partecipare alla riunione. La password può contenere solo i seguenti caratteri: [a-z A-Z 0-9]. Massimo 10 caratteri (Lascia vuoto se non vuoi inserire una password) Stampa Data di registrazione Registrati al webinar Reports Accesso limitato, effettua il login per continuare SN Crea SSO Salvato ! Seleziona Registrazione Seleziona una azione in blocco Impostazioni Inizia Dopo i Partecipanti Data/Ora di inizio * Orario di Inizio Avvia il video quando l'host partecipa alla riunione. Inizia il video quando i partecipanti si uniscono alla riunione. Data e ora di inizio del Meeting (Obbligatorio). Sincronizza utenti zoom Messaggio di ringraziamento alla fine della riunione Questo indirizzo è utilizzato per zoom (Obbligatorio). Fuso Orario A Argomento Minuti Totali del Meeting Partecipanti Totali Cestino Tipo di Utente (Obbligatorio) Meeting aggiornato. Tipo di Utente (Obbligatorio). Guarda la registrazione Giorno Giorni Ora Ore Minuto Minuti Secondo Secondi 