��    a      $      ,      ,     -     5     D     K     ^     j     x  9   �  >   �                    +     ;  
   B     M     Y     ^     u     �     �     �     �     �     �     �  
   �  "   �               "     *     :  U   K     �     �     �  
   �  	   �  !   �  #   �     "	     1	     F	  
   U	     `	     p	     �	     �	  &   �	     �	     �	     �	     �	  -   
     F
  	   K
     U
  �   b
     �
     �
            +        J  
   M     X     `     q     �     �     �  
   �  #   �  +   �  1        E  )   U          �     �     �     �     �     �     �     �     �                         #     *     2     9  �  A     �     �     �     �               +  9   =  S   w     �     �     �     �  	             %     <     B     \     q     w     ~     �     �     �  $   �     �  %   �       *        >     F     a  j   x     �                %     6  &   ?  0   f     �     �     �  
   �  	   �     �     �       +   '  $   S     x     �  '   �  9   �     �     �  
   	  �        �     �  &   �     �  9        =     @     N     \     q     �     �     �  	   �  1   �  9     1   ?  !   q  5   �     �     �     �     �     �  
             .     F     b     s     w     |     �     �     �     �     �   API Key API Secret Key Action Action (Required). Add Meeting Add a Meeting Alternative Hosts Alternative hosts IDs. Multiple value separated by comma. An error occured. Host ID and Meeting ID not defined properly. Auto Create Auto Recording Back to List Back to Meeting Create Created On Cust Create Date Deleted %d Meeting(s). Deleted meeting. Duration ERROR:  Edit a Meeting Email Email Address Enforce Login Enter the date to check: First Name First Name of the User (Required). From Hide Register Host ID Host join start Join Before Host Join meeting before host start the meeting. Only for scheduled or recurring meetings. Join via App Join via Web Last Client Last Login Last Name Last Name of the User (Required). Meeting Added To Zoom Successfully! Meeting Agenda Meeting Description. Meeting Host * Meeting ID Meeting Minutes Meeting Password Meeting Timezone Meeting Topic * Meeting duration (minutes). (optional) Meeting topic. (Required). Meetings Move to Trash Mute Participants upon entry Mutes Participants when entering the meeting. Name New Users Participants Password to join the meeting. Password may only contain the following characters: [a-z A-Z 0-9]. Max of 10 characters.( Leave blank for no Password ) Print Recording Date Register Reports Restricted access, please login to continue SN SSO Create Saved ! Select Recording Select bulk action Settings Start After Participants Start Date/Time * Start Time Start video when host join meeting. Start video when participants join meeting. Starting Date and Time of the Meeting (Required). Sync Zoom Users This address is used for zoom (Required). Timezone To Topic Total Meeting Minutes Total Participants Trash Type of User (Required) Updated meeting. User Type (Required). Watch Recording day days hour hours minute minutes second seconds Project-Id-Version: 
POT-Creation-Date: 2020-10-12 15:41+0000
PO-Revision-Date: 2022-04-19 03:41+0000
Last-Translator: Adeel Raza
Language-Team: German
Language: de_DE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-KeywordsList: __;_e;esc_html__;esc_html_e;esc_attr_e;esc_attr__;esc_attr_x;esc_html_x
X-Poedit-SearchPath-0: .
X-Poedit-SearchPath-1: includes
X-Poedit-SearchPath-2: includes/video-conferencing-with-zoom-shortcodes.php
Report-Msgid-Bugs-To: 
X-Loco-Version: 2.4.3; wp-5.5.1 API-Schlüssel API Secret Key Aktion Aktion (erforderlich). Meeting hinzufügen Meeting hinzufügen Alternative Hosts Alternative Hosts IDs. Mehrfachwert durch Komma getrennt. Es ist ein Fehler aufgetreten. Host-ID und Meeting-ID sind nicht korrekt definiert. Auto-Erstellung Auto-Aufnahme Zurück zur Liste Zurück zum Meeting Erstellen Erstellt am Aufbewahrung erstellen Datum Gelöschte %d Meeting(s). Gelöschtes Meeting. Dauer Fehler Ein Meeting bearbeiten Email E-Mail-Adresse: Login erwzwingen Gib das zu überprüfende Datum ein: Vorname Vorname des Benutzers (erforderlich). Von Register für Meeting / Webinar ausblenden Host-ID Beitreten des Host startet Vor dem Host beitreten Nimm an einem Meeting teil, bevor du das Meeting startest. Nur für geplante oder wiederkehrende Meetings. Beitreten über die Zoom-App Beitritt über das Web Letzter Kunde Letzte Anmeldung Nachname Nachname des Benutzers (erforderlich). Meeting wurde erfolgreich bei Zoom hinzugefügt! Tagesordnung Meeting Beschreibung. Leiter (Host) des Meetings Meeting-ID Protokoll Meeting Passwort Zeitzone des Meetings Meeting Überschrift* Dauer des Meetings (in Minuten). (optional) Meeting Überschrift (erforderlich). Termine In Papierkorb legen Teilnehmer beim Eintreten stummschalten Teilnehmer stummschalten, wenn sie dem Meeting beitreten. Name Neue Benutzer Teilnehmer Passwort für die Teilnahme an der Besprechung. Das Passwort darf nur die folgenden Zeichen enthalten: (a-z A-Z 0-9). Max. 10 Zeichen (( Leer lassen für kein Passwort) Drucken Aufnahmedatum Registrieren Sie sich für das Webinar Berichte Eingeschränkter Zugang, bitte anmelden, um fortzufahren. SN SSO erstellen Gespeichert ! Wählen Sie Aufnahme Mehrfachaktion auswählen Einstellungen Start nach den Teilnehmern Start Datum / Zeit * Startzeit Video starten, wenn der Host das Meeting betritt. Video starten, wenn die Teilnehmer am Meeting teilnehmen. Start Datum und Zeit des Meetings (erforderlich). Synchronisieren Sie Zoom-Benutzer Diese Adresse wird fü Zoom verwendet (erforderlich). Zeitzone Bis Thema Meeting Minuten gesamt Teilnehmer insgesamt Papierkorb Benutzertyp (erforderlich) Aktualisiertes Meeting. Benutzertyp (erforderlich). Aufnahme ansehen Tag Tage Stunde Stunden Minute Minuten Sekunde Sekunden 