��    {      �      �      �     �     �     �     �     �     
          &  9   8  >   r     �     �     �     �     �  
   �     �     	     	     #	     4	     =	     L	     R	     `	     n	  
   �	  "   �	     �	  0   �	     �	     �	     
     
  	   
     $
     4
  
   E
  	   P
  U   Z
     �
     �
     �
  
   �
  	   �
  !   �
  
          #        B     Q     b     t     �     �  
   �     �     �     �     �     �       &         G     X     v     �     �     �  -   �     �  	   �       5   !     W     d  �   x               #     ,  +   4     `  
   c     n     v     �     �     �     �     �     �  
   �  #   �  +   "  1   N     �  %   �  )   �     �     �     �     �               )     /     G  	   X     b     x     �  
   �  
   �     �     �     �     �     �     �     �     �  �  �  	   j     t     �     �     �     �     �     �  ?   �  V   5  
   �     �     �     �     �  	   �     �     �     �       	   $     .     A  !   U     w     �     �     �     �  =   �       *   2     ]     o     �     �     �      �     �  s   �     n     �     �     �     �  !   �     �     �  &        ,     B     X     n  	   �     �     �     �  !   �     �  	             :  .   P          �  !   �  	   �     �  !   �  ;        Q     X  :   f  @   �     �     �  �   �     �     �  !   �     �  3   �     .     1     P  %   \     �     �     �     �     �     �       ;   $  @   `  5   �     �  *   �  .        N     [     ]     b     |     �     �     �     �     �     �     	               (     4     9     ?     D     J     Q     Y     a   API Key API Secret Key Action Action (Required). Add Meeting Add Zoom User Add a Meeting Alternative Hosts Alternative hosts IDs. Multiple value separated by comma. An error occured. Host ID and Meeting ID not defined properly. Auto Create Auto Recording Back to List Back to Meeting Create Created On Cust Create Date Deleted %d Meeting(s). Deleted meeting. Duration Edit a Meeting Email Email Address Enforce Login Enter the date to check: First Name First Name of the User (Required). From Get past meetings report for a specified period. Hide Join Early Hide Join via App Host Host ID Host Name Host join start Join Before Host Join Early Join Time Join meeting before host start the meeting. Only for scheduled or recurring meetings. Join via App Join via Web Last Client Last Login Last Name Last Name of the User (Required). Leave Time Login Meeting Added To Zoom Successfully! Meeting Agenda Meeting Auto End Meeting Auto Join Meeting Description. Meeting Ended. Meeting Host * Meeting ID Meeting Minutes Meeting Participants Report Meeting Password Meeting Reports Meeting Timezone Meeting Topic * Meeting duration (minutes). (optional) Meeting password Meeting password is incorrect Meeting topic. (Required). Meetings Move to Trash Mute Participants upon entry Mutes Participants when entering the meeting. Name New Users Not redirected after countdown Only signed-in WordPress users can join this meeting. Participants Participants Report Password to join the meeting. Password may only contain the following characters: [a-z A-Z 0-9]. Max of 10 characters.( Leave blank for no Password ) Print Recording Date Register Reports Restricted access, please login to continue SN SSO Create Saved ! Select Date to Check...  Select Host Select Recording Select bulk action Settings Start After Participants Start Date/Time * Start Time Start video when host join meeting. Start video when participants join meeting. Starting Date and Time of the Meeting (Required). Sync Zoom Users Thank you for attending this meeting! This address is used for zoom (Required). Timezone To Topic Total Meeting Minutes Total Minutes Total Participants Trash Type of User (Required) Updated meeting. User Name User Type (Required). View Report Watch Recording Zoom Users click here day days hour hours minute minutes second seconds Project-Id-Version: 
POT-Creation-Date: 2020-10-12 15:41+0000
PO-Revision-Date: 2022-04-19 03:41+0000
Last-Translator: Adeel
Language-Team: Spanish (Argentina)
Language: es_AR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-KeywordsList: __;_e;esc_html__;esc_html_e;esc_attr_e;esc_attr__;esc_attr_x;esc_html_x
X-Poedit-SearchPath-0: .
X-Poedit-SearchPath-1: includes
X-Poedit-SearchPath-2: includes/video-conferencing-with-zoom-shortcodes.php
Report-Msgid-Bugs-To: 
X-Loco-Version: 2.4.4; wp-5.5.3 Clave API Clave API Secreta Acción Acción (Requerido). Agregar Reunión Agregar un Usuario Agregar una Reunión Anfitriones Alternativos ID de los anfitriones alternativos. Valores separados por coma. Un error ocurrió. La ID del Anfitrión y de la Reunión no se definieron propiamente. Auto Crear Auto Grabado Volver a la Lista Volver a la reunión Crear Creada El Crear Personalizado Fecha Reunion(es) %d Eliminadas. Reunión eliminada. Duración Editar la Reunión Correo Electrónico Dirección de Correo Electrónico Obligar Iniciar Sesión Agregar la fecha a revisar: Nombre Nombre del Usuario (Requerido). De Conseguir un reporte de la cuenta de un período específico. Ocultar unirse antes de tiempo Ocultar unirse a través de la aplicación ID del Anfitrión ID del Anfitrión Apellido Iniciar al unirse el anfitrión Unirse Antes que el Anfitrión Únete antes de la hora indicada Última Sesión Unirse a la reunión antes que el anfitrión inicie la reunión. Solo para las reuniones programadas o recurrentes. Únete con la App de Zoom Unirse vía Web Último Cliente Última Sesión Apellido Apellido del Usuario (Requerido). Horario de Inicio Última Sesión ¡Reunión Agregada a Zoom con Éxito! Agenda de la Reunión Agenda de la Reunión Agenda de la Reunión Descripción de la Reunión. Reuniones Anfitrión de la Reunión * ID de la Reunión Minutos de Reunión Silenciar Participantes al entrar Contraseña de la Reunión Reuniones Zona Horaria de la Reunión Tema de la Reunión * Duración de la reunión (minutos). (opcional) Contraseña de la Reunión Contraseña de la Reunión Tema de la reunión. (Requerido). Reuniones Mover a la Basura Silenciar Participantes al entrar Silenciar a los Participantes cuando entrar a una reunión. Nombre Nueva Usuario Si no eres redireccionado a la reunión a la hora acordada Solo usuarios con sesión abierta pueden unirse a esta reunión. Participantes Participantes Contraseña para unirse a la reunión. La contraseña solo puede contener los siguientes caracteres: [a-z A-Z 0-9]. Máximo 10 caracteres. (Dejar vacío para no poner Contraseña) Imprimir Fecha de grabación Regístrate en este seminario web Reportes Acceso restringido, por favor accede para continuar SN Crear Inicio de Sesión Único ¡Guardado! Seleccionar una Fecha para Revisar... Seleccionar un Anfitrión Seleccionar grabación Seleccionar acción masiva Configuración Comenzar Con Participantes Fecha/Horario de Inicio * Horario de Inicio Empezar el video cuando el anfitrión se une a la reunión. Empezar el video cuando los participantes se unen a la reunión. Fecha y Horario de Inicio de la Reunión (Requerido). Sincronizar usuarios de Zoom Muchas gracias por asistir a esta reunión Esta dirección es usada por zoom (Requerido). Zona Horaria A Tema Total Minutos de Reunión Total Minutos de Reunión Total Participantes Basura Tipo de Usuario (Requerido) Reunión actualizada. Apellido Tipo de Usuario (Requerido). Reportes Auto Grabado Usuarios pulsa aquí día días hora horas minuto minutos segundo segundos 