��    u      �      l      l     m     u     �     �     �     �     �  9   �  >        C     O     ^     k     {  
   �     �     �     �     �     �     �     �     �     �      	  
   	  "   $	     G	  0   L	     }	     �	     �	     �	  	   �	     �	     �	  
   �	  	   �	  U   �	     B
     O
     \
  
   h
  	   s
  !   }
  
   �
     �
  #   �
     �
     �
     �
       
        !     1     M     ^     o  &        �     �     �     �     �       -   $     R  	   W     a  5   �     �     �  �   �     m     s     �     �  +   �     �  
   �     �     �     �     �               '     @  
   R  #   ]  +   �  1   �     �     �  )        9     B     E     K     a     t     z     �  	   �     �     �     �  
   �     �     �     �     �     �                 �       �     �  
   �     �                2  ]   L  X   �               +     A     W     ^     g  
   s     ~     �     �     �     �     �     �           0  +   >     j  5   m  )   �  -   �     �            /   /  "   _     �  "   �     �  -   B      p     �     �     �  -   �            -   /     ]     v     �     �     �     �  $   �               5  /   K     {     �     �     �     �  $   �  I        U     Z  0   t  O   �     �       �        �     �     	  
   #  <   .     k  
   n  	   y  !   �     �     �      �     �  -   �      #     D  I   V  Q   �  5   �     (  .   G  =   v     �     �     �     �     �               /     F     V  
   v     �     �     �     �     �     �     �     �     �  
   �   API Key API Secret Key Action Action (Required). Add Meeting Add a Meeting Alternative Hosts Alternative hosts IDs. Multiple value separated by comma. An error occured. Host ID and Meeting ID not defined properly. Auto Create Auto Recording Back to List Back to Meeting Create Created On Cust Create Date Deleted %d Meeting(s). Deleted meeting. Duration Edit a Meeting Email Email Address Enforce Login Enter the date to check: First Name First Name of the User (Required). From Get past meetings report for a specified period. Hide Join Early Hide Join via App Host Host ID Host Name Host join start Join Before Host Join Early Join Time Join meeting before host start the meeting. Only for scheduled or recurring meetings. Join via App Join via Web Last Client Last Login Last Name Last Name of the User (Required). Leave Time Login Meeting Added To Zoom Successfully! Meeting Agenda Meeting Description. Meeting Ended. Meeting Host * Meeting ID Meeting Minutes Meeting Participants Report Meeting Password Meeting Timezone Meeting Topic * Meeting duration (minutes). (optional) Meeting password Meeting password is incorrect Meeting topic. (Required). Meetings Move to Trash Mute Participants upon entry Mutes Participants when entering the meeting. Name New Users Not redirected after countdown Only signed-in WordPress users can join this meeting. Participants Participants Report Password to join the meeting. Password may only contain the following characters: [a-z A-Z 0-9]. Max of 10 characters.( Leave blank for no Password ) Print Recording Date Register Reports Restricted access, please login to continue SN SSO Create Saved ! Select Date to Check...  Select Host Select Recording Select bulk action Settings Start After Participants Start Date/Time * Start Time Start video when host join meeting. Start video when participants join meeting. Starting Date and Time of the Meeting (Required). Sync Zoom Users Thankyou Message on Meeting End This address is used for zoom (Required). Timezone To Topic Total Meeting Minutes Total Participants Trash Type of User (Required) Updated meeting. User Name User Type (Required). View Report Watch Recording click here day days hour hours minute minutes second seconds Project-Id-Version: 
POT-Creation-Date: 2020-10-12 15:41+0000
PO-Revision-Date: 2022-04-19 03:41+0000
Last-Translator: Adeel Raza
Language-Team: Hebrew
Language: he_IL
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/
X-Poedit-Basepath: ..
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n==2 ? 1 : n>10 && n%10==0 ? 2 : 3);
X-Poedit-KeywordsList: __;_e;esc_html__;esc_html_e;esc_attr_e;esc_attr__;esc_attr_x;esc_html_x
X-Poedit-SearchPath-0: .
X-Poedit-SearchPath-1: includes
X-Poedit-SearchPath-2: includes/video-conferencing-with-zoom-shortcodes.php
Report-Msgid-Bugs-To: 
X-Loco-Version: 2.4.3; wp-5.5.1 מפתח API מפתח API סודי פעולה פעולה (חובה). הוסף מפגש הוסף מפגש מארחים נוספים מזהים של מארחים נוספים. ערכים מרובים מופרדים בפסיק. קרתה שגיאה. מזהה מארח ומזהה מפגש לא הוגדרו כראוי. Auto Create הקלטה אוטומטית חזרה לרשימה חזרה לפגישה Create נוצר Cust Create תאריך נמחק(ו) %d מפגש(ים) המפגש נמחק. משך המפגש ערוך מפגש אימייל כתובת מייל אכוף התחברות הזן תאריך לבדיקה:  שם פרטי שם פרטי של המשתמש (חובה). מ קבל דוח חשבון לתקופה ספציפית. הסתר הצטרפות לפני הזמן הצטרפות דרך אפליקציית Zoom מזהה מארח מזהה מארח שם משפחה הפעלת וידאו בהצטרפות מארח הצטרפות למני המארח הצטרף לפני הזמן זמן התחברות אחרונה הצטרפות לפני שהמארח מתחיל את המפגש. רק עבור מפגשים מתוזמנים או חוזרים. הצטרפות דרך אפליקציית Zoom הצטרפות דרך דפדפן דפדפן אחרון התחברות אחרונה שם משפחה שם משפחה של המשתמש (חובה). זמן התחלה התחברות אחרונה המפגש התווסף ל-Zoom בהצלחה! סדר יום למפגש תיאור המפגש. מפגשים שנערכו מארח מפגש * מזהה מפגש דקות מפגש השתק משתתפים בכניסה סיסמת המפגש אזור זמן למפגש נושא המפגש * משך המפגש (בדקות). (לא חובה) סיסמת הפגישה סיסמת המפגש נושא המפגש (חובה). מפגשים העבר לפח השתק משתתפים בכניסה להשתיק את המשתתפים כאשר הם נכנסים למפגש. שם משתמשים חדשים לא מופנה בתום הספירה לאחור רק משתמשים מחוברים יכולים להצטרף למפגש הזה. משתתפים משתתפים סיסמת התחברות למפגש. הסיסמה יכולה להכיל רק תווים אלפא-נומריים (a-z A-Z 0-9)/  מקסימום 10 תווים. (השאר ריק כדי לא להשתמש בסיסמה) הדפס תאריך הקלטה הירשם לוובינר דוחות גישה מוגבלת, אנא התחבר כדי להמשיך SN SSO Create נשמר! בחר תאריך לבדיקה... בחר מארח בחר הקלטה בחר פעולה קבוצתית הגדרות הפעלת וידאו אחרי משתתפים תאריך ושעת התחלה * זמן התחלה להפעיל את הוידאו כאשר המארח מצטרף למפגש. להפעיל את הוידאו כאשר משתתפים מצטרפים למפגש. תאריך ושעת התחלה למפגש (חובה). סנכרן משתמשי זום הודעת תודה על סיום הפגישה כתובת זו היא בשימוש עבור Zoom (חובה). אזור זמן עד נושא סך הכל דקות מפגש סך הכל משתתפים פח סוג המשתמש (חובה) המפגש עודכן. שם משפחה סוג המשתמש (חובה). דוחות הקלטה אוטומטית לחץ כאן יום ימים שעה שעות דקה דקות שניה שניות 