<form target="<?php echo esc_attr( $args['join_web_target'] ); ?>" action="<?php echo esc_attr( get_permalink() ); ?>" autocomplete="off" method="POST" class="meeting-form zoom-wp-form">
	<?php wp_nonce_field( 'meeting_nonce', 'meeting_nonce_field' ); ?>
	<div class="zoom-meeting-field">
		<div>
			<input autocomplete="nope" type="<?php echo ( isset( $args['zoom_map_array']['ld_lessons_bind'] ) && $args['display_name'] ? 'hidden' : 'text' ); ?>"
				name="display_name" id="display_name" value="<?php echo esc_attr( $args['display_name'] ); ?>"
				placeholder="<?php echo esc_html( $args['attendee_name_placeholder'] ); ?>" class="input form-control" required />
		</div>
	</div>
	<div class="zoom-meeting-field">
		<input autocomplete="no-email" type = "<?php echo ( ! is_user_logged_in() && ( $args['is_webinar'] || 2 != $args['zoom_map_array']['approval_type'] ) ? 'email' : 'hidden' ); ?>"
		name="meeting_email" id="meeting_email" value="<?php echo esc_attr( $args['user_email'] ); ?>" maxLength="254" placeholder="<?php echo __( 'Email', 'video-conferencing-with-zoom-api' ); ?>"
		class="form-control" <?php echo ( ! is_user_logged_in() && ( $args['is_webinar'] || 2 != $args['zoom_map_array']['approval_type'] ) ? 'required' : '' ); ?>/>
	</div>
	<div class="zoom-meeting-field">
		<span class="password-error"><?php echo esc_html( isset( $error_msg['password'] ) ? $error_msg['password'] : '' ); ?></span>
		<div class="zoom-meeting-field">
			<input autocomplete="new-password" <?php echo $is_password ? 'required' : ''; ?> type="<?php echo ( $is_password ? 'password' : 'hidden' ); ?>"
			name="meeting_pwd" id="meeting_pwd" value="" placeholder="<?php echo __( 'Meeting password', 'video-conferencing-with-zoom-api' ); ?>" class="form-control <?php echo ( isset( $error_msg['password'] ) ? 'error ' : '' ); ?>" />
		</div>
	</div>
	<?php
	if ( $args['zoom_lang_select'] ) :
		include( 'tpl-zoom-lang.php' );
	endif;
	?>
	<div class="join-btn-block">
		<button type="submit" id="join_iframe" name="join_iframe" class="zoom-link <?php echo esc_attr( $args['zoom_btn_css_class'] ); ?>">
			<?php echo ( $args['zoom_join_via_web_text'] ? __( esc_html( $args['zoom_join_via_web_text'] ), 'video-conferencing-with-zoom-api' ) : __( 'Join via Web', 'video-conferencing-with-zoom-api' ) ); ?>
		</button>
		<?php echo $args['join_via_app']; ?>
	</div>
</form>
