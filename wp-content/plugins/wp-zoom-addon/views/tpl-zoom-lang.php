<div class="zoom-meeting-field">
   <div>
      <select name="meeting_lang" class="input form-control">
         <option value="en-US">English</option>
         <option value="de-DE">German Deutsch</option>
         <option value="es-ES">Spanish Español</option>
         <option value="fr-FR">French Français</option>
         <option value="jp-JP">Japanese 日本語</option>
         <option value="pt-PT">Portuguese Portuguese</option>
         <option value="ru-RU">Russian Русский</option>
         <option value="zh-CN">Chinese 简体中文</option>
         <option value="zh-TW">Chinese 繁体中文</option>
         <option value="ko-KO">Korean 한국어</option>
         <option value="vi-VN">Vietnamese Tiếng Việt</option>
         <option value="it-IT">Italian italiano</option>
      </select>
   </div>
</div>