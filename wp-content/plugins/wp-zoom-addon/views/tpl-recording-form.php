<?php if ( ! isset( $_POST['play_recording'] ) ) : ?>
<div class="recording-display-block">
	<h3><?php echo __( 'View All Recordings', 'video-conferencing-with-zoom-api' ); ?></h3>
<?php endif; ?>
	<form class="zoom-wp-form" method="POST">
		<?php wp_nonce_field( 'cloud_recording_verify_nonce', 'cloud_recording_nonce' ); ?>
		<?php echo $back_to_meeting_btn; ?>
		<button onclick="event.preventDefault();" type="submit" name="select_recording" value="1" data-toggle="modal" 
			data-target="#recordingModal<?php echo esc_attr( $args['type_id'] ); ?>"
			class="play-recoding-btn zoom-link <?php echo esc_attr( $args['zoom_btn_css_class'] ); ?>">
			<span class="dashicons dashicons-video-alt2"></span>
			<?php echo __( 'Open Recordings', 'video-conferencing-with-zoom-api' ); ?>
		</button>
	</form>
<?php if ( ! isset( $_POST['play_recording'] ) ) : ?>
</div>
<?php endif; ?>
<div class="modal fade recordingModal" id="recordingModal<?php echo esc_attr( $args['type_id'] ); ?>" tabindex="-1" role="dialog" aria-labelledby="Zoom Cloud Recordings" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
		<div class="modal-header">
			<h5 class="modal-title"><?php echo __( 'Select Recording', 'video-conferencing-with-zoom-api' ); ?></h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
			</button>
		</div>
		<div class="modal-body">
			<table class="display recordings-list" style="width:100%"></table>
		</div>
		<div class="modal-footer">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
			</button>
		</div>
		</div>
	</div>
</div>
