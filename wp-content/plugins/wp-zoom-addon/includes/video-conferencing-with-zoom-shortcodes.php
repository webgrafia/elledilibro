<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
/**
 * Shortcodes
 *
 * @author   Adeel
 * @modified 3.2.2
 * @since    2.0.0
 */

// Add Shared Array buffer origin trial to meta
add_action( 'wp_head', 'video_conferencing_zoom_meta_tags', 0 );
function video_conferencing_zoom_meta_tags() {
	$zoom_sharedarraybuffer_token = esc_attr( get_option( 'zoom_sharedarraybuffer_token' ) );
	?>
	<meta http-equiv="origin-trial" content="<?php echo $zoom_sharedarraybuffer_token; ?>">
	<?php
}

//Register shortcode and scripts
add_action( 'init', 'video_conferencing_zoom_register_shortcodes' );
add_action( 'init', 'video_conferencing_zoom_render_meeting_window', 0 );

// Shortcode added from main post content
add_action( 'template_redirect', 'video_conferencing_zoom_redirect_cache_on_content', 0 );

// Shortcode added from widgets
add_filter( 'widget_text', 'video_conferencing_zoom_redirect_cache_on_widget', 0 );

function video_conferencing_zoom_register_shortcodes() {
	if ( ! video_conferencing_zoom_is_valid_request() ) {
		return;
	}

	$zoom_css_dep = array( 'dashicons-zoom-plugin' );

	if ( ! wp_style_is( 'dashicons-zoom-plugin' ) ) {
		wp_register_style( 'dashicons-zoom-plugin', get_site_url() . '/wp-includes/css/dashicons.css', array(), ZVCW_ZOOM_PLUGIN_VER );
		wp_enqueue_style( 'dashicons-zoom-plugin' );
	}

	wp_register_style( 'video-conferencing-with-zoom-api-iframe', ZOOM_VIDEO_CONFERENCE_PLUGIN_FRONTEND_CSS_PATH . '/zoom-iframe.css', $zoom_css_dep, ZVCW_ZOOM_PLUGIN_VER );

	add_shortcode( 'zoom_api_link', 'video_conferencing_zoom_render_shortcode' );
	add_shortcode( 'zoom_latest_recording', 'video_conferencing_zoom_show_latest_recording' );
	add_shortcode( 'zoom_recording', 'video_conferencing_zoom_show_latest_recording' );
}

// Shortcode added from main post content
function video_conferencing_zoom_redirect_cache_on_content() {
	if ( ! video_conferencing_zoom_is_valid_request() ) {
		return;
	}

	// No redirect to no cache is needed on admin pages & non single pages
	if ( video_conferencing_zoom_is_admin_request() || ! is_singular() ) {
		return;
	}

	global $post;
	$content = ( isset( $post->post_content ) ? $post->post_content : false );

	video_conferencing_zoom_redirect_to_nocache( $content );
}

// Shortcode added from widgets
function video_conferencing_zoom_redirect_cache_on_widget( $content ) {
	if ( ! video_conferencing_zoom_is_valid_request() ) {
		return;
	}

	video_conferencing_zoom_redirect_to_nocache( $content );

	return $content;
}

/**
 * Rendering Meeting Display Shortcode Output
 * @return string
 */
function video_conferencing_zoom_render_shortcode( $atts, $content = null ) {
	if ( ! video_conferencing_zoom_is_valid_request() ) {
		return;
	}
	ob_start();

	// If styles fail to enqueue earlier then add them with the shortcode
	if ( ! wp_style_is( 'video-conferencing-with-zoom-api-iframe' ) ) {
		wp_enqueue_style( 'video-conferencing-with-zoom-api-iframe' );

		if ( is_rtl() ) {
			wp_add_inline_style( 'video-conferencing-with-zoom-api-iframe', 'body ul.zoom-meeting-countdown{ direction: ltr; }' );
		}
	}

	$args = shortcode_atts(
		zoom_wp_get_meeting_shortcode_array(),
		$atts
	);

	$args['type_id'] = (float) str_replace( '-', '', str_replace( ' ', '', $args['meeting_id'] ) );

	if ( ! $args['type_id'] ) {
		$content = '<h4 class="no-meeting-id"><strong style="color:red;">' . __( 'ERROR: ', 'video-conferencing-with-zoom-api' ) . '</strong>' . __( 'Missing meeting or webinar id', 'video-conferencing-with-zoom-api' ) . '</h4>';
		return;
	}

	$content = '<div class="zoom-window-wrap"><div style="display:none;" class="loader"></div>';

	if ( $args['is_webinar'] ) {
		$args['option'] = 'zoom_api_webinar_options';
	} else {
		$args['option'] = 'zoom_api_meeting_options';
	}

	$args['zoom_map_array'] = get_post_meta( $args['type_id'], $args['option'], true );
	if ( ! $args['zoom_map_array'] ) {
		$args['zoom_map_array'] = array();
	}

	$args = video_conferencing_zoom_prepare_args( $args );

	// Add to Google calendar button if enabled in settings
	if ( 1 == $args['zoom_not_show_calendar_link'] && isset( $args['zoom_map_array']['calendar_link'] ) ) {
		$url   = $args['zoom_map_array']['calendar_link'];
		$title = __( 'Add', 'video-conferencing-with-zoom-api' ) . ' ' . $args['type_name'] . ' ' . __( 'To Calendar', 'video-conferencing-with-zoom-api' );

		$content .= '<a class="calendar-btn ' . esc_attr( $args['zoom_btn_css_class'] ) . '"
			target="_blank" rel="noopener"
			href="' . esc_url( $url ) . '"
			title="' . esc_attr( $title ) . '">
			<span class="dashicons dashicons-calendar-alt"></span>
		</a>';
	}

	if ( isset( $args['zoom_map_array']['enforce_login'] ) && ! is_user_logged_in() ) {
		$content .= video_conferencing_zoom_show_is_login();
	} elseif ( video_conferencing_zoom_is_countdown( $args ) ) {
		$content .= video_conferencing_zoom_show_countdown( $args );
	} else {
		$content .= video_conferencing_zoom_load_meeting( $args );
	}

	$content .= '</div>'; // Close zoom-window-wrap

	// Allow addon devs to perform filter before window rendering
	echo apply_filters( 'zoom_wp_before_window_content', $content );

	$display = ob_get_clean();

	return $display;
}

function video_conferencing_zoom_show_is_login() {
	return '<h5>' . esc_html__( 'Restricted access, please login to continue', 'video-conferencing-with-zoom-api' ) . '</h5>
		<a href="' . esc_url( wp_login_url( get_permalink() ) ) . '" alt="' . esc_attr__( 'Login', 'video-conferencing-with-zoom-api' ) . '">' .
		__( 'Login', 'video-conferencing-with-zoom-api' ) . '</a>';
}

function video_conferencing_zoom_load_meeting( $args ) {
	// Allow addon devs to perform action before window rendering
	do_action( 'zoom_wp_before_render_window' );

	if ( ! wp_style_is( 'wplms-style' ) && ! wp_script_is( 'bootstrap' ) && ! wp_script_is( 'bootstrap-modal' ) ) {
		wp_register_script( 'video-conferencing-with-zoom-api-modal', ZOOM_VIDEO_CONFERENCE_PLUGIN_FRONTEND_JS_PATH . '/bootstrap-modal.min.js', array( 'jquery' ), ZVCW_ZOOM_PLUGIN_VER, true );
		wp_enqueue_script( 'video-conferencing-with-zoom-api-modal' );
	}

	$content = '<div class="meeting-view ' . ( $args['zoom_window_size'] ? 'zoom-window-wrap-half' : '' ) . '">';
	if ( $args['zoom_meeting_title'] ) {
		$content .= '<h2>' . esc_html( $args['zoom_meeting_title'] ) . '</h2>';
	}

	// If join meeting not set check if recording should be played
	$args = video_conferencing_zoom_is_recoding_play( $args );

	// Show recording or render meeting
	if ( ! $args['recording_url'] ) {
		$valid = true;
		if ( 1 != $args['global_web_disable'] || $args['auto_join'] || 1 == $args['show_join_web'] ) {
			if ( isset( $args['zoom_map_array']['password'] ) && $args['zoom_map_array']['password'] ) {
				$is_password = esc_html( $args['zoom_map_array']['password'] );

				if ( isset( $_POST['meeting_pwd'] ) && $is_password != $_POST['meeting_pwd'] ) {
					$valid = false;
				}
			}

			if ( $args['auto_join'] || ( $args['join_meeting'] && $valid ) ) {

				do_action( 'zoom_wp_on_join_via_web', $args );

				// Load SDK
				$content .= video_conferencing_zoom_load_sdk_assets( $args );

				$args['hide_form'] = 1;
			}
		}

		// Show thank you message on meeting leave or end
		if ( isset( $_GET['zoom-meeting-leave'] ) ) {
			// Do not call Thankyou on other plugin functions
			if ( ! isset( $_POST['join_iframe'] ) && ! isset( $_POST['back_to_meeting'] ) && ! isset( $_POST['play_recording'] ) ) {
				$meeting_thankyou_message = str_replace( '{username}', esc_html( ucfirst( $args['display_name'] ) ), ( get_option( 'meeting_thankyou_message' ) ? get_option( 'meeting_thankyou_message' ) : '<h4><i>' . esc_html( ucfirst( $args['display_name'] ) ) . ', ' . __( 'Thank you for attending this meeting!', 'video-conferencing-with-zoom-api' ) ) . '</i></h4>' );
				$content                 .= '<div class="zoom-thankyou-msg">' . do_shortcode( $meeting_thankyou_message ) . '</div>';
			}
		}

		// Display meeting join form
		$content .= video_conferencing_zoom_load_meeting_form( $args );
	} else {
		// Show Recordings button
		if ( 1 != $args['zoom_not_show_recordings'] ) {
			$content .= video_conferencing_zoom_show_cloud_recordings( $args );
			$content .= video_conferencing_zoom_show_recording_video( $args );
		}
	}

	if ( ! video_conferencing_zoom_is_valid_license() && ( 1 == $args['meeting_role'] || video_conferencing_zoom_is_user_admin() ) ) {
		$content = '<p><strong>Please enter a valid license key in Zoom Meetings -> Settings -> Setup in order to activate plugin features and receive automatic updates. To check the licence status please visit your <a rel="noopnerer"  target="_blank" href="https://elearningevolve.com/my-account/">My Account page</a></strong>.</p>';
	}

	$content .= '</div>';

	return $content;
}

function video_conferencing_zoom_load_sdk_assets( $args ) {
	$zoom_api_key         = zoom_wp_get_api_key();
	$meeting_number       = $args['type_id'];
	$meeting_pwd          = esc_html( ( isset( $_POST['meeting_pwd'] ) ? $_POST['meeting_pwd'] : '' ) );
	$args['meeting_role'] = video_conferencing_zoom_is_meeting_host_email( $args );
	$args['meeting_role'] = ( $args['is_admin'] ? $args['is_admin'] : $args['meeting_role'] );

	// SDK join requires Webinar host email to start webinar
	if ( $args['is_webinar'] && $args['meeting_role'] && isset( $args['host_email'] ) ) {
		$args['user_email'] = $args['host_email'];
	}

	// Show Recordings button
	if ( 1 != $args['zoom_not_show_recordings'] ) {
		$args['cloud_recording_button'] = video_conferencing_zoom_show_cloud_recordings( $args );
	}

	wp_enqueue_script( 'video-conferencing-with-zoom-api-iframe-resizer', ZOOM_VIDEO_CONFERENCE_PLUGIN_FRONTEND_JS_PATH . '/iframeResizer.min.js', array(), ZVCW_ZOOM_PLUGIN_VER, true );
	wp_enqueue_script( 'video-conferencing-with-zoom-api-tool', ZOOM_VIDEO_CONFERENCE_PLUGIN_FRONTEND_JS_PATH . '/tool.js', array(), ZVCW_ZOOM_PLUGIN_VER, true );
	wp_register_script( 'video-conferencing-with-zoom-api-index', ZOOM_VIDEO_CONFERENCE_PLUGIN_FRONTEND_JS_PATH . '/index.js', array( 'video-conferencing-with-zoom-api-tool' ), ZVCW_ZOOM_PLUGIN_VER, true );
	wp_localize_script(
		'video-conferencing-with-zoom-api-index',
		'zvc_array',
		array(
			'zoom_api_key'      => esc_html( $zoom_api_key ),
			'signature'         => video_conferencing_zoom_generate_signature( $meeting_number, $args['meeting_role'], $zoom_api_key ),
			'display_name'      => esc_html( $args['display_name'] ),
			'user_email'        => esc_html( $args['user_email'] ),
			'meeting_number'    => esc_html( $meeting_number ),
			'meeting_pwd'       => esc_html( $meeting_pwd ),
			'lang'              => esc_html( video_conferencing_zoom_get_wp_locale_for_sdk( $args['lang_code'] ) ),
			'meeting_role'      => esc_html( $args['meeting_role'] ),
			'is_webinar'        => esc_html( $args['is_webinar'] ),
			'is_ld_bind'        => isset( $args['zoom_map_array']['ld_lessons_bind'] ),
			'zvc_security'      => wp_create_nonce( '_nonce_zvc_security' ),
			'meeting_path'      => esc_url( add_query_arg( 'zoom_wp_action=render_zoom_meeting&', '', get_permalink() ) ),
			'no_scroll_window'  => esc_attr( $args['zoom_not_scroll_window'] ),
			'thankyou_redirect' => ( $args['thankyou_redirect'] ? esc_url_raw( get_permalink( $args['thankyou_redirect'] ) ) : 0 ),
			'self_view'         => esc_attr( $args['self_view'] ),
			'join_web_target'   => esc_attr( $args['join_web_target'] ),
			'auto_join'         => $args['auto_join'],
			'new_tab_join_msg'  => esc_attr( __( 'Please allow popups for this site from your address bar and reload this page to join the meeting in a new tab', 'video-conferencing-with-zoom-api' ) ),
		)
	);
	wp_enqueue_script( 'video-conferencing-with-zoom-api-index' );
	return $args['join_via_app'] . '<div id="' . esc_attr( $args['id'] ) . '" class="zoom-iframe-container ' . esc_attr( $args['class'] ) . '"></div>' . $args['cloud_recording_button'];
}

function video_conferencing_zoom_load_meeting_form( $args ) {
	$is_password = false;
	$error_msg   = false;
	if ( ! video_conferencing_zoom_is_autologin( $args ) ) {
		if ( isset( $args['zoom_map_array']['password'] ) && $args['zoom_map_array']['password'] ) {
			$is_password = esc_html( $args['zoom_map_array']['password'] );

			if ( isset( $_POST['meeting_pwd'] ) && $is_password != $_POST['meeting_pwd'] ) {
				$error_msg['password'] = __( 'Meeting password is incorrect', 'video-conferencing-with-zoom-api' );
			}
		}
	}

	// Show Recordings button
	if ( 1 != $args['zoom_not_show_recordings'] && ! $args['hide_form'] ) {
		$args['cloud_recording_button'] = video_conferencing_zoom_show_cloud_recordings( $args );
	}

	$output = '<div style="display:' . ( $args['hide_form'] ? 'none' : 'block' ) . ';">';

	if ( 1 != $args['global_web_disable'] || 1 == $args['show_join_web'] ) {
		ob_start();
		include ZOOM_VIDEO_CONFERENCE_PLUGIN_VIEWS_PATH . DIRECTORY_SEPARATOR . 'tpl-meeting-form.php';
		$output .= ob_get_clean();
	} else {
		$output .= $args['join_via_app'];
	}

	$output .= $args['cloud_recording_button'] . '</div>';

	return $output;
}

function video_conferencing_zoom_show_countdown( $args ) {
	$nocache                    = time() + mt_rand( 0, mt_getrandmax() );
	$args['zoom_btn_css_class'] = get_option( 'zoom_btn_css_class' );
	$countdown_title            = apply_filters( 'zoom_wp_countdown_title', ( $args['countdown_title'] ? $args['countdown_title'] : get_option( 'zoom_countdown_title' ) ) );
	if ( $countdown_title ) {
		$countdown_title = '<h3 class="countdown_title">' . esc_html__( $countdown_title, 'video-conferencing-with-zoom-api' ) . '</h3>';
	}

	if ( $args['is_webinar'] ) {
		$register_text = __( 'Register', 'video-conferencing-with-zoom-api' );
	} else {
		$register_text = __( 'Register', 'video-conferencing-with-zoom-api' );
	}

	wp_register_script( 'video-conferencing-with-zoom-api-countdownjs', ZOOM_VIDEO_CONFERENCE_PLUGIN_FRONTEND_JS_PATH . '/jquery.downcount.min.js', array( 'jquery' ), ZVCW_ZOOM_PLUGIN_VER, true );
	wp_localize_script(
		'video-conferencing-with-zoom-api-countdownjs',
		'zvc_array',
		array(
			'meeting_number' => esc_html( $args['type_id'] ),
			'is_webinar'     => esc_html( $args['is_webinar'] ),
			'ajax_url'       => esc_url( admin_url( 'admin-ajax.php' ) ),
			'nonce'          => wp_create_nonce( 'ajax_nonce' ),
			'no_cache'       => $nocache,
		)
	);

	wp_enqueue_script( 'video-conferencing-with-zoom-api-countdownjs' );

	$output  = '<ul class="zoom-meeting-countdown" id="countdowns-' . esc_attr( $args['type_id'] ) . '" data-meeting-time="' . esc_attr( $args['zoom_map_array']['time'] ) . '" data-meeting-id="' . esc_attr( $args['type_id'] ) . '">';
	$output .= '<div class="register-meeting-btn">';
	if ( 1 != $args['hide_join_before_time'] ) {
		$output .= '<form class="zoom-wp-form" method="POST">' . wp_nonce_field( 'meeting_nonce', 'meeting_nonce_field' ) . '<button type="submit" name="join_early" class="zoom-link zoom-join-early' . esc_attr( $args['zoom_btn_css_class'] ) . '">' . __( 'Join Early', 'video-conferencing-with-zoom-api' ) . '</button></form>';
	}

	if ( 1 != $args['hide_register_for_meeting'] && isset( $args['zoom_map_array']['registration_url'] ) ) {

		// Don't show registration button if user already registered
		if ( ! isset( $args['user_data']->ID ) || ! get_user_meta( $args['user_data']->ID, 'zoom_registrant_url_' . $args['type_id'], true ) ) {
			$output .= '<a rel="noopnerer"  target="_blank" href="' . esc_url( $args['zoom_map_array']['registration_url'] ) . '" ><button ' . esc_attr( $args['type_id'] ) . '" class="zoom-link ' . esc_attr( $args['zoom_btn_css_class'] ) . '">' . esc_html( $register_text ) . '</button></a>';
		}
	}

	$output .= '</div>';

	$output .= $countdown_title .
		'<li>
            <span class="days">00</span>
            <p class="days_ref" data-translate-plural="' . esc_attr__( 'days', 'video-conferencing-with-zoom-api' ) . '" data-translate-single="' . esc_html__( 'day', 'video-conferencing-with-zoom-api' ) . '">' . esc_html__( 'days', 'video-conferencing-with-zoom-api' ) . '</p>
        </li>
        <li class="seperator">:</li>
        <li>
            <span class="hours">00</span>
            <p class="hours_ref" data-translate-plural="' . esc_attr__( 'hours', 'video-conferencing-with-zoom-api' ) . '" data-translate-single="' . esc_html__( 'hour', 'video-conferencing-with-zoom-api' ) . '">' . esc_html__( 'hours', 'video-conferencing-with-zoom-api' ) . '</p>
        </li>
        <li class="seperator">:</li>
        <li>
            <span class="minutes">00</span>
            <p class="minutes_ref" data-translate-plural="' . esc_attr__( 'minutes', 'video-conferencing-with-zoom-api' ) . '" data-translate-single="' . esc_html__( 'minute', 'video-conferencing-with-zoom-api' ) . '">' . esc_html__( 'minutes', 'video-conferencing-with-zoom-api' ) . '</p>
        </li>
        <li class="seperator">:</li>
        <li>
            <span class="seconds">00</span>
            <p class="seconds_ref" data-translate-plural="' . esc_attr__( 'seconds', 'video-conferencing-with-zoom-api' ) . '" data-translate-single="' . esc_html__( 'second', 'video-conferencing-with-zoom-api' ) . '">' . esc_html__( 'seconds', 'video-conferencing-with-zoom-api' ) . '</p>
		</li>
		<p class="redirected-text">' . __( 'Not redirected after countdown', 'video-conferencing-with-zoom-api' ) .
		', <a class="redirected-text" href="' . esc_url( add_query_arg( array( 'nocache' => $nocache ) ) ) . '">' . __( 'click here', 'video-conferencing-with-zoom-api' ) . '</a></p>
	</ul>';

	return $output;
}

/**
 * Rendering Latest Recording Display Shortcode Output
 * @return string
 */
function video_conferencing_zoom_show_latest_recording( $atts, $content = null ) {
	if ( ! video_conferencing_zoom_is_valid_request() ) {
		return;
	}

	// If styles fail to enqueue earlier then add them with the shortcode
	if ( ! wp_style_is( 'video-conferencing-with-zoom-api-iframe' ) ) {
		wp_enqueue_style( 'video-conferencing-with-zoom-api-iframe' );
	}

	$args = shortcode_atts(
		array(
			'meeting_id' => '',
			'is_webinar' => '',
			'play_url'   => '',
			'start_time' => '',
			'end_time'   => '',
		),
		$atts
	);

	$args['zoom_not_show_recordings'] = get_option( 'zoom_hide_recordings' );
	$args['zoom_window_size']         = get_option( 'zoom_window_size' );
	$args['meeting_id']               = (float) str_replace( '-', '', str_replace( ' ', '', $args['meeting_id'] ) );
	$args['is_only_recording']        = 1;
	$args['is_meeting_view']          = 0;

	if ( $args['is_webinar'] ) {
		$args['option'] = 'zoom_api_webinar_options';
	} else {
		$args['option'] = 'zoom_api_meeting_options';
	}

	if ( $args['meeting_id'] ) {
		$args['zoom_map_array'] = get_post_meta( $args['meeting_id'], $args['option'], true );
		if ( ! $args['zoom_map_array'] ) {
			$args['zoom_map_array'] = array();
		}

		if ( ! isset( $args['zoom_map_array']['past_recordings'] ) || ! $args['zoom_map_array']['past_recordings'] ) {
			// Try to populate data from API if record missing in DB
			try {
				$recordings = zoom_conference()->recordingsByMeeting( $args['meeting_id'] );

				if ( isset( $recordings->code ) ) {
					throw new Exception( $recordings->message );
				}

				if ( $recordings && isset( $recordings['recording_files'] ) ) {
					foreach ( $recordings['recording_files'] as $rec ) {

						$this_recording = video_conferencing_recording_get_params_by_sequence( (array) $recordings['recording_files'], array() );
						if ( isset( $this_recording['recording_url'] ) && $this_recording['recording_url'] ) {
							$args['zoom_map_array']['past_recordings'][] = $this_recording;
						}
					}

					$save_recordings['past_recordings'] = $args['zoom_map_array']['past_recordings'];
					update_post_meta( $args['meeting_id'], sanitize_text_field( $args['option'] ), $save_recordings );
				}
			} catch ( Exception $e ) {
				if ( current_user_can( 'manage_options' ) ) {
					echo $e->getMessage();
				}
				video_conferencing_zoom_log_error( $e->getMessage() );
			}
		}
	}

	$content = '<div class="' . ( $args['zoom_window_size'] ? 'zoom-window-wrap-half' : '' ) . '"><div style="display:none;" class="loader"></div>';

	// If join meeting not set check if recording should be played
	$args = video_conferencing_zoom_is_recoding_play( $args );
	if ( ! isset( $args['recording_url'] ) || ! $args['recording_url'] ) {
		$content .= __( 'Recording is not available at the moment, please try again later.', 'video-conferencing-with-zoom-api' );
	} else {
		// Apply start and end time logic if exist
		if ( $args['start_time'] || $args['end_time'] ) {
			$args['recording_url'] .= '#t=' . esc_attr( $args['start_time'] );
			$args['recording_url'] .= ( $args['end_time'] ? ',' . esc_attr( $args['end_time'] ) : '' );

			wp_register_script( 'video-conferencing-with-zoom-api-recording', ZOOM_VIDEO_CONFERENCE_PLUGIN_FRONTEND_JS_PATH . '/recording.js', array(), ZVCW_ZOOM_PLUGIN_VER, true );
			wp_enqueue_script( 'video-conferencing-with-zoom-api-recording' );
		}

		$content .= video_conferencing_zoom_show_recording_video( $args );
	}

	$content .= '</div>';

	return $content;
}

/**
 * Rendering Zoom Web SDK
 * @return string
 */
function video_conferencing_zoom_render_meeting_window() {
	if ( ! video_conferencing_zoom_is_valid_request() ) {
		return;
	}

	if ( isset( $_REQUEST['zoom_wp_action'] ) && 'render_zoom_meeting' == $_REQUEST['zoom_wp_action'] ) {
		include_once( ZOOM_VIDEO_CONFERENCE_PLUGIN_FRONTEND_PATH . DIRECTORY_SEPARATOR . 'meeting.php' );
	}
}
